//#define WINVER 0x0501
//#define _WIN32_WINNT _WIN32_WINNT_WINXP
#include "windows.h"


//#include <typeinfo>
#include "DebugLog.hpp"
DEBUG_USING_NAMESPACE

#include "wcxhead.h"
/*
#define AT_OK      0
#define AT_VOL     1
#define AT_DIR     2
#define AT_LONG    3
#define AT_INVALID 4
*/
#define AT_OK           0x0
#define AT_RO           0x1
#define AT_HIDDEN       0x2
#define AT_SYSTEM       0x4
#define AT_VOL          0x8
#define AT_DIR          0x10
#define AT_ARC          0x20
#define AT_ANY          0x3F

//#include "proto/assets_pac.h"
//#include "memory_mapped_file.h"
#include "plugins/plugin.hpp"
#include <map>
#include "graph.hpp"
#include "minIni/minIni.h"
class UPlugins
{
public:
    UPlugins();
    ~UPlugins();
    std::vector<Storage*>* get_all();
    std::vector<Storage*>* get_childs(int parent_id);
    std::vector<Storage*>* get_from_list(std::list<char*> sub_list);

private:
    pluma::Pluma* plugin_manager_;
    CGraph::Graph* heirarchy_;
    std::map<int, Storage*> plugins_map_;
};

typedef struct
{
    char archname[MAX_PATH];
    HANDLE hArchFile;    //opened file handle
    pluma::Pluma* plugin_manager;   // ?
    //Storage* current_plugin;       // delete
    //DWORD counter;                 // delete
    //int sub_counter;              // delete
    //Storage* sub_current_plugin;  // delete
    //StorageState* plugin_state;   // delete
    //StorageState* sub_plugin_state; // delete

    std::list<std::pair<Storage*,StorageState*>>* plugins_chain;
    UPlugins* plugins;
    tChangeVolProc pLocChangeVol;
    tProcessDataProc pLocProcessData;
} tState;
typedef tState* STATE_HANDLE;



template<class Cont>
class const_reverse_wrapper
{
    const Cont& container;

public:
    const_reverse_wrapper(const Cont& cont) : container(cont) { }
    decltype(container.rbegin()) begin() const
    {
        return container.rbegin();
    }
    decltype(container.rend()) end() const
    {
        return container.rend();
    }
};

template<class Cont>
class reverse_wrapper
{
    Cont& container;

public:
    reverse_wrapper(Cont& cont) : container(cont) { }
    decltype(container.rbegin()) begin()
    {
        return container.rbegin();
    }
    decltype(container.rend()) end()
    {
        return container.rend();
    }
};

template<class Cont>
const_reverse_wrapper<Cont> reverse(const Cont& cont)
{
    return const_reverse_wrapper<Cont>(cont);
}

template<class Cont>
reverse_wrapper<Cont> reverse(Cont& cont)
{
    return reverse_wrapper<Cont>(cont);
}

#define ATTR_READONLY     0x01
#define ATTR_HIDDEN       0x02
#define ATTR_SYSTEM       0x04
#define ATTR_VOLUME_ID    0x08
#define ATTR_DIRECTORY    0x10
#define ATTR_ARCHIVE      0x20
#define ATTR_LONG_NAME    0x0F

// 
#define PK_KEY_SUCCESS 0
#define PK_KEY_RELOAD 1
// Key Modifier offsets
#define SC_SHIFT 0x2000
#define SC_CTRL  0x4000
#define SC_ALT   0x8000
#define SC_META  0x1000

//-----------------------=[ DLL exports ]=--------------------

#ifdef __cplusplus
extern "C" {
#endif

// OpenArchive should perform all necessary operations when an archive is to be opened
STATE_HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData);

// WinCmd calls ReadHeader to find out what files are in the archive
int __stdcall ReadHeader(STATE_HANDLE hArcData, tHeaderData *HeaderData);

// ProcessFile should unpack the specified file or test the integrity of the archive
int __stdcall ProcessFile(STATE_HANDLE hArcData, int Operation, char *DestPath, char *DestName);

// CloseArchive should perform all necessary operations when an archive is about to be closed
int __stdcall CloseArchive(STATE_HANDLE hArcData);

// This function allows you to notify user about changing a volume when packing files
void __stdcall SetChangeVolProc(STATE_HANDLE hArcData, tChangeVolProc pChangeVolProc);

// This function allows you to notify user about the progress when you un/pack files
void __stdcall SetProcessDataProc(STATE_HANDLE hArcData, tProcessDataProc pProcessDataProc);

int __stdcall PackFiles (char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags);

int __stdcall GetPackerCaps();

int __stdcall DeleteFiles (char *PackedFile, char *DeleteList);

BOOL __stdcall CanYouHandleThisFile (char *FileName);

int __stdcall SetKeyRegisterCallback(tKeyRegisterProc KeyRegisterProc);

int __stdcall KeyHandle (unsigned int ShortCut);

#ifdef __cplusplus
}
#endif
