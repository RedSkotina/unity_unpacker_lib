#ifndef MEMORYMAPPEDFILE_H
#define MEMORYMAPPEDFILE_H
#include "windows.h"
#include "binpac.h"
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
using binpac::IMemoryStorage;
class MemoryMappedFile: public IMemoryStorage
{
public:
	MemoryMappedFile(size_t page_size);
	~MemoryMappedFile();
	
public:
	void Open(std::string& full_path, bool readonly = false);
	void Close();
	void FinalClose();
	DWORD GetSize();
	void SetSize(DWORD size);
	unsigned char* Begin();
	unsigned char* End();
	void SetRange(DWORD begin, DWORD end);
	unsigned char* RangeBegin();
	unsigned char* RangeEnd();
	HANDLE GetFileHandle();
	void Flush();
	size_t GetPageSize(); //common page size
	size_t GetCurrentPageSize(); // current page size
	
	size_t Read(unsigned char* buffer , unsigned int offset, size_t len);
    size_t Read(unsigned char* buffer , unsigned char* ptr,  size_t len);
    
    size_t Read(unsigned char* value , unsigned int offset);
    size_t ReadDWord(unsigned int* value , unsigned int offset);
    size_t WriteDWord(unsigned int value , unsigned int offset);
    
	unsigned int GetCurrentPageId();
	void SetPage(unsigned int page);
	void ReserveMemory(DWORD begin, DWORD size);
	void FreeMemory(DWORD begin, DWORD size);
	
    void AddStorePiece(size_t size);
    static void WrapperAddStorePiece(size_t size);
    static void SetSelf(MemoryMappedFile* mmf);
    
	unsigned char* GetTempBufferPtr();
    std::string GetFullPath(){ return fullpath_; };
private:
	static unsigned char temp_buffer[67108864];
	HANDLE file_handle_;
	HANDLE map_handle_;
	DWORD file_size_;
	std::string fullpath_;
    unsigned char* map_begin_;
	unsigned char* map_end_;
	DWORD page_size_;
	DWORD offset_granularity_;
	DWORD current_page_id_;
	bool is_memory_reserved_;
	DWORD reserved_memory_offset_;
	LPVOID reserved_memory_ptr_;
	int is_open;
	//std::string full_path_;
    size_t capacity_;
    static MemoryMappedFile* self_;
	
};
void DeleteMemoryMappedFile(MemoryMappedFile* mmf);

class DirectMemory: public IMemoryStorage
{
public:
    DirectMemory();
    ~DirectMemory();
    void Open(size_t size);
    void Close();
    DWORD GetSize();
    unsigned char* Begin();
	unsigned char* End();
    unsigned char& operator[] (int index);
    void SetPage(unsigned int);
    size_t GetPageSize();
    unsigned int GetCurrentPageId();
    unsigned char* GetTempBufferPtr();
    size_t GetCurrentPageSize();
    size_t ReadDWord(unsigned int* value , unsigned int offset);
    size_t WriteDWord(unsigned int value , unsigned int offset);
private:
    unsigned char * memory_;
    size_t size_;
};
void DeleteDirectMemory(DirectMemory* dm);

class Memory
{
public:
    binpac::const_byteptr begin;
    binpac::const_byteptr end;
    std::shared_ptr<MemoryMappedFile> mmf;
    std::string name;
};


unsigned int get_pageid_from_ptr(unsigned char* ptr, unsigned char* base, size_t page_size);
unsigned int get_offset_from_ptr(unsigned char* ptr, unsigned char* base, size_t page_size);

//void MemMove(binpac::IMemoryStorage* dst_mmf, binpac::IMemoryStorage* src_mmf,  unsigned char* dst, unsigned char* src, size_t size);

void MemMove(IMemoryStorage* dst_mmf, IMemoryStorage* src_mmf,  unsigned char* dst, unsigned char* src, size_t size);
void MemMove(IMemoryStorage* mmf,  unsigned char* dst, unsigned char* src, size_t size);
void MemCpy(IMemoryStorage* dst_mmf, IMemoryStorage* src_mmf, unsigned char* dst, unsigned char* src, size_t size, bool forward);
void MemCpy(IMemoryStorage* mmf, unsigned char* dst, unsigned char* src, size_t size, bool forward = true);
bool CopyFile(MemoryMappedFile* dst_mmf, Memory* src_mem);
#endif // MEMORYMAPPEDFILE_H