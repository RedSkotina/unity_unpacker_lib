#include "memory_mapped_file.h"
#include <exception>
#include <stdexcept>
#include <system_error>

#include "DebugLog.hpp"
DEBUG_USING_NAMESPACE
//DEBUG_TO_CONSOLE();
std::string get_current_dir();
DEBUG_TO_FILE((get_current_dir()+std::string("\\memory-log.txt")).c_str());

 #include <sstream>
std::string LastError()
{
 // Get last error.
DWORD err = GetLastError();
std::ostringstream out;
out << err;
std::string str_err = out.str();
// Translate ErrorCode to String.
LPTSTR Error = 0;
if(::FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                    NULL,
                    err,
					0, 
                    (LPTSTR)&Error,
                    0,
                    NULL) == 0)
{
   // Failed in translating.
}
CharToOem(Error, Error);
std::string error_string = str_err +" : "+ Error  ;
if( Error )
{
   ::LocalFree( Error );
   Error = 0;
}
return error_string;
}

MemoryMappedFile::MemoryMappedFile(size_t page_size)
{
    OutputDebugString("INIT:");
   SYSTEM_INFO SysInfo;          // system information; used to get the granularity
	DWORD dwSysGran;              // system allocation granularity
	// Get the system allocation granularity.
	GetSystemInfo(&SysInfo);
	dwSysGran = SysInfo.dwAllocationGranularity; // x32 = 64 kb
	//page_size_ = 64 * 16 * dwSysGran; // ~64MB
	page_size_ = page_size * dwSysGran; 
	offset_granularity_ = 4096;
	map_begin_ = nullptr;
	ReserveMemory(0,page_size_);
	is_memory_reserved_ = true; 
	reserved_memory_offset_ = 0;
	is_open = 0;
    capacity_ = 0;
}

MemoryMappedFile::~MemoryMappedFile()
{
    is_open = 0;
}

void MemoryMappedFile::ReserveMemory(DWORD begin, DWORD size)
{
	DEBUG_METHOD();
	LPVOID ret = VirtualAlloc((void*)begin,size,MEM_RESERVE,PAGE_READWRITE);
	if (ret == NULL )
	{
		DEBUG_VALUE_OF_HEX(begin);
		DEBUG_MESSAGE(LastError());
		throw std::runtime_error(LastError());
	}
	reserved_memory_ptr_ = ret;
	if (map_begin_ == nullptr)
		map_begin_ = (unsigned char*)ret;  // first time set
}
void MemoryMappedFile::FreeMemory(DWORD begin, DWORD size)
{
	DEBUG_METHOD();
	BOOL  ret = VirtualFree((void*)reserved_memory_ptr_,0,MEM_RELEASE);
	if ( !ret  )
	{
		DEBUG_MESSAGE(LastError());
		throw std::runtime_error(LastError());
	}
	reserved_memory_ptr_ = nullptr;
	is_memory_reserved_ = false;
	reserved_memory_offset_ = 0;
}
void MemoryMappedFile::Close()
{
	DEBUG_METHOD();
	if (is_open <= 1)
	{
		DEBUG_MESSAGE("MemoryMappedFile already closed");
		//OutputDebugString("MemoryMappedFile already closed\n");
		return;
	}
	
	Flush();
	
	UnmapViewOfFile(map_begin_);
	map_begin_ = nullptr;
	CloseHandle(map_handle_);
	map_handle_ = (HANDLE)ERROR_INVALID_HANDLE ;
	CloseHandle(file_handle_);
	file_handle_ = (HANDLE)ERROR_INVALID_HANDLE ;
	file_size_ = 0;
	//free partial meemory reserve
	if (is_memory_reserved_)
		FreeMemory(reserved_memory_offset_,page_size_ - reserved_memory_offset_);
	// take full memory reserve
	ReserveMemory(0,page_size_);
	is_memory_reserved_ = true; 
	reserved_memory_offset_ = 0;
	is_open = 1;
    //OutputDebugString("Close:");
    //OutputDebugString(fullpath_.c_str());
		
}
void MemoryMappedFile::FinalClose()
{
	DEBUG_METHOD();
	if (is_open != 1)
	{
		DEBUG_MESSAGE("MemoryMappedFile already open or closed");
		//printf("MemoryMappedFile already open or closed\n");
		return;
	}
	
	if (is_memory_reserved_)
		FreeMemory(reserved_memory_offset_,page_size_ - reserved_memory_offset_);
	
}
void print_last_err();
void MemoryMappedFile::Open(std::string& full_path,bool readonly)
{
    OutputDebugString("OPEN:");
    OutputDebugString(full_path.c_str());
	
	DEBUG_METHOD();
	if (is_open == 2)
	{
		DEBUG_MESSAGE("MemoryMappedFile already open");
		printf("MemoryMappedFile already open\n");
		return;
	}
	DWORD dwCreationDisposition = OPEN_ALWAYS;
	if (readonly)
		dwCreationDisposition = OPEN_EXISTING;
	file_handle_= CreateFile(full_path.c_str(),
	                         GENERIC_READ | GENERIC_WRITE,
	                         FILE_SHARE_WRITE,
	                         0,
	                         dwCreationDisposition,
	                         FILE_ATTRIBUTE_NORMAL,
	                         0);
	if ((file_handle_ == INVALID_HANDLE_VALUE) && (readonly))
	{
		DEBUG_MESSAGE(LastError());
        DEBUG_MESSAGE(full_path.c_str());
		std::error_code ec (1,std::generic_category());
		throw std::system_error(ec,"Could not open src file for read");
	}
	if (file_handle_ == INVALID_HANDLE_VALUE)
	{
		DEBUG_MESSAGE(LastError());
        DEBUG_MESSAGE(full_path.c_str());
		printf(LastError().c_str());
		std::error_code ec (1,std::generic_category());
		throw std::system_error(ec,"Could not open file for write");
	
	}	
	is_open = 2;
	file_size_ = GetFileSize(file_handle_, NULL);

	if (file_size_ == INVALID_FILE_SIZE)
	{
		DEBUG_MESSAGE(LastError());
        DEBUG_MESSAGE(full_path.c_str());
		throw std::runtime_error(LastError());
	}
	if (file_size_ == 0)
	{
		BOOL res1 = SetFilePointer(file_handle_, 4 , nullptr, FILE_BEGIN); 
		BOOL res2 = SetEndOfFile(file_handle_);
		BOOL res3 = SetFilePointer(file_handle_, 0 , nullptr, FILE_BEGIN);
		file_size_ = 4;
        capacity_ = 0;
	}
	map_handle_ = CreateFileMapping(file_handle_,
	                                    NULL,
	                                    PAGE_READWRITE,
	                                    0,
	                                    0,//nFileSize + src_size - dst_region_size,
	                                    0);
	if (map_handle_ == NULL )
	{
		DEBUG_MESSAGE(LastError());
        DEBUG_MESSAGE(full_path.c_str());
		throw std::runtime_error(LastError());
	}
	SetRange(0,(page_size_<file_size_)?page_size_:file_size_);
	current_page_id_ = 0;
    fullpath_ = full_path;

}
size_t MemoryMappedFile::Read(unsigned char* buffer, unsigned int offset, size_t len) // UNTESTED
{
	
	for (int i = 0; i<len; i++)
	{
		unsigned int page_id = get_pageid_from_ptr(map_begin_+offset, map_begin_, page_size_);
		unsigned int page_offset = get_offset_from_ptr(map_begin_+offset, map_begin_, page_size_);
		SetPage(page_id);
		buffer[i] = (map_begin_+page_offset)[i];
	}
	return len;
}
size_t MemoryMappedFile::Read( unsigned char* buffer, unsigned char* ptr,  size_t len) // UNTESTED
{
	
	for (int i = 0; i<len; i++)
	{
		unsigned int page_id = get_pageid_from_ptr(ptr, map_begin_, page_size_);
		unsigned int page_offset = get_offset_from_ptr(ptr, map_begin_, page_size_);
		SetPage(page_id);
		buffer[i] = (map_begin_+page_offset)[i];
	}
	return len;
}
size_t MemoryMappedFile::Read(unsigned char* value , unsigned int offset)
{
	unsigned int page_id = get_pageid_from_ptr(map_begin_+offset, map_begin_, page_size_);
	unsigned int page_offset = get_offset_from_ptr(map_begin_+offset, map_begin_, page_size_);
	SetPage(page_id);
	*value = *(map_begin_+page_offset);
	return 1;
}
size_t MemoryMappedFile::ReadDWord(unsigned int* value , unsigned int offset)
{
	unsigned int page_id = get_pageid_from_ptr(map_begin_+offset, map_begin_, page_size_);
	unsigned int page_offset = get_offset_from_ptr(map_begin_+offset, map_begin_, page_size_);
	SetPage(page_id);
	*value = *(unsigned int*)(map_begin_+page_offset);
	return 4;
}
size_t MemoryMappedFile::WriteDWord(unsigned int value , unsigned int offset)
{
	unsigned int page_id = get_pageid_from_ptr(map_begin_+offset, map_begin_, page_size_);
	unsigned int page_offset = get_offset_from_ptr(map_begin_+offset, map_begin_, page_size_);
	SetPage(page_id);
	*(unsigned int*)(map_begin_+page_offset) = value;
	return 4;
}      
unsigned char* MemoryMappedFile::Begin()
{
	return map_begin_;
}

unsigned char* MemoryMappedFile::End()
{
	return map_begin_+file_size_;
}

DWORD MemoryMappedFile::GetSize()
{
		return file_size_;
}

void MemoryMappedFile::SetSize(DWORD size)
{
	DEBUG_METHOD();
	if (map_begin_ != nullptr)
		UnmapViewOfFile(map_begin_);
	if (map_handle_ != NULL)
		CloseHandle(map_handle_);
	// set physical size of file
	BOOL res1 = SetFilePointer(file_handle_, (size - file_size_), nullptr, FILE_END); 
	BOOL res2 = SetEndOfFile(file_handle_);
	
	HANDLE map_handle = CreateFileMapping(file_handle_,
	                                    NULL,
	                                    PAGE_READWRITE,
	                                    0,
	                                    size,
	                                    0);
	if (map_handle == NULL )
	{
		DEBUG_MESSAGE(LastError());
		throw std::runtime_error(LastError());
	}
    if (GetLastError() == 0xB7) //ERROR_ALREADY_EXISTS
    {
        throw std::runtime_error("map_handle already exist. cant change size.");
    }
	//if ((map_handle != map_handle_ ) && (map_handle_ != nullptr))
	//	throw std::runtime_error("Cannot remap file: map_handle != map_handle_");
	map_handle_ = map_handle;
	file_size_ = size;
	
	SetRange(0,(page_size_<file_size_)?page_size_:file_size_);
	
}

unsigned char* MemoryMappedFile::RangeBegin()
{
	return (unsigned char*)map_begin_;
}

unsigned char* MemoryMappedFile::RangeEnd()
{
	return (unsigned char*)map_end_;
}

void MemoryMappedFile::SetRange(DWORD begin, DWORD end)
{
	DEBUG_METHOD();
	if (map_begin_ != nullptr)
		UnmapViewOfFile(map_begin_);
	if (is_memory_reserved_)
		FreeMemory(reserved_memory_offset_,page_size_ - reserved_memory_offset_);
	
		
	unsigned char* map_begin =(unsigned char*) MapViewOfFileEx(map_handle_,
	                                    FILE_MAP_ALL_ACCESS,
	                                    0,
	                                    begin,
	                                    end - begin ,
										map_begin_);
	
	if (map_begin == NULL)
	{
		printf("null %d %d ", map_begin_,map_handle_);
		printf("%d %d ", begin,end);
		printf("%d ", reserved_memory_offset_);
		//DEBUG_VALUE_OF(map_begin_);
		DEBUG_MESSAGE(LastError());
		throw std::runtime_error(LastError());
	}
	if ((map_begin != map_begin_) &&(map_begin_ != nullptr))
	{
		DEBUG_MESSAGE("MapViewOfFileEx allocated another address");
		printf("%d ", map_begin_);
		//DEBUG_VALUE_OF(map_begin_);
		throw std::runtime_error(LastError());
	}
	if ( (end-begin) != page_size_)
	{
		DWORD size = end - begin;
		DWORD temp = (size + 65536)/65536; // вводим переменную чтобы компилятор не оптимизировал вычисления
		DWORD aligned_offset =  65536 *  temp; // выравниваем по границе 64к в большую сторону
		DWORD ptr = (DWORD) map_begin + aligned_offset;
		ReserveMemory( ptr , page_size_ - aligned_offset ); // лочим/резервируем остаток 
		is_memory_reserved_ = true;
		reserved_memory_offset_ = aligned_offset;
	}
	else
		is_memory_reserved_ = false; // если страница выделена целиком, то не резервируем между вызовами SetRange память
		
	
	//MessageBox(nullptr, "ddd1","runtime error", MB_OK);
	map_begin_ = map_begin;
	if (end == 0) 
		end = file_size_;
	
	
	map_end_ = map_begin_ + (end - begin);
	
	
	
}
HANDLE MemoryMappedFile::GetFileHandle()
{
	return file_handle_;
}

void MemoryMappedFile::Flush()
{
	DEBUG_METHOD();
	if (map_begin_ != nullptr)
	if (!FlushViewOfFile(map_begin_, 0))
	{
		DEBUG_MESSAGE(LastError());
		printf(LastError().c_str());
		throw std::runtime_error(LastError());
	}
}

size_t MemoryMappedFile::GetPageSize()
{
	return page_size_;
}

unsigned int MemoryMappedFile::GetCurrentPageId()
{
	return current_page_id_;
}

size_t MemoryMappedFile::GetCurrentPageSize()
{
	return map_end_ - map_begin_;
}
void MemoryMappedFile::SetPage(unsigned int page)
{
	DEBUG_METHOD();
	DEBUG_VALUE_OF(page);
	if (current_page_id_ == page)
	{
		//DEBUG_MESSAGE("...skip...");
		return;
	}	
	DWORD begin = page*page_size_;
	DWORD end = (page+1)*page_size_;
	
	if (end > file_size_)
		end = file_size_;
	SetRange(begin,end);
	current_page_id_ = page;
}

unsigned char* MemoryMappedFile::GetTempBufferPtr()
{
	return temp_buffer;
}

void MemoryMappedFile::AddStorePiece(size_t size)
{
    DWORD cur_size = GetSize();
    SetSize(cur_size + size);
}

MemoryMappedFile* MemoryMappedFile::self_ = NULL;

void MemoryMappedFile::WrapperAddStorePiece(size_t size)
{
    self_->AddStorePiece(size);
}
void MemoryMappedFile::SetSelf(MemoryMappedFile* mmf)
{
    MemoryMappedFile::self_ = mmf;
}
/*
void MemMove(binpac::IMemoryStorage* dst_mmf, binpac::IMemoryStorage* src_mmf,  unsigned char* dst, unsigned char* src, size_t size)
{
    MemMove((MemoryMappedFile*)dst_mmf, (MemoryMappedFile*) src_mmf, dst, src, size);
}*/


unsigned char MemoryMappedFile::temp_buffer[] = {0};

void DeleteMemoryMappedFile(MemoryMappedFile* mmf)
{
    mmf->Close();
	mmf->FinalClose();
	delete mmf;
}

void MemMove(IMemoryStorage* dst_mmf, IMemoryStorage* src_mmf, unsigned char* dst, unsigned char* src, size_t size)
{
	DEBUG_METHOD();
	
	if( (dst > src) && (dst < (src + size))) { // src < dst < src+size 
		/*
		* Overlapping Buffers
		*  copy from higher addresses to lower addresses
		* src |----------|
		*       dst |------------|
		*/
		MemCpy(dst_mmf, src_mmf, dst, src, size, false);
	} 
	else if ( (dst < src) && (src < (dst + size)) ) // dst < src < dst + size
	{
		/*
		* Overlapping Buffers
		*         src |----------|
		* dst |------------|
		*  
		* copy from lower addresses to higher addresses
		*/
		MemCpy(dst_mmf, src_mmf, dst, src, size, true);
		
	}
	else {
		/*
		* Non-Overlapping Buffers
		* src |----------|
		*                     dst |------------|
		* copy from lower addresses to higher addresses
		*/
		MemCpy(dst_mmf, src_mmf, dst, src, size, true);
	}
	
	//return dst;

}
void MemMove(IMemoryStorage* mmf, unsigned char* dst, unsigned char* src, size_t size)
{
	DEBUG_METHOD();
	
	if( (dst > src) && (dst < (src + size))) { // src < dst < src+size 
		/*
		* Overlapping Buffers
		*  copy from higher addresses to lower addresses
		* src |----------|
		*       dst |------------|
		*/
		MemCpy(mmf, dst, src, size, false);
	} 
	else if ( (dst < src) && (src < (dst + size)) ) // dst < src < dst + size
	{
		/*
		* Overlapping Buffers
		*         src |----------|
		* dst |------------|
		*  
		* copy from lower addresses to higher addresses
		*/
		MemCpy(mmf, dst, src, size, true);
		
	}
	else {
		/*
		* Non-Overlapping Buffers
		* src |----------|
		*                     dst |------------|
		* copy from lower addresses to higher addresses
		*/
		MemCpy(mmf, dst, src, size, true);
	}
	
	//return dst;

}
unsigned int get_pageid_from_ptr(unsigned char* ptr, unsigned char* base, size_t page_size)
{
	unsigned int pageid = (ptr - base)/page_size;
	return pageid;
}
unsigned int get_offset_from_ptr(unsigned char* ptr, unsigned char* base, size_t page_size)
{
	unsigned int offset = (ptr - base)%page_size;
	return offset;
}

void MemCpy(IMemoryStorage* dst_mmf, IMemoryStorage* src_mmf, unsigned char* dst, unsigned char* src, size_t size, bool forward = true )
{
	DEBUG_METHOD();
    if ( (src + size - 1) >= src_mmf->End() )
	{
		DEBUG_MESSAGE("Probably invalid size for copy from src_mmf: ");
		DEBUG_VALUE_OF_HEX((unsigned int)src);
		DEBUG_VALUE_OF_HEX(size);
		DEBUG_VALUE_OF_HEX((unsigned int)dst_mmf->End());
	
		throw std::runtime_error("not have enough src bytes for copy ");
	}
	if ( (dst + size - 1) >= dst_mmf->End() )
	{
		DEBUG_MESSAGE("Overflow dst_mmf: ");
		DEBUG_VALUE_OF_HEX((unsigned int)dst);
		DEBUG_VALUE_OF_HEX(size);
		DEBUG_VALUE_OF_HEX((unsigned int)dst_mmf->End());
	
		throw std::runtime_error("overflow dst size with copy");
	}
	unsigned char* dst_base = dst_mmf->Begin();
	unsigned char* src_base = src_mmf->Begin();
	unsigned int src_page_size = src_mmf->GetPageSize();
	unsigned int dst_page_size = dst_mmf->GetPageSize();
							
	unsigned int src_offset = get_offset_from_ptr(src, src_base, src_page_size);
	unsigned int dst_offset = get_offset_from_ptr(dst, dst_base, dst_page_size);
	/*
    // в пределах одной страницы
	bool src_one_page = false;
	if ((src_page_size - src_offset)>= size)
		src_one_page = true;
	// выделяем или нет для бекапа память	
	unsigned char * temp;
	if (src_one_page)
		temp = src_base;
	else
		temp = src_mmf->GetTempBufferPtr();
	
	unsigned char* temp_base = temp;
	*/		
	// keep current page id
	unsigned int old_src_pageid = src_mmf->GetCurrentPageId();
	unsigned int old_dst_pageid = dst_mmf->GetCurrentPageId();
	unsigned int src_pageid;
	unsigned int dst_pageid;
	
	unsigned char* cur_src_ptr;
	unsigned char* cur_dst_ptr;
	size_t cur_src_size;
	size_t cur_dst_size;
	size_t cur_size;
	unsigned int src_cur_page_size;
	
	
	if (forward) //copy from lower addresses to higher addresses
	{
		
		while (size)
		{
			src_pageid = get_pageid_from_ptr(src, src_base, src_page_size );
			src_mmf->SetPage(src_pageid);
			/*
            if (src_one_page)
			{
				temp_base = src_base;
			}
			else
			{
				src_cur_page_size = src_mmf->GetCurrentPageSize();
				memcpy(temp_base, src_base, src_cur_page_size); //backup src page
			}
             */
			dst_pageid = get_pageid_from_ptr(dst, dst_base, dst_page_size );
			dst_mmf->SetPage(dst_pageid);
			
			src_offset = get_offset_from_ptr(src, src_base, src_page_size);
			dst_offset = get_offset_from_ptr(dst, dst_base, dst_page_size);
	
			cur_src_ptr = src_base + src_offset;
			cur_dst_ptr = dst_base + dst_offset;
			cur_src_size = src_page_size - src_offset;
			cur_dst_size = dst_page_size - dst_offset;
			
			cur_size = (cur_src_size < cur_dst_size) ? cur_src_size : cur_dst_size; 
			
			while(cur_size-- && size )
			{
				*cur_dst_ptr++ = *cur_src_ptr++;
				src++;
				dst++;
				size--;
			}
		
		}
		//copy from temp to dst
	}
	else // copy from higher addresses to lower addresses
	{
		src += size-1;
		dst += size-1;
		while (size)
		{
			src_pageid = get_pageid_from_ptr(src, src_base, src_page_size );
			src_mmf->SetPage(src_pageid);
			/*
			if (src_one_page)
			{
				temp_base = src_base;
			}
			else
			{
				src_cur_page_size = src_mmf->GetCurrentPageSize();
				memcpy(temp_base, src_base, src_cur_page_size); //backup src page
			}
			*/
			dst_pageid = get_pageid_from_ptr(dst, dst_base, dst_page_size );
			dst_mmf->SetPage(dst_pageid);
			
			src_offset = get_offset_from_ptr(src, src_base, src_page_size);
			dst_offset = get_offset_from_ptr(dst, dst_base, dst_page_size);
            
            cur_src_ptr = src_base + src_offset;
			cur_dst_ptr = dst_base + dst_offset;
			cur_src_size = src_offset + 1;
			cur_dst_size = dst_offset + 1;
			
			cur_size = (cur_src_size < cur_dst_size) ? cur_src_size : cur_dst_size; 
			
			while(cur_size-- && size )
			{
				*cur_dst_ptr-- = *cur_src_ptr--;
				src--;
				dst--;
				size--;
			}
		
		}
	}
	
	//restore old page id
	src_mmf->SetPage(old_src_pageid); 
	dst_mmf->SetPage(old_dst_pageid); 
	
	//delete[] temp;
}

// копируем данные в пределах одного файла
// мы не беспокоимся о пересечении исходного буфера и буфера назначения
void MemCpy(IMemoryStorage* mmf, unsigned char* dst, unsigned char* src, size_t size, bool forward)
{
	DEBUG_METHOD();
	if ( (src + size - 1) >= mmf->End() )
	{
		DEBUG_MESSAGE("Probably invalid size for copy from mmf: ");
		DEBUG_VALUE_OF_HEX((unsigned int)src);
		DEBUG_VALUE_OF_HEX(size);
		DEBUG_VALUE_OF_HEX((unsigned int)mmf->End());
	
		throw std::runtime_error("not have enough src bytes for copy ");
	}
	if ( (dst + size - 1) >= mmf->End() )
	{
		DEBUG_MESSAGE("Overflow dst mmf: ");
		DEBUG_VALUE_OF_HEX((unsigned int)dst);
		DEBUG_VALUE_OF_HEX(size);
		DEBUG_VALUE_OF_HEX((unsigned int)mmf->End());
	
		throw std::runtime_error("overflow dst size with copy");
	}
	unsigned char* base = mmf->Begin();
	unsigned int page_size = mmf->GetPageSize();
							
	unsigned int src_offset = get_offset_from_ptr(src, base, page_size);
	unsigned int dst_offset = get_offset_from_ptr(dst, base, page_size);
    unsigned int src_pageid = get_pageid_from_ptr(src, base, page_size );
    unsigned int dst_pageid = get_pageid_from_ptr(dst, base, page_size );
    
	// в пределах одной страницы
	bool one_page = false;
    // если исходный буффер лежит целиком в одной странице 
    // если буффер назначения лежит целиком в этой же странице
	if ((page_size - src_offset)>= size)
        if ((page_size - dst_offset)>= size)
            if (src_pageid == dst_pageid)
                one_page = true;
	// выделяем или нет для бекапа память	
	unsigned char* temp_base;
	if (one_page)
		temp_base = base;
	else
		temp_base = mmf->GetTempBufferPtr();
				
	// keep current page id
	unsigned int old_pageid = mmf->GetCurrentPageId();
			
	unsigned char* cur_src_ptr;
	unsigned char* cur_dst_ptr;
	size_t cur_src_size;
	size_t cur_dst_size;
	size_t cur_size;
	unsigned int src_cur_page_size;
	
	
	if (forward) //copy from lower addresses to higher addresses
	{
		
		while (size)
		{
			src_pageid = get_pageid_from_ptr(src, base, page_size );
			mmf->SetPage(src_pageid);
			if (!one_page)
			{
				src_cur_page_size = mmf->GetCurrentPageSize();
				memcpy(temp_base, base, src_cur_page_size); // бэкапим исходную страницу для того
                                                                // чтобы не тратить время на переключение
                                                                // со страницы назначения на исходную
                                                                // при копировании каждого байта
			}
			dst_pageid = get_pageid_from_ptr(dst, base, page_size );
			mmf->SetPage(dst_pageid);
			
			src_offset = get_offset_from_ptr(src, base, page_size);
			dst_offset = get_offset_from_ptr(dst, base, page_size);
	
			cur_src_ptr = temp_base + src_offset;
			cur_dst_ptr = base + dst_offset;
            
			cur_src_size = page_size - src_offset;
			cur_dst_size = page_size - dst_offset;
			
			cur_size = (cur_src_size < cur_dst_size) ? cur_src_size : cur_dst_size; 
			
			while(cur_size-- && size )
			{
				*cur_dst_ptr++ = *cur_src_ptr++;
				src++;
				dst++;
				size--;
			}
		
		}
		//copy from temp to dst
	}
	else // copy from higher addresses to lower addresses
	{
		src += size-1;
		dst += size-1;
		while (size)
		{
			
			
			page_size = mmf->GetPageSize();
			src_pageid = get_pageid_from_ptr(src, base, page_size );
			mmf->SetPage(src_pageid);
			
			if (!one_page)
			{
				src_cur_page_size = mmf->GetCurrentPageSize();
				memcpy(temp_base, base, src_cur_page_size); //backup src page
			}
			
			dst_pageid = get_pageid_from_ptr(dst, base, page_size );
			mmf->SetPage(dst_pageid);
			
			src_offset = get_offset_from_ptr(src, base, page_size);
			dst_offset = get_offset_from_ptr(dst, base, page_size);
	
            cur_src_ptr = temp_base + src_offset;
			cur_dst_ptr = base + dst_offset;
			cur_src_size = src_offset + 1;
			cur_dst_size = dst_offset + 1;
			
			cur_size = (cur_src_size < cur_dst_size) ? cur_src_size : cur_dst_size; 
			
			while(cur_size-- && size )
			{
				*cur_dst_ptr-- = *cur_src_ptr--;
				src--;
				dst--;
				size--;
			}
		
		}
	}
	
	//restore old page id
	mmf->SetPage(old_pageid); 
	
}


unsigned char* DirectMemory::Begin()
{
	return memory_;
}
unsigned char* DirectMemory::End()
{
	return memory_+size_;
}
void DirectMemory::Open(size_t size)
{
	if (memory_)
		delete [] memory_;
	size_ = size;
    memory_ = new unsigned char[size];
    
}
DWORD DirectMemory::GetSize()
{
	return (DWORD)size_;
}
void DirectMemory::Close()
{
	delete[] memory_;
    memory_ = NULL;
    size_ = 0;
    
}
DirectMemory::DirectMemory()
{
	memory_ = NULL;
	size_ = 0;
}
DirectMemory::~DirectMemory()
{
}

unsigned char& DirectMemory::operator[] (int index)
{
    return memory_[index];
}
void DirectMemory::SetPage(unsigned int)
{
}
size_t DirectMemory::GetPageSize()
{
	return size_;
}
unsigned int DirectMemory::GetCurrentPageId()
{
	return 0;
}
unsigned char* DirectMemory::GetTempBufferPtr()
{
    assert(0); // Not Implemented
}
size_t DirectMemory::GetCurrentPageSize()
{
    return size_;
}
size_t DirectMemory::ReadDWord(unsigned int* value , unsigned int offset)
{
	*value = *(unsigned int*)(memory_+offset);
	return 4;
}
size_t DirectMemory::WriteDWord(unsigned int value , unsigned int offset)
{
	*(unsigned int*)(memory_+offset) = value;
	return 4;
}     

void DeleteDirectMemory(DirectMemory* dm)
{
	dm->Close();
	delete dm;
}

bool CopyFile(MemoryMappedFile* dst_mmf, Memory* src_mem)
{
    size_t size = src_mem->end - src_mem->begin;
    dst_mmf->SetSize(size);
	MemMove(
				dst_mmf,
                src_mem->mmf.get(),
				dst_mmf->Begin(),
                src_mem->mmf->Begin(),
				size
	);
}