
analyzer UOgm withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

extern type ext;
extern type stdstring;
%header{
	#include <string>

	//#include <custom/custom.h>
    typedef std::string stdstring; 
	 
	class Ext{
	public:
		Ext(){};
	static stdstring tex_filename;
	static stdstring extension;
	
	stdstring const & getExtension() const{
		return Ext::extension;
	}
	
	static stdstring fullname(const stdstring& name, const stdstring& name_ext  )
	{
		return (name + name_ext);
	}
	
    
	};
	static Ext ext;
%}
%code{
	stdstring Ext::tex_filename="init";
	stdstring Ext::extension=".ogm";
%}
# image
type Pac = record {
  global_header: GlobalHeader;
  content: Content(global_header.content_size);
  global_footer: GlobalFooter;
  } &let
{
	extension: stdstring = ext.getExtension();
    fullname: stdstring =  ext.fullname( ext.tex_filename, extension);
} &byteorder=littleendian  ;

# header of main storage
type GlobalHeader = record {
  dummy1:   uint32;
  dummy2:   uint32;
  uid:      uint32;
  content_size: uint32;
  
} &let {
    file_size: uint32 = content_size;
    content_offset: uint32 = offsetof(content_size) + 4;
};
# header of main storage
type GlobalFooter = record {
  
  dummy4:  uint32;
  
} &let {
};


#array of pixels	
type Content(size: uint32) = record {
 data: bytestring &transient &length=size;
 pad:  padding align 4;
}; 
