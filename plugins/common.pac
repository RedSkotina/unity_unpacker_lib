connection Connection{
upflow = Flow; downflow = Flow;
};

flow Flow {
 datagram = Pac withcontext (connection, this) ;
 
 function pacdata(pacdata: const_bytestring, offset: uint32, size: uint32): const_bytestring
    %{
    // Omitted: DNS pointer loop detection
    if ( offset < 0)
		throw ExceptionOutOfBound("Pac pacdata offset negative", 0,	offset);
	if  (offset >= pacdata.length())
		throw ExceptionOutOfBound("Pac pacdata offset out of bound", offset, pacdata.length());
	if ( (offset + size) > pacdata.length() )
		throw ExceptionOutOfBound("Pac pacdata end offset out of bound", offset + size,	pacdata.length());
	//return const_bytestring(0, 0);
    return const_bytestring(pacdata.begin() + offset, pacdata.begin()  + offset + size);
    %}
	
 #function getDataUnit(): Pac
 #   %{
 #      return dataunit_;
 #   %}

} 
