#ifndef DDS_HPP
#define DDS_HPP
#include "proto/dds_pac.h"
#include "proto/plugin_tex_pac.h"
#include "memory_mapped_file/memory_mapped_file.h"

class DDSUtil
{
public:
    DDSUtil();
    ~DDSUtil();
    int tex_to_dds(std::shared_ptr<MemoryMappedFile> dst_mmf, std::shared_ptr<MemoryMappedFile> src_mmf, size_t src_offset,size_t width,size_t height,size_t size,bool has_mipmap, unsigned int format);
    int dds_to_tex(std::shared_ptr<MemoryMappedFile> dst_mmf, std::shared_ptr<MemoryMappedFile> src_mmf, std::unique_ptr<binpac::Tex::Connection>& tex_connection);

};


// raw: int: argb memory:[bgra]
class TexToDDSConvertor
{
public:
    unsigned int from;
    unsigned int to;
    unsigned int to_raw(unsigned int& src) // inverted if LE
    {
        unsigned int c1 = src & 0xff000000;
        unsigned int c2 = src & 0x00ff0000;
        unsigned int c3 = src & 0x0000ff00;
        unsigned int c4 = src & 0x000000ff;
        unsigned int cc1,cc2,cc3,cc4,cc5,cc6,cc7,cc8;
        switch (from)
        {
        case binpac::Tex::TEX_FORMAT_ABGR4444: // abgr4444 -> argb
            // 2 pixels in uint
            cc1 = c1 & 0xf0000000; // pixel 1
            cc2 = c1 & 0x0f000000;
            cc3 = c2 & 0x00f00000;
            cc4 = c2 & 0x000f0000;
            cc5 = c3 & 0x0000f000; // pixel 2
            cc6 = c3 & 0x00000f00;
            cc7 = c4 & 0x000000f0;
            cc8 = c4 & 0x0000000f;
            //cc1 >>= 3*4; // pixel 1
            cc2 >>= 2*4;
            //cc3 <<= 1*4;
            cc4 <<= 2*4;
            //cc5 >>= 3*4; // pixel 2
            cc6 >>= 2*4;
            //cc7 <<= 1*4;
            cc8 <<= 2*4;
            c1 = cc1 + cc2;
            c2 = cc3 + cc4;
            c3 = cc5 + cc6;
            c4 = cc7 + cc8;
            break;
        case binpac::Tex::TEX_FORMAT_ABGR32: // abgr -> argb
            //c1 >>= 6*4;
            c2 >>= 2*8;
            //c3 <<= 2*4;
            c4 <<= 2*8;
            break;
        case binpac::Tex::TEX_FORMAT_A8: // not need convert
            break;
        /*case binpac::Tex::TEX_FORMAT_BGR24: // 
            break;*/
        /*case binpac::Tex::TEX_FORMAT_RGBA32: // rgba -> argb
            c1 >>= 1*8;
            c2 >>= 1*8;
            c3 >>= 1*8;
            c4 <<= 3*8;
            break;*/
        case binpac::Tex::TEX_FORMAT_BGRA32: // bgra -> argb
            c1 >>= 3*8;
            c2 >>= 1*8;
            c3 <<= 1*8;
            c4 <<= 3*8;
            break;
        case binpac::Tex::TEX_FORMAT_DXT5: // we cannot do something with compressed data
            break;
        default:
            break;

        }
        unsigned int res = c1 + c2 + c3 + c4;
        return res;
    };
    unsigned int from_raw(unsigned int& raw) // argb
    {
        unsigned int c1 = raw & 0xff000000; // a
        unsigned int c2 = raw & 0x00ff0000; // r
        unsigned int c3 = raw & 0x0000ff00; // g
        unsigned int c4 = raw & 0x000000ff; // b
        unsigned int cc1,cc2,cc3,cc4,cc5,cc6,cc7,cc8;
        switch (to)
        {
        case binpac::DDS::DDS_FORMAT_A4R4G4B4: // argb4444 -> abgr4444
            // 2 pixels in uint
            cc1 = c1 & 0xf0000000; // pixel 1
            cc2 = c1 & 0x0f000000;
            cc3 = c2 & 0x00f00000;
            cc4 = c2 & 0x000f0000;
            cc5 = c3 & 0x0000f000; // pixel 2
            cc6 = c3 & 0x00000f00;
            cc7 = c4 & 0x000000f0;
            cc8 = c4 & 0x0000000f;
            //cc1 >>= 3*4; // pixel 1
            cc2 >>= 2*4;
            //cc3 >>= 1*4;
            cc4 <<= 2*4;
            //cc5 >>= 3*4; // pixel 2
            cc6 >>= 2*4;
            //cc7 >>= 1*4;
            cc8 <<= 2*4;
            c1 = cc1 + cc2;
            c2 = cc3 + cc4;
            c3 = cc5 + cc6;
            c4 = cc7 + cc8;
            break;
        case binpac::DDS::DDS_FORMAT_A8R8G8B8 : // argb -> argb
            //c1 >>= 2*4;
            //c2 >>= 2*4;
            //c3 >>= 2*4;
            //c4 <<= 6*4;
            break;
        case binpac::DDS::DDS_FORMAT_DXT1 :
            break;
        case binpac::DDS::DDS_FORMAT_DXT5 :
            break;
        case binpac::DDS::DDS_FORMAT_A8 : // argb -> argb
            break;
        default:
            break;
        }
        unsigned int res = c1 + c2 + c3 + c4;
        return res;
    };

    unsigned int doit(unsigned int& src)
    {
        unsigned int raw = to_raw(src);
        unsigned int res = from_raw(raw);
        return res;
    };
};

// raw: int: argb memory:[bgra]
class DDSToTexConvertor
{
public:
    unsigned int from;
    unsigned int to;
    unsigned int to_raw(unsigned int& src) // inverted if LE
    {
        unsigned int c1 = src & 0xff000000;
        unsigned int c2 = src & 0x00ff0000;
        unsigned int c3 = src & 0x0000ff00;
        unsigned int c4 = src & 0x000000ff;
        unsigned int cc1,cc2,cc3,cc4,cc5,cc6,cc7,cc8;
        switch (from)
        {
        case binpac::DDS::DDS_FORMAT_A4R4G4B4: // argb4444 -> argb4444
            // 2 pixels in uint

            break;
        case binpac::DDS::DDS_FORMAT_R8G8B8 : // rgb -> rgb
            break;
        case binpac::DDS::DDS_FORMAT_A8R8G8B8 : // argb -> argb
            //c1 >>= 2*4;
            //c2 >>= 2*4;
            //c3 >>= 2*4;
            //c4 <<= 6*4;
            break;
        case binpac::DDS::DDS_FORMAT_DXT5 :
            break;
        default:
            break;

        }
        unsigned int res = c1 + c2 + c3 + c4;
        return res;
    };
    unsigned int from_raw(unsigned int& raw) // argb
    {
        unsigned int c1 = raw & 0xff000000; // a
        unsigned int c2 = raw & 0x00ff0000; // r
        unsigned int c3 = raw & 0x0000ff00; // g
        unsigned int c4 = raw & 0x000000ff; // b
        unsigned int cc1,cc2,cc3,cc4,cc5,cc6,cc7,cc8;
        switch (to)
        {
        case binpac::Tex::TEX_FORMAT_ABGR4444: // argb4444 -> abgr4444
            // 2 pixels in uint
            cc1 = c1 & 0xf0000000; // pixel 1
            cc2 = c1 & 0x0f000000;
            cc3 = c2 & 0x00f00000;
            cc4 = c2 & 0x000f0000;
            cc5 = c3 & 0x0000f000; // pixel 2
            cc6 = c3 & 0x00000f00;
            cc7 = c4 & 0x000000f0;
            cc8 = c4 & 0x0000000f;
            //cc1 >>= 3*4; // pixel 1
            cc2 >>= 2*4;
            //cc3 <<= 1*4;
            cc4 <<= 2*4;
            //cc5 >>= 3*4; // pixel 2
            cc6 >>= 2*4;
            //cc7 <<= 1*4;
            cc8 <<= 2*4;
            c1 = cc1 + cc2;
            c2 = cc3 + cc4;
            c3 = cc5 + cc6;
            c4 = cc7 + cc8;
            break;
            //case TEX_FORMAT_ABGR32: // abgr -> argb
            //    //c1 >>= 6*4;
            //    c2 >>= 2*8;
            //    //c3 <<= 2*4;
            //    c4 <<= 2*8;
            //    break;
        /*case binpac::Tex::TEX_FORMAT_RGBA32: // argb -> rgba
            c1 >>= 3*8;
            c2 <<= 1*8;
            c3 <<= 1*8;
            c4 <<= 1*8;
            break;*/
        case binpac::Tex::TEX_FORMAT_BGRA32: // argb -> bgra
            c1 >>= 3*8;
            c2 >>= 1*8;
            c3 <<= 1*8;
            c4 <<= 3*8;
            break;
        case binpac::Tex::TEX_FORMAT_DXT1: // we cannot do something with compressed data
            break;
        case binpac::Tex::TEX_FORMAT_DXT5: // we cannot do something with compressed data
            break;
        default:
            break;
        }
        unsigned int res = c1 + c2 + c3 + c4;
        return res;
    };

    unsigned int doit(unsigned int& src)
    {
        unsigned int raw = to_raw(src);
        unsigned int res = from_raw(raw);
        return res;
    };
};

#endif // DDS_HPP
