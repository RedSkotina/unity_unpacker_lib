analyzer Tga withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

# main storage
type Pac = record {
  global_header: GlobalHeader;
  image_colormap: Image_ColorMap(global_header); 
 }  &byteorder=littleendian;


type GlobalHeader = record {
    image_ident_field_length: uint8;
    colormap_type: uint8;
    image_type: uint8;
    colormap_spec: ColorMap_Spec;
    image_spec: Image_Spec;
} 

type ColorMap_Spec = record {
    colormap_origin: uint16;
    colormap_length: uint16;
    colormap_entry_size: uint8;
}

type Image_Spec = record {
    x_origin: uint16;
    y_origin: uint16;
    width: uint16;
    height: uint16;
    image_pixel_size: uint8;
    image_descriptor: uint8;
}

type Image_ColorMap(global_header: GlobalHeader) = record {
    image_ident_field: uint8[global_header.image_ident_field_length];
    colormap_data_or_not: case global_header.colormap_type of {
        1 -> colormap_data: ColorMap_Data(global_header.colormap_spec);
        0 -> none:empty;
    };
    image_pixels_or_indicies: case global_header.colormap_type of {
        1 -> image_indicies: Image_Data_Indicies(global_header.image_spec);
        0 -> image_pixels: Image_Data_Pixels(global_header.image_spec);
    };   
} ;

####  ColorMap
type ColorMap_Data(colormap_spec: ColorMap_Spec) = record {
    entries: bytestring &transient &length=colormap_spec.colormap_entry_size * colormap_spec.colormap_length;
} 

#### Image WITHOUT ColorMap
type Image_Data_Pixels(image_spec: Image_Spec) = record {
    pixels: bytestring &transient &length=(image_spec.image_pixel_size/8) * image_spec.width * image_spec.height;

} 


#### Image WITH ColorMap
type Image_Data_Indicies(image_spec: Image_Spec) = record {
       indicies: bytestring &transient &length=(image_spec.image_pixel_size/8) * image_spec.width * image_spec.height ;
} 