analyzer PVR withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

# main storage
type Pac = record {
  header: PVR_HEADER;
  
  header10_or_not: case ext.hasHeader10(header) of {
    true -> header10: DDS_HEADER_DXT10;
	false -> none:empty;
  };
  bdata:  bytestring &transient &length=nbytes;
  bdata2: bytestring &transient &length=nbytes2;
 } &let {
     compressed: bool = ((header.ddspf.dwFlags & DDPF_FOURCC) != 0)? true: false;
     numBlocks:uint32 = ((header.dwWidth + 3)/4) * ((header.dwHeight + 3)/4);
     format:uint32 = ext.detect_format(compressed, header);
     numComponents:uint32 = ext.calc_num_components(format);
     nbytes: uint32 = (compressed == true)? ext.get_compressed_size(header.dwWidth, header.dwHeight, format): ext.get_uncompressed_size(header.dwWidth, header.dwHeight, numComponents);
     nbytes2: uint32 = ext.get_mipmaps_total_size(compressed, header.dwWidth, header.dwHeight, format, numComponents, header);
     
 }  &byteorder=littleendian;

type PVR_HEADER = record {
  version: uint32;
  flags: uint32;
  pixel_format: uint64;
  colour_space: uint32;
  channel_type: uint32;
  height: uint32;
  width: uint32;
  depth: uint32;
  num_surfaces: uint32;
  num_faces: uint32;
  mipmap_count: uint32;
  metadata_size: uint32;
}
// The exact value of ‘Version’ will be one of
//the following depending upon the endianess of the file, and the computer reading it
enum PVR_VERSION {
    ENDIANESS_NOT_MATCH = 0x03525650,
    ENDIANESS_MATCH     = 0x50565203
}
enum PVR_FLAGS {
    NOFLAG          = 0x0, //No flag has been set. 
    PREMULTIPLIED   = 0x2  //When this flag is set, colour values within the texture have been pre-multiplied by the alpha values. 
}
enum PVR_PIXEL_FORMAT {
    PVRTC_2bpp_RGB  = 0,
    PVRTC_2bpp_RGBA = 1,
    PVRTC_4bpp_RGB  = 2,
    PVRTC_4bpp_RGBA = 3,
    PVRTC-II_2bpp   = 4,
    PVRTC-II_4bpp   = 5,
    ETC1            = 6,
    DXT1            = 7,
    DXT2            = 8,
    DXT3            = 9,
    DXT4            = 10,
    DXT5            = 11,
    //BC1             = 7, duplicate format ?
    //BC2             = 9, duplicate format ?
    //BC3             = 11,duplicate format ? 
    BC4             = 12,
    BC5             = 13,
    BC6             = 14,
    BC7                 = 15,
    UYVY                = 16,
    YUY2                = 17,
    BW1bpp              = 18,
    R9G9B9E5_Shared_Exponent = 19,
    RGBG8888            = 20,
    GRGB8888            = 21,
    ETC2_RGB            = 22,
    ETC2_RGBA           = 23,
    ETC2_RGB_A1         = 24,
    EAC_R11_Unsigned    = 25,
    EAC_R11_Signed      = 26,
    EAC_RG11_Unsigned   = 27,
    EAC_RG11_Signed     = 28 
}


extern type ext;
extern type stdstring;
%header{
	#include <string>
	#include <custom/custom.h>
    typedef std::string stdstring; 
	
    const unsigned long FOURCC_DXT1 = 0x31545844; //(MAKEFOURCC('D','X','T','1'))
    const unsigned long FOURCC_DXT3 = 0x33545844; //(MAKEFOURCC('D','X','T','3'))
    const unsigned long FOURCC_DXT5 = 0x35545844; //(MAKEFOURCC('D','X','T','5'))
    const unsigned long FOURCC_DX10 = 0x30315844; //(MAKEFOURCC('D','X','1','0'))
    const unsigned long DDS_MAGIC_NUMBER = 0x20534444; // (MAKEFOURCC('D','D','S',' '))
	class Ext{
	public:
		Ext(){};
    static bool hasHeader10(DDS_HEADER* header)
    {
        if ((header->ddspf()->dwFlags() & DDPF_FOURCC) && (header->ddspf()->dwFourCC() == FOURCC_DX10))
            return true;
        return false;
    }
	static uint32 detect_format(bool compressed, DDS_HEADER* header)
    {
        uint32 format;
        const uint32 DDS_RGBA =  DDPF_RGB & DDPF_ALPHAPIXELS;
        if (compressed)
        switch ( header->ddspf()->dwFourCC() )
        {
        case FOURCC_DXT1:
            if ( header->ddspf()->dwFlags() & DDPF_ALPHAPIXELS ) 
                format = DDS_FORMAT_DXT1; //GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
            else
                format = DDS_FORMAT_DXT1 ; //GL_COMPRESSED_RGB_S3TC_DXT1_EXT
            break;

        case FOURCC_DXT3:
            format     = DDS_FORMAT_DXT3;
            break;

        case FOURCC_DXT5:
            format    = DDS_FORMAT_DXT5;
            break;

        default:
            format = DDS_FORMAT_UNKNOWN;
        }
        else //uncompressed
        switch ( header->ddspf()->dwFlags() )
        {
        case DDS_RGBA:
            if (header->ddspf()->dwRGBBitCount() == 32)
                format = DDS_FORMAT_A8R8G8B8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;

        case DDPF_RGB:
            if ( header->ddspf()->dwRGBBitCount() == 32 ) 
                format = DDS_FORMAT_A8R8G8B8;
            else if ( header->ddspf()->dwRGBBitCount() == 24 )
                format = DDS_FORMAT_R8G8B8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;

        default:
            format = DDS_FORMAT_UNKNOWN;
        }    
        
        return format;
    }
    static uint32 get_mipmaps_total_size(bool compressed, uint32 width, uint32 height, uint32 format, uint32 num_components, DDS_HEADER* header )
    {
        uint32 h = height >> 1;
        uint32 w = width >> 1;
        uint32 size = 0;
        uint32 total_size = 0;
        
        for (unsigned int i = 0; i < header->dwMipMapCount() && (w || h); i++)
        {
            if (compressed)
                size = get_compressed_size(w, h, format);
            else
                size = get_uncompressed_size(w, h, num_components);
            
            total_size += size;

            h >>= 1;
            w >>= 1;
        }
        
        return total_size;
    }
    static uint32 get_compressed_size(uint32 width, uint32 height, uint32 format)
    {
        uint32 size = ((width+3)/4)*((height+3)/4) *  (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT? 8: 16);
        return size;
    }
    static uint32 get_uncompressed_size(uint32 width, uint32 height, uint32 num_components)
    {
        uint32 size = width * height * num_components;
        return size;
    }
	static uint32 calc_num_components(uint32 format)  
	{
        uint32 num_components;
		switch ( format )
        {
        case GL_BGRA_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            num_components = 4;
            break;
        case GL_BGR_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
            num_components = 3;
            break;
        default:
            num_components = 4;
        }
        return num_components;
	}
    };

	static Ext ext;
	
%}
%code{
//	stdstring Ext::extension="init";
//	stdstring Ext::assets_filename="assets";
%}
