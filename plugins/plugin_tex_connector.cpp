#include <Pluma/Connector.hpp>
#include "plugin_tex.hpp"

/////////////////////////////////////////////////////////
/// Plugin connects to hosts through this function
/// Add Eagle and Jaguar providers to the host, so that it
/// can create and use those kind of warriors
/////////////////////////////////////////////////////////
PLUMA_CONNECTOR
bool connect(pluma::Host& host){
    host.add( new PluginTexProvider() );
    return true;
}
