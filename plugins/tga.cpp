#include "tga.hpp"
#include "DebugLog.hpp"
Tga::Tga()
{
}

Tga::~Tga()
{
}
int Tga::save(MemoryMappedFile* dest_mmf, MemoryMappedFile* src_mmf,size_t src_offset,size_t width,size_t height,size_t size,size_t format)
{
    DEBUG_METHOD();
    binpac::Tga::Connection* tga_connection = new binpac::Tga::Connection();
    binpac::Tga::Pac* tga = new binpac::Tga::Pac(); 
    
    binpac::Tga::GlobalHeader* global_header = new binpac::Tga::GlobalHeader();
    global_header->set_image_ident_field_length(0);
    global_header->set_colormap_type(0);
    global_header->set_image_type(2);
    binpac::Tga::ColorMap_Spec* colormap_spec = new binpac::Tga::ColorMap_Spec();
    global_header->set_colormap_spec(colormap_spec);
    binpac::Tga::Image_Spec* image_spec = new binpac::Tga::Image_Spec();
    image_spec->set_width(width);
    image_spec->set_height(height);
    image_spec->set_image_pixel_size(32);
    image_spec->set_image_descriptor(0x2f);
    global_header->set_image_spec(image_spec);
    tga->set_global_header(global_header);
    
    binpac::Tga::Image_ColorMap* image_colormap = new binpac::Tga::Image_ColorMap(global_header);
    vector<binpac::uint8>* image_ident_field = new vector<binpac::uint8>();
    image_colormap->set_image_ident_field(image_ident_field);
    image_colormap->set_colormap_data_or_not_case_index(0);
    
    
    unsigned int unpack_size;
    unsigned char a,r,g,b;
    unsigned int argb;
    size_t pixels_count = size / 4;
    unsigned int compression_type = format;
    DirectMemory* temp_pixels = new DirectMemory();
    if (compression_type == 2)
    {
        temp_pixels->Open(size*2);
        unpack_size = size*2 + 100; // header 100
    }
    else if (compression_type == 12)
    {
        // width * height not correct in tex ? must be divide by four ?
        tga->global_header()->image_spec()->set_width(width/2);
        tga->global_header()->image_spec()->set_height(height/2);
        temp_pixels->Open(size);
        unpack_size = size + 100; // header 100
    }
    else
    {
        temp_pixels->Open(size);
        unpack_size = size + 100; // header 100
    }
    switch (compression_type)
    {
	case 5: // argb -> rgba -> convert to LE() = argb -> bgra
        for (unsigned int k = 0; k < pixels_count; k++)
        {
            src_mmf->Read(&b, src_offset + k*4 + 3);
            (*temp_pixels)[k*4+0] = b;
            src_mmf->Read(&g, src_offset + k*4 + 2);
            (*temp_pixels)[k*4+1] = g;
            src_mmf->Read(&r, src_offset + k*4 + 1);
            (*temp_pixels)[k*4+2] = r;
            src_mmf->Read(&a, src_offset + k*4 + 0);
            (*temp_pixels)[k*4+3] = a;
            
        }
        break;
    case 4:  // rgba -> rgba -> convert to LE() = rgba -> bgra
        for (unsigned int k = 0; k < pixels_count; k++)
        {
            src_mmf->Read(&b, src_offset + k*4 + 2);
            (*temp_pixels)[k*4+0] = b;
            src_mmf->Read(&g, src_offset + k*4 + 1);
            (*temp_pixels)[k*4+1] = g;
            src_mmf->Read(&r, src_offset + k*4 + 0);
            (*temp_pixels)[k*4+2] = r;
            src_mmf->Read(&a, src_offset + k*4 + 3);
            (*temp_pixels)[k*4+3] = a;
            
        }
        break;
    case 2:  // rgba4444 -> rgba -> convert to LE() = rgba4444 -> bgra
        pixels_count = size / 2;
        for (unsigned int k = 0; k < pixels_count; k++)
        {
            src_mmf->Read(&r, src_offset + k*2 + 0);
            (*temp_pixels)[k*4+2] = r & 240; // 11110000
            (*temp_pixels)[k*4+1] = r & 15;  // 00001111
            
            src_mmf->Read(&a, src_offset + k*2 + 1);
            (*temp_pixels)[k*4+0] = a & 240;
            (*temp_pixels)[k*4+3] = a & 15;            
            
        }
        break;
    case 12:  // dxt5 -> rgba -> convert to LE() = dxt5 -> bgra
        pixels_count = size/4;
        for (unsigned int k = 0; k < pixels_count; k++)
        {
            src_mmf->ReadDWord(&argb, src_offset + k*4 + 0);
            (*temp_pixels)[k*4+0] = (unsigned char)((argb & 0xff));
            (*temp_pixels)[k*4+1] = (unsigned char)((argb & 0xff00)>>8);  
            (*temp_pixels)[k*4+2] = (unsigned char)((argb & 0xff0000)>>16);
            (*temp_pixels)[k*4+3] = (unsigned char)((argb & 0xff000000)>>24);
        }
        break;
	default:
        DEBUG_MESSAGE("Error: invalid compression type");
        DEBUG_VALUE_OF(compression_type);
        break;
    }   
    
    
    binpac::const_bytestring tga_pixels(temp_pixels->Begin(), size);
    tga_pixels.set_mem_strg(temp_pixels);
    
    binpac::Tga::Image_Data_Pixels* image_data_pixels = new binpac::Tga::Image_Data_Pixels(image_spec);
    image_data_pixels->set_pixels(tga_pixels);
    image_colormap->set_image_pixels_or_indicies_case_index(1);
    image_colormap->set_image_pixels(image_data_pixels);
    tga->set_image_colormap(image_colormap);
    
    // INIT CODE FOR STORING
    MemoryMappedFile::SetSelf(dest_mmf);
    binpac::Tga::ADD_STORE_PIECE_FPTR = &(MemoryMappedFile::WrapperAddStorePiece); 
    binpac::IMemoryStorageSingleton::instance()->set_mem_strg(dest_mmf); 
    
    size_t store_size = tga_connection->store(true, dest_mmf->Begin(), dest_mmf->Begin() + unpack_size, tga);
    dest_mmf->SetSize(store_size);
    
    binpac::const_bytestring pixels =(binpac::const_bytestring)(tga->image_colormap()->image_pixels()->pixels());
    DirectMemory* mem_strg = (DirectMemory*)(pixels.mem_strg());
    delete mem_strg;
}
