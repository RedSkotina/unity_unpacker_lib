#ifndef TGA_HPP
#define TGA_HPP
#include "proto/tga_pac.h"
#include "memory_mapped_file/memory_mapped_file.h"
class Tga
{
public:
    Tga();
    ~Tga();
    int save(MemoryMappedFile* dest_mmf, MemoryMappedFile* src_mmf, size_t src_offset,size_t width,size_t height,size_t size,size_t format);
};

#endif // TGA_HPP
