#include "plugin_tex.hpp"
#include <algorithm>

#include <list>
#include <fstream>
//DEBUG_TO_FILE("c:\\totalcmd\\Plugins\\wcx\\Unity\\plugin-tex-log.txt");
std::string get_current_dir();
DEBUG_TO_FILE((get_current_dir()+std::string("\\plugin-tex-log.txt")).c_str());
/*
//Convert an array of four bytes into a 32-bit integer.
DWORD BytesToDword(BYTE *b, bool LittleEndian = 1 )
{
    if (LittleEndian)
        return (b[0]) | (b[1] << 8) | (b[2] << 16) | (b[3] << 24);
    else
        return (b[0]<<24) | (b[1] << 16) | (b[2] << 8) | (b[3] );
}
void DwordToBytes(DWORD a, BYTE *b, bool LittleEndian = 1 )
{
    if (LittleEndian)
    {
        b[3] = (a >> 24) & 0xff;
        b[2] = (a >> 16) & 0xff;
        b[1] = (a >> 8) & 0xff;
        b[0] = a & 0xff ;
    }
    else
    {
        b[0] = (a >> 24) & 0xff;
        b[1] = (a >> 16) & 0xff;
        b[2] = (a >> 8) & 0xff;
        b[3] = a & 0xff ;
    }
}

void WordToBytes(WORD a, BYTE *b, bool LittleEndian = 1 )
{
    if (LittleEndian)
    {
        b[1] = (a >> 8) & 0xff;
        b[0] = a & 0xff ;
    }
    else
    {
        b[0] = (a >> 8) & 0xff;
        b[1] = a & 0xff ;
    }
}

void* TryPtrOrDie(void* ptr)  // TEMPLATE !!!!!
{
    assert(ptr!=nullptr);
    return ptr;
}
void* TryHandleOrDie(void* ptr)
{
    if (ptr == INVALID_HANDLE_VALUE)
        throw std::bad_exception();
    return ptr;
}
void TryReadResultOrDie(int must_read_bytes,int readed_bytes)
{
    if(must_read_bytes!=readed_bytes) throw std::system_error(std::error_code());
    return;
}
*/

bool PluginTex::mem_can_handle(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();

    BOOL can_handle = true;

    TexStorageState* plugin_state;
    try
    {
        plugin_state = (TexStorageState*)mem_open(mem);
    }
    catch (...)
    {
        DEBUG_MESSAGE("Unhandled Exception");
        can_handle = false;
    }
    if (plugin_state != NULL)
    {
        binpac::Tex::GlobalHeader* gheader = plugin_state->connection->upflow()->GetDataUnit()->global_header();
        unsigned int img_size= gheader->height() * gheader->width();
        // TODO: rewrite
        if (img_size == 0 || ( (gheader->image_size() != 4 * img_size) && (gheader->image_size() != 2 * img_size) && (gheader->image_size() != img_size)))
            can_handle = false;
        if (gheader->bytes() !=  gheader->image_size() )
            can_handle = false;
        
        if (gheader->mipmap() >= 1 && gheader->bytes() == gheader->image_size() && gheader->image_size() != 0 )
            can_handle = true;
            
        if (gheader->compression_type() == 10 && gheader->bytes() == gheader->image_size() && gheader->image_size() != 0)
            can_handle = true;
        
        if (gheader->compression_type() == 12 && gheader->bytes() == gheader->image_size() && gheader->image_size() != 0)
            can_handle = true;
            
            
    }
    else
    {
        DEBUG_MESSAGE("plugin_state == NULL");
        can_handle = false;
        return can_handle;
    }
    //MemoryMappedFile* tmmf = plugin_state->mmf;
    mem_close(plugin_state);

    //tmmf->FinalClose();
    //delete tmmf;

    return can_handle;
}

std::unique_ptr<Memory> PluginTex::get_mem(StorageState* plugin_state)
{
    DEBUG_METHOD();
    DEBUG_MESSAGE("Not implemented");
    std::unique_ptr<Memory> nil(nullptr);
    return nil;

}
std::unique_ptr<Memory> PluginTex::get_mem_by_name(StorageState* plugin_state, std::string name)
{
    DEBUG_METHOD();
    DEBUG_MESSAGE("Not implemented");
    std::unique_ptr<Memory> nil(nullptr);
    return nil;
}
std::unique_ptr<Memory> PluginTex::mem_pre_open(std::string& abs_filename)
{
    DEBUG_METHOD();
    try
    {
        std::shared_ptr<MemoryMappedFile> mmf(new MemoryMappedFile(64*16) , DeleteMemoryMappedFile ); // 64 Mb
        mmf->Open(abs_filename);
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf.get());
        auto mem = make_unique<Memory>();
        mem->begin = binpac::const_byteptr(mmf->Begin());
        mem->end = binpac::const_byteptr(mmf->Begin()+mmf->GetSize());
        mem->mmf = mmf;
        mem->name = extract_filename_from_absolute_filename(abs_filename);
        return mem;
    }
    catch (...)
    {
        DEBUG_MESSAGE("Error: unhandled exception");
        return nullptr;
    }
}
StorageState* PluginTex::mem_open(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    try
    {
        std::shared_ptr<MemoryMappedFile> mmf = mem->mmf;

        binpac::Tex::Ext::tex_filename = mem->name;
        binpac::Tex::Ext::extension = std::string(".dds");
        DEBUG_MESSAGE("Processing tex image");

        auto connection = make_unique<binpac::Tex::Connection>();
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf.get()); // IMPORTANT: Before using binpac we need set IMemoryStorage for offset_pr
        connection->parse(true, mem->begin, mem->end);

        TexStorageState* plugin_state = new TexStorageState();
        plugin_state->filename = mem->name;
        plugin_state->mmf = mmf;
        plugin_state->connection =  std::move(connection);

        return (StorageState*)plugin_state;

    }
    catch (binpac::Exception& exc)
    {
        DEBUG_MESSAGE("Error: binpac error");
        DEBUG_MESSAGE(exc.c_msg());
        return NULL;
    }
    catch (std::system_error& exc)
    {
        DEBUG_MESSAGE("Error: system error");
        DEBUG_MESSAGE(exc.what());
        return NULL;
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE("Error: runtime error");
        DEBUG_MESSAGE(exc.what());
        return NULL;
    }
    catch (...)
    {
        DEBUG_MESSAGE("Error: unhandled exception");
        return NULL;
    }
}

pHeaderData* PluginTex::next_item(StorageState* plugin_state)
{
    DEBUG_METHOD();
    TexStorageState* state = (TexStorageState*)plugin_state;
    binpac::Tex::Pac* pac = state->connection->upflow()->GetDataUnit();
    if (state->counter >= 1) // only 1 image in tex
    {
        return nullptr;
        plugin_state->counter = 0;
    }
    DWORD i = state->counter;
    pHeaderData* header = new pHeaderData();

    //size_t filename_len = state->filename.find_last_of('.') - state->filename.find_last_of('\\') - 1;
    //std::string filename_root = state->filename.substr(state->filename.find_last_of('\\') + 1, filename_len);

    header->arc_name  = state->filename;
    header->file_name = pac->fullname();
    header->file_time =0; // minutes
    //DEBUG_MESSAGE("ffd");
    header->pack_size = pac->global_header()->file_size();
    header->unp_size  = pac->global_header()->file_size() - 38 + 110;

    state->counter++;

    return header;//ok
};


bool PluginTex::unpack(StorageState* plugin_state, string dst_filename)
{
    DEBUG_METHOD();
    TexStorageState* state = (TexStorageState*)plugin_state;
    binpac::Tex::Pac* tex = state->connection->upflow()->GetDataUnit();
    int i = state->counter - 1;

    //minIni ini(state->filename + ".ini");
    //std::string s_unity_version = ini.gets( "", "unity_version" , "4.3" );
    //DEBUG_MESSAGE("unity_version");
    //DEBUG_VALUE_OF(s_unity_version);
    //MemoryMappedFile dest_mmf(8*16); // 8 Mb
    std::shared_ptr<MemoryMappedFile> dst_mmf(new MemoryMappedFile(8*16) , DeleteMemoryMappedFile ); // 8 Mb

    try
    {
        dst_mmf->Open(dst_filename);
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE(exc.what());
        return false;
    }

    dst_mmf->SetSize(4); // 18 tga header

    DEBUG_MESSAGE("unpack: read src");

    size_t src_offset = tex->image_pixels()->pixels().begin() - state->mmf->Begin();
    size_t width = tex->global_header()->width();
    size_t height = tex->global_header()->height();
    size_t size = tex->global_header()->image_size();
    size_t format = tex->global_header()->compression_type();
    bool has_mipmap = tex->global_header()->mipmap();
    //Tga tga;
    //tga.unpack(&dest_mmf, state->mmf, src_offset, width, height, size, format);

    DDSUtil dds;
    dds.tex_to_dds(dst_mmf, state->mmf, src_offset, width, height, size, has_mipmap, format);

    DEBUG_MESSAGE("TEST2");

    return true;//ok
};

int PluginTex::mem_close(StorageState* plugin_state)
{
    DEBUG_METHOD();
    TexStorageState* state = (TexStorageState*) plugin_state;
    if (!state)
    {
        return 0;
    }
    if (state->connection)
    {
        state->connection.reset();
    }
    if (state)
    {
        delete state;
        state = nullptr;
    }

    return 0;// ok
};
int PluginTex::mem_post_close(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    mem->mmf.reset();

    return 0;// ok
};
//-------------------------------------------------------------

std::shared_ptr<MemoryMappedFile> PluginTex::t_pack(std::unique_ptr<Memory>& arc_mem, std::string& src_path, std::list<std::string> pack_list)
{
    DEBUG_METHOD();
    TexStorageState *state;
    std::shared_ptr<MemoryMappedFile> dst_mmf(nullptr);

    bool direct_using_arc = false;
    if (! arc_mem->mmf)
    {
        direct_using_arc = true;
    }

    for(std::list<std::string>::iterator pack_list_it = pack_list.begin(); pack_list_it != pack_list.end(); pack_list_it++)
    {
        std::string pack_file = *pack_list_it;

        if ( direct_using_arc)
        {
            auto mem = mem_pre_open(arc_mem->name); // invalid
            if (!mem)
                return false;
            arc_mem = std::move(mem);
        }
        state = (TexStorageState*)mem_open(arc_mem) ;

        auto tex = state->connection->upflow()->GetDataUnit();
        if (tex->fullname() != pack_file )
        {
            mem_close(state);
            if (direct_using_arc)
                mem_post_close(arc_mem);
            DEBUG_MESSAGE("Not found file with name " + pack_file);
            return nullptr;
        }

        DEBUG_MESSAGE("tex_Pack: Create Dst File");

        std::string dst_abs_filename;
        if (direct_using_arc)
        {
            dst_mmf = state->mmf;
            dst_abs_filename = arc_mem->name;
        }
        else
        {
            // take temp path
            DWORD buffer_len = 255;
            char buffer[buffer_len];
            GetTempPath(buffer_len, buffer);
            std::string tmp_path = buffer;
            // open temp file
            std::shared_ptr<MemoryMappedFile> tmp_mmf(new MemoryMappedFile(8*16), DeleteMemoryMappedFile);
            dst_mmf = tmp_mmf;
            dst_abs_filename = tmp_path + arc_mem->name;
            dst_mmf->Open(dst_abs_filename, false);
        }




        std::shared_ptr<MemoryMappedFile> src_mmf(new MemoryMappedFile(8*16),DeleteMemoryMappedFile); // 8 Mb
        std::string src_abs_filename = src_path + pack_file;
        src_mmf->Open(src_abs_filename, true);
        if (!direct_using_arc)
        {
            //copy arc mem to dst file
            CopyFile(dst_mmf.get(),arc_mem.get());
        }
        // modify dst file
        // Read src file
        DEBUG_MESSAGE("Processing dds image");

        DDSUtil dds;
        dds.dds_to_tex(dst_mmf, src_mmf, state->connection);

        mem_close(state);
        if (direct_using_arc)
            mem_post_close(arc_mem);
        //res_mmf = dst_mmf;
    }
    return dst_mmf; //ok
}

/*
bool unpack_v1()
{

    vector<binpac::Tga::Image_Pixel*>* image_pixels = new vector<binpac::Tga::Image_Pixel*>();

    for (unsigned int k = 0; k < pixels_count; k++)
        {
            binpac::Tga::Image_Pixel32* image_pixel32 = new binpac::Tga::Image_Pixel32();
            state->mmf->Read(&b, src_offset + k*4 + 3);
            image_pixel32->set_b(b);
            state->mmf->Read(&g, src_offset + k*4 + 2);
            image_pixel32->set_g(g);
            state->mmf->Read(&r, src_offset + k*4 + 1);
            image_pixel32->set_r(r);
            state->mmf->Read(&a, src_offset + k*4 + 0);
            image_pixel32->set_a(a);
            binpac::Tga::Image_Pixel* image_pixel = new binpac::Tga::Image_Pixel(image_spec->image_pixel_size());
            image_pixel->set_image_pixel32(image_pixel32);
            image_pixels->push_back(image_pixel);
        }

    image_data_pixels->set_pixels(image_pixels);


}
 */
/*
bool unpack_v2(binpac::Tga::Pac* tga, binpac::Tex::Pac* tex, TexStorageState* state)
{
     vector<binpac::uint8>* image_pixels = new vector<binpac::uint8>();


    image_pixels->resize( tex->global_header()->image_size() );
    binpac::uint8* dst_base = &((*image_pixels)[0]);

       for (unsigned int k = 0; k < pixels_count; k++)
        {
            state->mmf->Read(&b, src_offset + k*4 + 3);
            memcpy(&dst_base[k*4+0],&b,1);
            state->mmf->Read(&g, src_offset + k*4 + 2);
            memcpy(&dst_base[k*4+1],&g,1);
            state->mmf->Read(&r, src_offset + k*4 + 1);
            memcpy(&dst_base[k*4+2],&r,1);
            state->mmf->Read(&a, src_offset + k*4 + 0);
            memcpy(&dst_base[k*4+3],&a,1);
        }

    image_data_pixels->set_pixels(image_pixels);

}
*/
