#include "plugin.hpp"
#include "proto/plugin_uogm_pac.h"
#include "proto/tga_pac.h"
#include <assert.h>
#include <exception>
#include <stdexcept>
#include <system_error>
#include <memory>
#include "DebugLog.hpp"
#include "wcxhead.h"
#include "minIni/minIni.h"
#include <common.h>

DEBUG_USING_NAMESPACE
class UOgmStorageState: public StorageState
{
public:
    std::unique_ptr<binpac::UOgm::Connection> connection;
};

class PluginUOgm: public Storage
{
public:
    std::string description()
    {
        return "UOgm: unity container for media";
    };
    virtual StorageID get_id()
    {
        StorageID id;
        id.parent_id = 0;
        id.id = 2;
        return id;
    };
    bool mem_can_handle(std::unique_ptr<Memory>& mem);
    std::unique_ptr<Memory> get_mem(StorageState* plugin_state);
    std::unique_ptr<Memory> get_mem_by_name(StorageState* plugin_state, std::string name);
    std::unique_ptr<Memory> mem_pre_open(std::string& abs_filename);
    StorageState* mem_open(std::unique_ptr<Memory>& mem);
    int mem_post_close(std::unique_ptr<Memory>&  mem);
    int mem_close(StorageState* plugin_state) ;
    bool unpack(StorageState* plugin_state, string dst_filename) ;
    std::shared_ptr<MemoryMappedFile> t_pack (std::unique_ptr<Memory>& arc_mem, std::string& src_path, std::list<std::string> pack_list);
    pHeaderData* next_item(StorageState* plugin_state) ;
};
PLUMA_INHERIT_PROVIDER(PluginUOgm, Storage);
