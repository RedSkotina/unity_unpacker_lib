// This file is automatically generated from ./plugins/dds.pac.

#ifndef __plugins_dds_pac_h
#define __plugins_dds_pac_h

#include <vector>

#include "binpac.h"

namespace binpac {

namespace DDS {
class ContextDDS;
class Connection;
class Flow;
class Pac;
class DDS_HEADER;
class DDS_PIXELFORMAT;
class DDS_HEADER_DXT10;
enum DDS_HEADER_FLAGS {
	DDSD_CAPS = 1,
	DDSD_HEIGHT = 2,
	DDSD_WIDTH = 4,
	DDSD_PITCH = 8,
	DDSD_PIXELFORMAT = 4096,
	DDSD_MIPMAPCOUNT = 131072,
	DDSD_LINEARSIZE = 524288,
	DDSD_DEPTH = 8388608,
};
enum DDSCAPS_FLAGS {
	DDSCAPS_COMPLEX = 8,
	DDSCAPS_MIPMAP = 4194304,
	DDSCAPS_TEXTURE = 4096,
};
enum PIXELFORMAT_FLAGS {
	DDPF_ALPHAPIXELS = 1,
	DDPF_ALPHA = 2,
	DDPF_FOURCC = 4,
	DDPF_RGB = 64,
	DDPF_YUV = 512,
	DDPF_LUMINANCE = 131072,
};
enum DXGI_FORMAT {
	DXGI_FORMAT_UNKNOWN = 0,
	DXGI_FORMAT_R32G32B32A32_TYPELESS = 1,
	DXGI_FORMAT_R32G32B32A32_FLOAT = 2,
	DXGI_FORMAT_R32G32B32A32_UINT = 3,
	DXGI_FORMAT_R32G32B32A32_SINT = 4,
	DXGI_FORMAT_R32G32B32_TYPELESS = 5,
	DXGI_FORMAT_R32G32B32_FLOAT = 6,
	DXGI_FORMAT_R32G32B32_UINT = 7,
	DXGI_FORMAT_R32G32B32_SINT = 8,
	DXGI_FORMAT_R16G16B16A16_TYPELESS = 9,
	DXGI_FORMAT_R16G16B16A16_FLOAT = 10,
	DXGI_FORMAT_R16G16B16A16_UNORM = 11,
	DXGI_FORMAT_R16G16B16A16_UINT = 12,
	DXGI_FORMAT_R16G16B16A16_SNORM = 13,
	DXGI_FORMAT_R16G16B16A16_SINT = 14,
	DXGI_FORMAT_R32G32_TYPELESS = 15,
	DXGI_FORMAT_R32G32_FLOAT = 16,
	DXGI_FORMAT_R32G32_UINT = 17,
	DXGI_FORMAT_R32G32_SINT = 18,
	DXGI_FORMAT_R32G8X24_TYPELESS = 19,
	DXGI_FORMAT_D32_FLOAT_S8X24_UINT = 20,
	DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS = 21,
	DXGI_FORMAT_X32_TYPELESS_G8X24_UINT = 22,
	DXGI_FORMAT_R10G10B10A2_TYPELESS = 23,
	DXGI_FORMAT_R10G10B10A2_UNORM = 24,
	DXGI_FORMAT_R10G10B10A2_UINT = 25,
	DXGI_FORMAT_R11G11B10_FLOAT = 26,
	DXGI_FORMAT_R8G8B8A8_TYPELESS = 27,
	DXGI_FORMAT_R8G8B8A8_UNORM = 28,
	DXGI_FORMAT_R8G8B8A8_UNORM_SRGB = 29,
	DXGI_FORMAT_R8G8B8A8_UINT = 30,
	DXGI_FORMAT_R8G8B8A8_SNORM = 31,
	DXGI_FORMAT_R8G8B8A8_SINT = 32,
	DXGI_FORMAT_R16G16_TYPELESS = 33,
	DXGI_FORMAT_R16G16_FLOAT = 34,
	DXGI_FORMAT_R16G16_UNORM = 35,
	DXGI_FORMAT_R16G16_UINT = 36,
	DXGI_FORMAT_R16G16_SNORM = 37,
	DXGI_FORMAT_R16G16_SINT = 38,
	DXGI_FORMAT_R32_TYPELESS = 39,
	DXGI_FORMAT_D32_FLOAT = 40,
	DXGI_FORMAT_R32_FLOAT = 41,
	DXGI_FORMAT_R32_UINT = 42,
	DXGI_FORMAT_R32_SINT = 43,
	DXGI_FORMAT_R24G8_TYPELESS = 44,
	DXGI_FORMAT_D24_UNORM_S8_UINT = 45,
	DXGI_FORMAT_R24_UNORM_X8_TYPELESS = 46,
	DXGI_FORMAT_X24_TYPELESS_G8_UINT = 47,
	DXGI_FORMAT_R8G8_TYPELESS = 48,
	DXGI_FORMAT_R8G8_UNORM = 49,
	DXGI_FORMAT_R8G8_UINT = 50,
	DXGI_FORMAT_R8G8_SNORM = 51,
	DXGI_FORMAT_R8G8_SINT = 52,
	DXGI_FORMAT_R16_TYPELESS = 53,
	DXGI_FORMAT_R16_FLOAT = 54,
	DXGI_FORMAT_D16_UNORM = 55,
	DXGI_FORMAT_R16_UNORM = 56,
	DXGI_FORMAT_R16_UINT = 57,
	DXGI_FORMAT_R16_SNORM = 58,
	DXGI_FORMAT_R16_SINT = 59,
	DXGI_FORMAT_R8_TYPELESS = 60,
	DXGI_FORMAT_R8_UNORM = 61,
	DXGI_FORMAT_R8_UINT = 62,
	DXGI_FORMAT_R8_SNORM = 63,
	DXGI_FORMAT_R8_SINT = 64,
	DXGI_FORMAT_A8_UNORM = 65,
	DXGI_FORMAT_R1_UNORM = 66,
	DXGI_FORMAT_R9G9B9E5_SHAREDEXP = 67,
	DXGI_FORMAT_R8G8_B8G8_UNORM = 68,
	DXGI_FORMAT_G8R8_G8B8_UNORM = 69,
	DXGI_FORMAT_BC1_TYPELESS = 70,
	DXGI_FORMAT_BC1_UNORM = 71,
	DXGI_FORMAT_BC1_UNORM_SRGB = 72,
	DXGI_FORMAT_BC2_TYPELESS = 73,
	DXGI_FORMAT_BC2_UNORM = 74,
	DXGI_FORMAT_BC2_UNORM_SRGB = 75,
	DXGI_FORMAT_BC3_TYPELESS = 76,
	DXGI_FORMAT_BC3_UNORM = 77,
	DXGI_FORMAT_BC3_UNORM_SRGB = 78,
	DXGI_FORMAT_BC4_TYPELESS = 79,
	DXGI_FORMAT_BC4_UNORM = 80,
	DXGI_FORMAT_BC4_SNORM = 81,
	DXGI_FORMAT_BC5_TYPELESS = 82,
	DXGI_FORMAT_BC5_UNORM = 83,
	DXGI_FORMAT_BC5_SNORM = 84,
	DXGI_FORMAT_B5G6R5_UNORM = 85,
	DXGI_FORMAT_B5G5R5A1_UNORM = 86,
	DXGI_FORMAT_B8G8R8A8_UNORM = 87,
	DXGI_FORMAT_B8G8R8X8_UNORM = 88,
	DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM = 89,
	DXGI_FORMAT_B8G8R8A8_TYPELESS = 90,
	DXGI_FORMAT_B8G8R8A8_UNORM_SRGB = 91,
	DXGI_FORMAT_B8G8R8X8_TYPELESS = 92,
	DXGI_FORMAT_B8G8R8X8_UNORM_SRGB = 93,
	DXGI_FORMAT_BC6H_TYPELESS = 94,
	DXGI_FORMAT_BC6H_UF16 = 95,
	DXGI_FORMAT_BC6H_SF16 = 96,
	DXGI_FORMAT_BC7_TYPELESS = 97,
	DXGI_FORMAT_BC7_UNORM = 98,
	DXGI_FORMAT_BC7_UNORM_SRGB = 99,
	DXGI_FORMAT_AYUV = 100,
	DXGI_FORMAT_Y410 = 101,
	DXGI_FORMAT_Y416 = 102,
	DXGI_FORMAT_NV12 = 103,
	DXGI_FORMAT_P010 = 104,
	DXGI_FORMAT_P016 = 105,
	DXGI_FORMAT_420_OPAQUE = 106,
	DXGI_FORMAT_YUY2 = 107,
	DXGI_FORMAT_Y210 = 108,
	DXGI_FORMAT_Y216 = 109,
	DXGI_FORMAT_NV11 = 110,
	DXGI_FORMAT_AI44 = 111,
	DXGI_FORMAT_IA44 = 112,
	DXGI_FORMAT_P8 = 113,
	DXGI_FORMAT_A8P8 = 114,
	DXGI_FORMAT_B4G4R4A4_UNORM = 115,
	DXGI_FORMAT_FORCE_UINT = -1,
};
enum D3D10_RESOURCE_DIMENSION {
	D3D10_RESOURCE_DIMENSION_UNKNOWN = 0,
	D3D10_RESOURCE_DIMENSION_BUFFER = 1,
	D3D10_RESOURCE_DIMENSION_TEXTURE1D = 2,
	D3D10_RESOURCE_DIMENSION_TEXTURE2D = 3,
	D3D10_RESOURCE_DIMENSION_TEXTURE3D = 4,
};
enum DDS_FORMAT {
	DDS_FORMAT_UNKNOWN = 0,
	DDS_FORMAT_R8G8B8 = 1,
	DDS_FORMAT_A8R8G8B8 = 2,
	DDS_FORMAT_A4R4G4B4 = 7,
	DDS_FORMAT_A8 = 8,
	DDS_FORMAT_DXT1 = 62,
	DDS_FORMAT_DXT3 = 63,
	DDS_FORMAT_DXT5 = 64,
};
enum GL_TEXTURE_INTERNAL_FORMAT {
	GL_UNKNOWN = 0,
	GL_BGR_EXT = 32992,
	GL_BGRA_EXT = 32993,
	GL_BGRA4444_EXT = 32994,
	GL_COMPRESSED_RGB_S3TC_DXT1_EXT = 33776,
	GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = 33777,
	GL_COMPRESSED_RGBA_S3TC_DXT3_EXT = 33778,
	GL_COMPRESSED_RGBA_S3TC_DXT5_EXT = 33779,
};
} // namespace DDS

namespace DDS {

typedef void (*ADD_STORE_PIECE_FUNC)(size_t size);
extern  ADD_STORE_PIECE_FUNC ADD_STORE_PIECE_FPTR;


class ContextDDS
{
public:
	ContextDDS(Connection * connection, Flow * flow);
	~ContextDDS();
	
	// Member access functions
	Connection * connection() const { return connection_; }
	void set_connection( Connection * value) {  connection_ = value; }
	Flow * flow() const { return flow_; }
	void set_flow( Flow * value) {  flow_ = value; }
	
protected:
	Connection * connection_;
	Flow * flow_;
};


class Connection : public binpac::ConnectionAnalyzer
{
public:
	Connection();
	~Connection();
	
	// Member access functions
	Flow * upflow() const { return upflow_; }
	void set_upflow( Flow * value) {  upflow_ = value; }
	Flow * downflow() const { return downflow_; }
	void set_downflow( Flow * value) {  downflow_ = value; }
	
	void NewData(bool is_orig, const_byteptr begin, const_byteptr end);
	void NewGap(bool is_orig, int gap_length);
	void FlowEOF(bool is_orig);
	Pac * parse(bool is_orig, const_byteptr begin, const_byteptr end);
	uint32 store(bool is_orig, const_byteptr begin, const_byteptr end, Pac * dataunit);
	
protected:
	Flow * upflow_;
	Flow * downflow_;
};


class Flow : public binpac::FlowAnalyzer
{
public:
	Flow(Connection * connection);
	~Flow();
	
	// Member access functions
	Connection * connection() const { return connection_; }
	void set_connection( Connection * value) {  connection_ = value; }
	
	void NewData(const_byteptr t_begin_of_data, const_byteptr t_end_of_data);
	void NewGap(int gap_length);
	void FlowEOF();
	Pac * parse(const_byteptr t_begin_of_data, const_byteptr t_end_of_data);
	uint32 store(const_byteptr t_begin_of_data, const_byteptr t_end_of_data, Pac * dataunit);
	void DeleteContext();
	void DeleteDataUnit();
	Pac * GetDataUnit() { return dataunit_; };
	
	// Functions
	const_bytestring pacdata(const_bytestring const & pacdata, uint32 offset, uint32 size);
	
protected:
	Pac * dataunit_;
	ContextDDS * context_;
	Connection * connection_;
};


class Pac
{
public:
	Pac();
	~Pac();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data);
	
	// Member access functions
	uint32 magic_number() const { return magic_number_; }
	void set_magic_number( uint32 value) {  magic_number_ = value; }
	DDS_HEADER * header() const { return header_; }
	void set_header( DDS_HEADER * value) {  header_ = value; }
	int header10_or_not_case_index() const	{ return header10_or_not_case_index_; }
	void set_header10_or_not_case_index( int value) 	{ header10_or_not_case_index_ = value; }
	DDS_HEADER_DXT10 * header10() const
		{
		switch ( header10_or_not_case_index() )
			{
			case 1:
				break;  // OK
			default:
				throw binpac::ExceptionInvalidCase("./plugins/dds.pac:13:header10", header10_or_not_case_index(), "true");
				break;
			}
		return header10_;
		}
	void set_header10( DDS_HEADER_DXT10 * value) {  header10_ = value; }
	const_bytestring const & bdata() const { return bdata_; }
	void set_bdata( const_bytestring value) {  bdata_ = value; }
	const_bytestring const & bdata2() const { return bdata2_; }
	void set_bdata2( const_bytestring value) {  bdata2_ = value; }
	int byteorder() const { return byteorder_; }
	void set_byteorder( int value) {  byteorder_ = value; }
	bool compressed() const { return compressed_; }
	void set_compressed( bool value) {  compressed_ = value; }
	uint32 numBlocks() const { return numBlocks_; }
	void set_numBlocks( uint32 value) {  numBlocks_ = value; }
	uint32 format() const { return format_; }
	void set_format( uint32 value) {  format_ = value; }
	uint32 numComponents() const { return numComponents_; }
	void set_numComponents( uint32 value) {  numComponents_ = value; }
	uint32 nbytes() const { return nbytes_; }
	void set_nbytes( uint32 value) {  nbytes_ = value; }
	uint32 nbytes2() const { return nbytes2_; }
	void set_nbytes2( uint32 value) {  nbytes2_ = value; }
	
protected:
	uint32 magic_number_;
	DDS_HEADER * header_;
	int header10_or_not_case_index_;
	DDS_HEADER_DXT10 * header10_;
	const_bytestring bdata_;
	const_bytestring bdata2_;
	int byteorder_;
	bool compressed_;
	uint32 numBlocks_;
	uint32 format_;
	uint32 numComponents_;
	uint32 nbytes_;
	uint32 nbytes2_;
};


class DDS_HEADER
{
public:
	DDS_HEADER();
	~DDS_HEADER();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	
	// Member access functions
	uint32 dwSize() const { return dwSize_; }
	void set_dwSize( uint32 value) {  dwSize_ = value; }
	uint32 dwFlags() const { return dwFlags_; }
	void set_dwFlags( uint32 value) {  dwFlags_ = value; }
	uint32 dwHeight() const { return dwHeight_; }
	void set_dwHeight( uint32 value) {  dwHeight_ = value; }
	uint32 dwWidth() const { return dwWidth_; }
	void set_dwWidth( uint32 value) {  dwWidth_ = value; }
	uint32 dwPitchOrLinearSize() const { return dwPitchOrLinearSize_; }
	void set_dwPitchOrLinearSize( uint32 value) {  dwPitchOrLinearSize_ = value; }
	uint32 dwDepth() const { return dwDepth_; }
	void set_dwDepth( uint32 value) {  dwDepth_ = value; }
	uint32 dwMipMapCount() const { return dwMipMapCount_; }
	void set_dwMipMapCount( uint32 value) {  dwMipMapCount_ = value; }
	vector<uint32> * dwReserved1() const { return dwReserved1_; }
	void set_dwReserved1( vector<uint32> * value) {  dwReserved1_ = value; }
	DDS_PIXELFORMAT * ddspf() const { return ddspf_; }
	void set_ddspf( DDS_PIXELFORMAT * value) {  ddspf_ = value; }
	uint32 dwCaps() const { return dwCaps_; }
	void set_dwCaps( uint32 value) {  dwCaps_ = value; }
	uint32 dwCaps2() const { return dwCaps2_; }
	void set_dwCaps2( uint32 value) {  dwCaps2_ = value; }
	uint32 dwCaps3() const { return dwCaps3_; }
	void set_dwCaps3( uint32 value) {  dwCaps3_ = value; }
	uint32 dwCaps4() const { return dwCaps4_; }
	void set_dwCaps4( uint32 value) {  dwCaps4_ = value; }
	uint32 dwReserved2() const { return dwReserved2_; }
	void set_dwReserved2( uint32 value) {  dwReserved2_ = value; }
	
protected:
	uint32 dwSize_;
	uint32 dwFlags_;
	uint32 dwHeight_;
	uint32 dwWidth_;
	uint32 dwPitchOrLinearSize_;
	uint32 dwDepth_;
	uint32 dwMipMapCount_;
	vector<uint32> * dwReserved1_;
	uint32 dwReserved1__elem_;
	DDS_PIXELFORMAT * ddspf_;
	uint32 dwCaps_;
	uint32 dwCaps2_;
	uint32 dwCaps3_;
	uint32 dwCaps4_;
	uint32 dwReserved2_;
};


class DDS_PIXELFORMAT
{
public:
	DDS_PIXELFORMAT();
	~DDS_PIXELFORMAT();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	
	// Member access functions
	uint32 dwSize() const { return dwSize_; }
	void set_dwSize( uint32 value) {  dwSize_ = value; }
	uint32 dwFlags() const { return dwFlags_; }
	void set_dwFlags( uint32 value) {  dwFlags_ = value; }
	uint32 dwFourCC() const { return dwFourCC_; }
	void set_dwFourCC( uint32 value) {  dwFourCC_ = value; }
	uint32 dwRGBBitCount() const { return dwRGBBitCount_; }
	void set_dwRGBBitCount( uint32 value) {  dwRGBBitCount_ = value; }
	uint32 dwRBitMask() const { return dwRBitMask_; }
	void set_dwRBitMask( uint32 value) {  dwRBitMask_ = value; }
	uint32 dwGBitMask() const { return dwGBitMask_; }
	void set_dwGBitMask( uint32 value) {  dwGBitMask_ = value; }
	uint32 dwBBitMask() const { return dwBBitMask_; }
	void set_dwBBitMask( uint32 value) {  dwBBitMask_ = value; }
	uint32 dwABitMask() const { return dwABitMask_; }
	void set_dwABitMask( uint32 value) {  dwABitMask_ = value; }
	
protected:
	uint32 dwSize_;
	uint32 dwFlags_;
	uint32 dwFourCC_;
	uint32 dwRGBBitCount_;
	uint32 dwRBitMask_;
	uint32 dwGBitMask_;
	uint32 dwBBitMask_;
	uint32 dwABitMask_;
};


class DDS_HEADER_DXT10
{
public:
	DDS_HEADER_DXT10();
	~DDS_HEADER_DXT10();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	
	// Member access functions
	uint32 dxgiFormat() const { return dxgiFormat_; }
	void set_dxgiFormat( uint32 value) {  dxgiFormat_ = value; }
	uint32 resourceDimension() const { return resourceDimension_; }
	void set_resourceDimension( uint32 value) {  resourceDimension_ = value; }
	uint32 miscFlag() const { return miscFlag_; }
	void set_miscFlag( uint32 value) {  miscFlag_ = value; }
	uint32 arraySize() const { return arraySize_; }
	void set_arraySize( uint32 value) {  arraySize_ = value; }
	uint32 miscFlags2() const { return miscFlags2_; }
	void set_miscFlags2( uint32 value) {  miscFlags2_ = value; }
	
protected:
	uint32 dxgiFormat_;
	uint32 resourceDimension_;
	uint32 miscFlag_;
	uint32 arraySize_;
	uint32 miscFlags2_;
};


	#include <string>
	#include <custom/custom.h>
    typedef std::string stdstring; 
	
    const unsigned long FOURCC_DXT1 = 0x31545844; //(MAKEFOURCC('D','X','T','1'))
    const unsigned long FOURCC_DXT3 = 0x33545844; //(MAKEFOURCC('D','X','T','3'))
    const unsigned long FOURCC_DXT5 = 0x35545844; //(MAKEFOURCC('D','X','T','5'))
    const unsigned long FOURCC_DX10 = 0x30315844; //(MAKEFOURCC('D','X','1','0'))
    const unsigned long DDS_MAGIC_NUMBER = 0x20534444; // (MAKEFOURCC('D','D','S',' '))
	class Ext{
	public:
		Ext(){};
    static bool hasHeader10(DDS_HEADER* header)
    {
        if ((header->ddspf()->dwFlags() & DDPF_FOURCC) && (header->ddspf()->dwFourCC() == FOURCC_DX10))
            return true;
        return false;
    }
	static uint32 detect_format(bool compressed, DDS_HEADER* header)
    {
        uint32 format;
        const uint32 DDS_RGBA =  DDPF_RGB | DDPF_ALPHAPIXELS;
        if (compressed)
        switch ( header->ddspf()->dwFourCC() )
        {
        case FOURCC_DXT1:
            if ( header->ddspf()->dwFlags() & DDPF_ALPHAPIXELS ) 
                format = DDS_FORMAT_DXT1; //GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
            else
                format = DDS_FORMAT_DXT1 ; //GL_COMPRESSED_RGB_S3TC_DXT1_EXT
            break;

        case FOURCC_DXT3:
            format     = DDS_FORMAT_DXT3;
            break;

        case FOURCC_DXT5:
            format    = DDS_FORMAT_DXT5;
            break;

        default:
            format = DDS_FORMAT_UNKNOWN;
        }
        else //uncompressed
        switch ( header->ddspf()->dwFlags() )
        {
        case DDPF_ALPHA:
            if (header->ddspf()->dwRGBBitCount() == 8)
                format = DDS_FORMAT_A8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;
        case DDS_RGBA:
            if (header->ddspf()->dwRGBBitCount() == 32)
                format = DDS_FORMAT_A8R8G8B8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;

        case DDPF_RGB:
            if ( header->ddspf()->dwRGBBitCount() == 32 ) 
                format = DDS_FORMAT_A8R8G8B8;
            else if ( header->ddspf()->dwRGBBitCount() == 24 )
                format = DDS_FORMAT_R8G8B8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;

        default:
            format = DDS_FORMAT_UNKNOWN;
        }    
        
        return format;
    }
    static uint32 get_mipmaps_total_size(bool compressed, uint32 width, uint32 height, uint32 format, uint32 num_components, DDS_HEADER* header )
    {
        uint32 h = height >> 1;
        uint32 w = width >> 1;
        uint32 size = 0;
        uint32 total_size = 0;
        
        for (unsigned int i = 0; i < header->dwMipMapCount() && (w || h); i++)
        {
            if (compressed)
                size = get_compressed_size(w, h, format);
            else
                size = get_uncompressed_size(w, h, num_components);
            
            total_size += size;

            h >>= 1;
            w >>= 1;
        }
        
        return total_size;
    }
    static uint32 get_compressed_size(uint32 width, uint32 height, uint32 format)
    {
        uint32 size = ((width+3)/4)*((height+3)/4) *  (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT? 8: 16);
        return size;
    }
    static uint32 get_uncompressed_size(uint32 width, uint32 height, uint32 num_components)
    {
        uint32 size = width * height * num_components;
        return size;
    }
	static uint32 calc_num_components(uint32 format)  
	{
        uint32 num_components;
		switch ( format )
        {
        case GL_BGRA_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            num_components = 4;
            break;
        case GL_BGR_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
            num_components = 3;
            break;
        default:
            num_components = 4;
        }
        return num_components;
	}
    };

	static Ext ext;
	

} // namespace DDS
}  // namespace binpac
#endif /* __plugins_dds_pac_h */
