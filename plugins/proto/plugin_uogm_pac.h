// This file is automatically generated from ./plugins/plugin_uogm.pac.

#ifndef __plugins_plugin_uogm_pac_h
#define __plugins_plugin_uogm_pac_h

#include <vector>

#include "binpac.h"

namespace binpac {

namespace UOgm {
class ContextUOgm;
class Connection;
class Flow;
class Pac;
class GlobalHeader;
class GlobalFooter;
class Content;
} // namespace UOgm

namespace UOgm {

typedef void (*ADD_STORE_PIECE_FUNC)(size_t size);
extern  ADD_STORE_PIECE_FUNC ADD_STORE_PIECE_FPTR;


class ContextUOgm
{
public:
	ContextUOgm(Connection * connection, Flow * flow);
	~ContextUOgm();
	
	// Member access functions
	Connection * connection() const { return connection_; }
	void set_connection( Connection * value) {  connection_ = value; }
	Flow * flow() const { return flow_; }
	void set_flow( Flow * value) {  flow_ = value; }
	
protected:
	Connection * connection_;
	Flow * flow_;
};


class Connection : public binpac::ConnectionAnalyzer
{
public:
	Connection();
	~Connection();
	
	// Member access functions
	Flow * upflow() const { return upflow_; }
	void set_upflow( Flow * value) {  upflow_ = value; }
	Flow * downflow() const { return downflow_; }
	void set_downflow( Flow * value) {  downflow_ = value; }
	
	void NewData(bool is_orig, const_byteptr begin, const_byteptr end);
	void NewGap(bool is_orig, int gap_length);
	void FlowEOF(bool is_orig);
	Pac * parse(bool is_orig, const_byteptr begin, const_byteptr end);
	uint32 store(bool is_orig, const_byteptr begin, const_byteptr end, Pac * dataunit);
	
protected:
	Flow * upflow_;
	Flow * downflow_;
};


class Flow : public binpac::FlowAnalyzer
{
public:
	Flow(Connection * connection);
	~Flow();
	
	// Member access functions
	Connection * connection() const { return connection_; }
	void set_connection( Connection * value) {  connection_ = value; }
	
	void NewData(const_byteptr t_begin_of_data, const_byteptr t_end_of_data);
	void NewGap(int gap_length);
	void FlowEOF();
	Pac * parse(const_byteptr t_begin_of_data, const_byteptr t_end_of_data);
	uint32 store(const_byteptr t_begin_of_data, const_byteptr t_end_of_data, Pac * dataunit);
	void DeleteContext();
	void DeleteDataUnit();
	Pac * GetDataUnit() { return dataunit_; };
	
	// Functions
	const_bytestring pacdata(const_bytestring const & pacdata, uint32 offset, uint32 size);
	
protected:
	Pac * dataunit_;
	ContextUOgm * context_;
	Connection * connection_;
};


	#include <string>

	//#include <custom/custom.h>
    typedef std::string stdstring; 
	 
	class Ext{
	public:
		Ext(){};
	static stdstring tex_filename;
	static stdstring extension;
	
	stdstring const & getExtension() const{
		return Ext::extension;
	}
	
	static stdstring fullname(const stdstring& name, const stdstring& name_ext  )
	{
		return (name + name_ext);
	}
	
    
	};
	static Ext ext;


class Pac
{
public:
	Pac();
	~Pac();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data);
	
	// Member access functions
	GlobalHeader * global_header() const { return global_header_; }
	void set_global_header( GlobalHeader * value) {  global_header_ = value; }
	Content * content() const { return content_; }
	void set_content( Content * value) {  content_ = value; }
	GlobalFooter * global_footer() const { return global_footer_; }
	void set_global_footer( GlobalFooter * value) {  global_footer_ = value; }
	int byteorder() const { return byteorder_; }
	void set_byteorder( int value) {  byteorder_ = value; }
	stdstring const & extension() const { return extension_; }
	void set_extension( stdstring value) {  extension_ = value; }
	stdstring const & fullname() const { return fullname_; }
	void set_fullname( stdstring value) {  fullname_ = value; }
	
protected:
	GlobalHeader * global_header_;
	Content * content_;
	GlobalFooter * global_footer_;
	int byteorder_;
	stdstring extension_;
	stdstring fullname_;
};


class GlobalHeader
{
public:
	GlobalHeader();
	~GlobalHeader();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	
	// Member access functions
	uint32 dummy1() const { return dummy1_; }
	void set_dummy1( uint32 value) {  dummy1_ = value; }
	uint32 dummy2() const { return dummy2_; }
	void set_dummy2( uint32 value) {  dummy2_ = value; }
	uint32 uid() const { return uid_; }
	void set_uid( uint32 value) {  uid_ = value; }
	uint32 content_size() const { return content_size_; }
	void set_content_size( uint32 value) {  content_size_ = value; }
	uint32 file_size() const { return file_size_; }
	void set_file_size( uint32 value) {  file_size_ = value; }
	uint32 content_offset() const { return content_offset_; }
	void set_content_offset( uint32 value) {  content_offset_ = value; }
	
protected:
	uint32 dummy1_;
	uint32 dummy2_;
	uint32 uid_;
	uint32 content_size_;
	uint32 file_size_;
	uint32 content_offset_;
};


class GlobalFooter
{
public:
	GlobalFooter();
	~GlobalFooter();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data, int t_byteorder);
	
	// Member access functions
	uint32 dummy4() const { return dummy4_; }
	void set_dummy4( uint32 value) {  dummy4_ = value; }
	
protected:
	uint32 dummy4_;
};


class Content
{
public:
	Content(uint32 size);
	~Content();
	int Parse(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data);
	int Store(const_byteptr const t_begin_of_data, const_byteptr const t_end_of_data);
	
	// Member access functions
	const_bytestring const & data() const { return data_; }
	void set_data( const_bytestring value) {  data_ = value; }
	uint32 size() const { return size_; }
	void set_size( uint32 value) {  size_ = value; }
	
protected:
	const_bytestring data_;
	uint32 size_;
};

} // namespace UOgm
}  // namespace binpac
#endif /* __plugins_plugin_uogm_pac_h */
