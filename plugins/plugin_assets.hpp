#include "plugin.hpp"
#include "proto/plugin_assets_pac.h"
#include <assert.h>
#include <exception>
#include <stdexcept>
#include <system_error>
#include <memory>
#include "DebugLog.hpp"
#include "wcxhead.h"
#include "minIni/minIni.h"
#include <common.h>
DEBUG_USING_NAMESPACE
class AssetsStorageState: public StorageState
{
public:
    //binpac::Assets::Pac* pac;
    std::unique_ptr<binpac::Assets::Connection> connection;
};

class PluginAssets: public Storage
{
public:
    std::string description()
    {
        return "Assets: unity main storage";
    };
    virtual StorageID get_id()
    {
        StorageID id;
        id.parent_id = 0;
        id.id = 0;
        return id;
    };
    //bool can_handle(std::string& name);
    bool mem_can_handle(std::unique_ptr<Memory>& mem);
    //StorageState* open(std::string& filename) ;
    std::unique_ptr<Memory> get_mem(StorageState* plugin_state);
    std::unique_ptr<Memory> get_mem_by_name(StorageState* plugin_state, std::string name);
    std::unique_ptr<Memory> mem_pre_open(std::string& abs_filename);
    StorageState* mem_open(std::unique_ptr<Memory>& mem);
    int mem_post_close(std::unique_ptr<Memory>&  mem);
    //int close(StorageState* plugin_state) ;
    int mem_close(StorageState* plugin_state) ;
    bool unpack(StorageState* plugin_state, string dst_filename) ;
    //bool pack(std::string& src_path, std::string& arcname, std::list<char*> pack_list);
    std::shared_ptr<MemoryMappedFile> t_pack (std::unique_ptr<Memory>& arc_mem, std::string& src_path, std::list<std::string> pack_list);
    pHeaderData* next_item(StorageState* plugin_state) ;
};
PLUMA_INHERIT_PROVIDER(PluginAssets, Storage);
