#include "plugin_uogm.hpp"
#include <algorithm>

#include <list>
#include <fstream>
//DEBUG_TO_FILE("c:\\totalcmd\\Plugins\\wcx\\Unity\\plugin-uogm-log.txt");
std::string get_current_dir();
DEBUG_TO_FILE((get_current_dir()+std::string("\\plugin-uogm-log.txt")).c_str());


bool PluginUOgm::mem_can_handle(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();

    BOOL can_handle = true;

    UOgmStorageState* plugin_state;
    try
    {
        plugin_state = (UOgmStorageState*)mem_open(mem);
    }
    catch (...)
    {
        DEBUG_MESSAGE("Unhandled Exception");
        can_handle = false;
    }
    if (plugin_state != NULL)
    {
        const size_t last_dot_idx = mem->name.find_last_of(".");
        std::string  file_extension = mem->name.substr(last_dot_idx+1);
        
        if (file_extension.compare("uogm") != 0 ) //not equal
            can_handle = false;
            
        binpac::UOgm::GlobalHeader* gheader = plugin_state->connection->upflow()->GetDataUnit()->global_header();
        binpac::UOgm::GlobalFooter* gfooter = plugin_state->connection->upflow()->GetDataUnit()->global_footer();
        
        unsigned int dummy1= gheader->dummy1();
        unsigned int dummy2= gheader->dummy2();
        unsigned int dummy4= gfooter->dummy4();
        
        if (dummy1 != 0 || dummy2 !=0 || dummy4 !=0)
            can_handle = false;
        
    }
    else
    {
        DEBUG_MESSAGE("plugin_state == NULL");
        can_handle = false;
        return can_handle;
    }
    mem_close(plugin_state);
    return can_handle;
}

std::unique_ptr<Memory> PluginUOgm::get_mem(StorageState* plugin_state)
{
    DEBUG_METHOD();
    DEBUG_MESSAGE("Not implemented");
    std::unique_ptr<Memory> nil(nullptr);
    return nil;

}
std::unique_ptr<Memory> PluginUOgm::get_mem_by_name(StorageState* plugin_state, std::string name)
{
    DEBUG_METHOD();
    DEBUG_MESSAGE("Not implemented");
    std::unique_ptr<Memory> nil(nullptr);
    return nil;
}
std::unique_ptr<Memory> PluginUOgm::mem_pre_open(std::string& abs_filename)
{
    DEBUG_METHOD();
    try
    {
        std::shared_ptr<MemoryMappedFile> mmf(new MemoryMappedFile(64*16) , DeleteMemoryMappedFile ); // 64 Mb
        mmf->Open(abs_filename);
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf.get());
        auto mem = make_unique<Memory>();
        mem->begin = binpac::const_byteptr(mmf->Begin());
        mem->end = binpac::const_byteptr(mmf->Begin()+mmf->GetSize());
        mem->mmf = mmf;
        mem->name = extract_filename_from_absolute_filename(abs_filename);
        return mem;
    }
    catch (...)
    {
        DEBUG_MESSAGE("Error: unhandled exception");
        return nullptr;
    }
}
StorageState* PluginUOgm::mem_open(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    try
    {
        std::shared_ptr<MemoryMappedFile> mmf = mem->mmf;

        binpac::UOgm::Ext::tex_filename = mem->name;
        binpac::UOgm::Ext::extension = std::string(".ogm");
        DEBUG_MESSAGE("Processing media file");

        auto connection = make_unique<binpac::UOgm::Connection>();
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf.get()); // IMPORTANT: Before using binpac we need set IMemoryStorage for offset_pr
        connection->parse(true, mem->begin, mem->end);

        UOgmStorageState* plugin_state = new UOgmStorageState();
        plugin_state->filename = mem->name;
        plugin_state->mmf = mmf;
        plugin_state->connection =  std::move(connection);
        plugin_state->begin = mem->begin;
        plugin_state->end = mem->end;
        return (StorageState*)plugin_state;

    }
    catch (binpac::Exception& exc)
    {
        DEBUG_MESSAGE("Error: binpac error");
        DEBUG_MESSAGE(exc.c_msg());
        return NULL;
    }
    catch (std::system_error& exc)
    {
        DEBUG_MESSAGE("Error: system error");
        DEBUG_MESSAGE(exc.what());
        return NULL;
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE("Error: runtime error");
        DEBUG_MESSAGE(exc.what());
        return NULL;
    }
    catch (...)
    {
        DEBUG_MESSAGE("Error: unhandled exception");
        return NULL;
    }
}

pHeaderData* PluginUOgm::next_item(StorageState* plugin_state)
{
    DEBUG_METHOD();
    UOgmStorageState* state = (UOgmStorageState*)plugin_state;
    binpac::UOgm::Pac* pac = state->connection->upflow()->GetDataUnit();
    if (state->counter >= 1) // only 1 mediafile in uogm
    {
        return nullptr;
        plugin_state->counter = 0;
    }
    DWORD i = state->counter;
    pHeaderData* header = new pHeaderData();

    header->arc_name  = state->filename;
    header->file_name = pac->fullname();
    header->file_time =0; // minutes
    header->pack_size = pac->global_header()->file_size();
    header->unp_size  = pac->global_header()->file_size();

    state->counter++;

    return header;//ok
};


bool PluginUOgm::unpack(StorageState* plugin_state, string dst_filename)
{
    DEBUG_METHOD();
    UOgmStorageState* state = (UOgmStorageState*)plugin_state;
    binpac::UOgm::Pac* uogm = state->connection->upflow()->GetDataUnit();
    int i = state->counter - 1;

    std::shared_ptr<MemoryMappedFile> dst_mmf(new MemoryMappedFile(8*16) , DeleteMemoryMappedFile ); // 8 Mb

    try
    {
        dst_mmf->Open(dst_filename);
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE(exc.what());
        return false;
    }

    dst_mmf->SetSize(uogm->global_header()->file_size()); 

    DEBUG_MESSAGE("unpack: read src");

    MemMove(dst_mmf.get(),
            state->mmf.get(),
            dst_mmf->Begin(),
            (state->begin + uogm->global_header()->content_offset()).get_fictive_ptr(),
            uogm->global_header()->content_size());

    return true;//ok
};

int PluginUOgm::mem_close(StorageState* plugin_state)
{
    DEBUG_METHOD();
    UOgmStorageState* state = (UOgmStorageState*) plugin_state;
    if (!state)
    {
        return 0;
    }
    if (state->connection)
    {
        state->connection.reset();
    }
    if (state)
    {
        delete state;
        state = nullptr;
    }

    return 0;// ok
};
int PluginUOgm::mem_post_close(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    mem->mmf.reset();

    return 0;// ok
};
//-------------------------------------------------------------

std::shared_ptr<MemoryMappedFile> PluginUOgm::t_pack(std::unique_ptr<Memory>& arc_mem, std::string& src_path, std::list<std::string> pack_list)
{
    DEBUG_METHOD();
    UOgmStorageState *state;
    std::shared_ptr<MemoryMappedFile> dst_mmf(nullptr);

    bool direct_using_arc = false;
    if (! arc_mem->mmf)
    {
        direct_using_arc = true;
    }

    for(std::list<std::string>::iterator pack_list_it = pack_list.begin(); pack_list_it != pack_list.end(); pack_list_it++)
    {
        std::string pack_file = *pack_list_it;

        if ( direct_using_arc)
        {
            auto mem = mem_pre_open(arc_mem->name); // invalid
            if (!mem)
                return false;
            arc_mem = std::move(mem);
        }
        state = (UOgmStorageState*)mem_open(arc_mem) ;

        auto uogm = state->connection->upflow()->GetDataUnit();
        if (uogm->fullname() != pack_file )
        {
            mem_close(state);
            if (direct_using_arc)
                mem_post_close(arc_mem);
            DEBUG_MESSAGE("Not found file with name " + pack_file);
            return nullptr;
        }

        DEBUG_MESSAGE("uogm_Pack: Create Dst File");

        std::string dst_abs_filename;
        if (direct_using_arc)
        {
            dst_mmf = state->mmf;
            dst_abs_filename = arc_mem->name;
        }
        else
        {
            // take temp path
            DWORD buffer_len = 255;
            char buffer[buffer_len];
            GetTempPath(buffer_len, buffer);
            std::string tmp_path = buffer;
            // open temp file
            std::shared_ptr<MemoryMappedFile> tmp_mmf(new MemoryMappedFile(8*16), DeleteMemoryMappedFile);
            dst_mmf = tmp_mmf;
            dst_abs_filename = tmp_path + arc_mem->name;
            dst_mmf->Open(dst_abs_filename, false);
        }




        std::shared_ptr<MemoryMappedFile> src_mmf(new MemoryMappedFile(8*16),DeleteMemoryMappedFile); // 8 Mb
        std::string src_abs_filename = src_path + pack_file;
        src_mmf->Open(src_abs_filename, true);
        if (!direct_using_arc)
        {
            //copy arc mem to dst file
            CopyFile(dst_mmf.get(),arc_mem.get());
        }
        // modify dst file
        // Read src file
        DEBUG_MESSAGE("Processing uomg media");

        unsigned int content_size = src_mmf->GetSize();
        uogm->global_header()->set_content_size(content_size);
        
        std::unique_ptr<DirectMemory,decltype(&DeleteDirectMemory)> temp_data(new DirectMemory(), DeleteDirectMemory);
        temp_data->Open(content_size);
        size_t src_offset = 0;
        MemMove(temp_data.get(),
            src_mmf.get(),
            temp_data->Begin(),
            src_mmf->Begin() + src_offset,
            content_size);
    
        binpac::const_bytestring uogm_data(temp_data->Begin(), temp_data->GetSize());
        uogm_data.set_mem_strg(temp_data.get());
        uogm->content()->set_data(uogm_data);
        uogm->content()->set_size(temp_data->GetSize());
        // INIT CODE FOR STORING
        MemoryMappedFile::SetSelf(dst_mmf.get());
        binpac::UOgm::ADD_STORE_PIECE_FPTR = &(MemoryMappedFile::WrapperAddStorePiece); 
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(dst_mmf.get()); 
        
        size_t store_size = state->connection->store(true, dst_mmf->Begin(), dst_mmf->Begin() + src_mmf->GetSize()+100, uogm);
        dst_mmf->SetSize(store_size);
        
        
        mem_close(state);
        if (direct_using_arc)
            mem_post_close(arc_mem);
        //res_mmf = dst_mmf;
    }
    return dst_mmf; //ok
}

