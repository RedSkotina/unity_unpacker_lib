#include <Pluma/Pluma.hpp>
//#define _WIN32_WINNT _WIN32_WINNT_WINXP
#include "windows.h"
std::string get_path(std::string fullpatch);
std::string get_current_dir();
//#include "graph.hpp"

#include "memory_mapped_file/memory_mapped_file.h"

class pHeaderData
{
public:
    std::string arc_name;
    std::string file_name;
    unsigned int file_time;
    unsigned int pack_size;
    unsigned int unp_size;

};

class UPlugins;

class StorageState
{

public:
    StorageState():filename(""), mmf(nullptr),counter(0) {}; //,filehandle(INVALID_HANDLE_VALUE)
    std::string filename;
    std::shared_ptr<MemoryMappedFile> mmf;  // TODO: refactor ?
    DWORD counter; //
    UPlugins* plugins;
    // for using in process unpack . refactor ?
    binpac::const_byteptr begin;
    binpac::const_byteptr end;
    
};

struct StorageID
{
    unsigned int parent_id;
    unsigned int id;
};
class Storage
{
public:
    virtual std::string description()  = 0;
    virtual StorageID get_id() = 0;
    //virtual bool can_handle(std::string& name)  = 0;
    //virtual StorageState* open(std::string& name)  = 0;
    //virtual int close(StorageState* plugin_state) = 0 ;
    virtual bool mem_can_handle(std::unique_ptr<Memory>& mem)  = 0;
    virtual bool unpack(StorageState* plugin_state, std::string dst_filename)  = 0;
    //virtual bool pack(std::string& src_path, std::string& arcname, std::list<char*> pack_list)  = 0;
    virtual std::shared_ptr<MemoryMappedFile> t_pack (std::unique_ptr<Memory>& arc_mem, std::string& src_path, std::list<std::string> pack_list)  = 0;
    virtual std::unique_ptr<Memory> mem_pre_open(std::string& abs_filename) = 0;
    virtual StorageState* mem_open(std::unique_ptr<Memory>& mem)  = 0;
    virtual pHeaderData* next_item(StorageState* plugin_state)  = 0;
    virtual int mem_close(StorageState* plugin_state) = 0 ;
    virtual int mem_post_close(std::unique_ptr<Memory>& mem) = 0;
    virtual std::unique_ptr<Memory> get_mem(StorageState* plugin_state) = 0;
    virtual std::unique_ptr<Memory> get_mem_by_name(StorageState* plugin_state, std::string name) = 0;
};
PLUMA_PROVIDER_HEADER(Storage);
