#include "dds.hpp"
#include "DebugLog.hpp"
#include <cmath>
#include <algorithm>

DDSUtil::DDSUtil()
{
}

DDSUtil::~DDSUtil()
{
}
int DDSUtil::tex_to_dds(std::shared_ptr<MemoryMappedFile> dst_mmf, std::shared_ptr<MemoryMappedFile> src_mmf, size_t src_offset, size_t width, size_t height, size_t size, bool has_mipmap, unsigned int format)
{
    DEBUG_METHOD();
    auto dds_connection = make_unique<binpac::DDS::Connection>();
    //binpac::DDS::Connection* dds_connection = new binpac::DDS::Connection();
    binpac::DDS::Pac* dds = new binpac::DDS::Pac(); 
    dds->set_magic_number(binpac::DDS::DDS_MAGIC_NUMBER);
    binpac::DDS::DDS_HEADER* header = new binpac::DDS::DDS_HEADER();
    header->set_dwSize(0x7C);
    binpac::uint32 dwFlags = binpac::DDS::DDSD_CAPS + binpac::DDS::DDSD_HEIGHT + binpac::DDS::DDSD_WIDTH + binpac::DDS::DDSD_PIXELFORMAT; 
    binpac::uint32 dwMipMapCount = 0;
    binpac::uint32 dwCaps = binpac::DDS::DDSCAPS_TEXTURE;
    
    if (has_mipmap)
    {
        dwFlags += binpac::DDS::DDSD_MIPMAPCOUNT; 
        dwMipMapCount = (unsigned int) log (std::max(height,width)) / log(2); 
        dwCaps += binpac::DDS::DDSCAPS_COMPLEX + binpac::DDS::DDSCAPS_MIPMAP;
    }
    DEBUG_VALUE_OF_HEX(dwMipMapCount);
    header->set_dwFlags(dwFlags);
    header->set_dwHeight(height);
    header->set_dwWidth(width);
    header->set_dwPitchOrLinearSize(0);
    header->set_dwDepth(0);
    header->set_dwMipMapCount(dwMipMapCount);
    vector<binpac::uint32>* reserved1 = new vector<binpac::uint32>(11, 0);
    header->set_dwReserved1(reserved1);
    
    binpac::DDS::DDS_PIXELFORMAT* ddspf = new binpac::DDS::DDS_PIXELFORMAT();
    ddspf->set_dwSize(0x20);
    ddspf->set_dwFlags(0);
    ddspf->set_dwFourCC(0);
    ddspf->set_dwRGBBitCount(0);
    ddspf->set_dwRBitMask(0);
    ddspf->set_dwGBitMask(0);
    ddspf->set_dwBBitMask(0);
    ddspf->set_dwABitMask(0);
    
    header->set_ddspf(ddspf);
    header->set_dwCaps(dwCaps);
    header->set_dwCaps2(0);
    header->set_dwCaps3(0);
    header->set_dwCaps4(0);
    header->set_dwReserved2(0);
    // TODO: add support  flip image
    unsigned int pitch;
    unsigned int block_size;
    TexToDDSConvertor conv;
    switch (format)
    {
        case binpac::Tex::TEX_FORMAT_A8:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_ALPHA);
            header->ddspf()->set_dwRGBBitCount(0x8);
            header->ddspf()->set_dwRBitMask(0x0);
            header->ddspf()->set_dwGBitMask(0x0);
            header->ddspf()->set_dwBBitMask(0x0);
            header->ddspf()->set_dwABitMask(0xFF);
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_PITCH);
            pitch = ( header->dwWidth() * header->ddspf()->dwRGBBitCount() + 7 ) / 8 ;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_A8;
            conv.to = binpac::DDS::DDS_FORMAT_A8;
            break;
        case binpac::Tex::TEX_FORMAT_ABGR4444:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_ALPHAPIXELS + binpac::DDS::DDPF_RGB);
            header->ddspf()->set_dwRGBBitCount(0x10);
            header->ddspf()->set_dwRBitMask(0xF);
            header->ddspf()->set_dwGBitMask(0xF0);
            header->ddspf()->set_dwBBitMask(0xF00);
            header->ddspf()->set_dwABitMask(0xF000);
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_PITCH);
            pitch = ( header->dwWidth() * header->ddspf()->dwRGBBitCount() + 7 ) / 8 ;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_ABGR4444;
            conv.to = binpac::DDS::DDS_FORMAT_A4R4G4B4;
            break;
        /*case binpac::Tex::TEX_FORMAT_BGR24: cant now implement because need read 3 byte isntead 4
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_RGB);
            header->ddspf()->set_dwRGBBitCount(0x0C);
            header->ddspf()->set_dwRBitMask(0xFF000);
            header->ddspf()->set_dwGBitMask(0xFF00);
            header->ddspf()->set_dwBBitMask(0xFF);
            header->ddspf()->set_dwABitMask(0x00);
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_PITCH);
            pitch = ( header->dwWidth() * header->ddspf()->dwRGBBitCount() + 7 ) / 8 ;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_BGR24;
            conv.to = binpac::DDS::DDS_FORMAT_R8G8B8;
            break;*/    
        case binpac::Tex::TEX_FORMAT_ABGR32:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_ALPHAPIXELS + binpac::DDS::DDPF_RGB);
            header->ddspf()->set_dwRGBBitCount(0x20);
            header->ddspf()->set_dwRBitMask(0xFF0000);
            header->ddspf()->set_dwGBitMask(0xFF00);
            header->ddspf()->set_dwBBitMask(0xFF);
            header->ddspf()->set_dwABitMask(0xFF000000);
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_PITCH);
            pitch = ( header->dwWidth() * header->ddspf()->dwRGBBitCount() + 7 ) / 8 ;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_ABGR32;
            conv.to = binpac::DDS::DDS_FORMAT_A8R8G8B8;
            break;
        case binpac::Tex::TEX_FORMAT_BGRA32:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_ALPHAPIXELS + binpac::DDS::DDPF_RGB);
            header->ddspf()->set_dwRGBBitCount(0x20);
            header->ddspf()->set_dwRBitMask(0xFF0000);
            header->ddspf()->set_dwGBitMask(0xFF00);
            header->ddspf()->set_dwBBitMask(0xFF);
            header->ddspf()->set_dwABitMask(0xFF000000);
            
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_PITCH);
            pitch = ( header->dwWidth() * header->ddspf()->dwRGBBitCount() + 7 ) / 8 ;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_BGRA32;
            conv.to = binpac::DDS::DDS_FORMAT_A8R8G8B8;
            break;
        case binpac::Tex::TEX_FORMAT_DXT1:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_FOURCC);
            header->ddspf()->set_dwFourCC(binpac::DDS::FOURCC_DXT1);
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_LINEARSIZE);
            //The block-size is 8 bytes for DXT1, BC1, and BC4 formats, and 16 bytes for other block-compressed formats.
            block_size = 8;
            pitch = std::max( 1, ((int)(header->dwWidth()) + 3)/4 ) * block_size;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_DXT5;
            conv.to = binpac::DDS::DDS_FORMAT_DXT5;
            break;
        case binpac::Tex::TEX_FORMAT_DXT5:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_FOURCC);
            header->ddspf()->set_dwFourCC(binpac::DDS::FOURCC_DXT5);
            header->set_dwFlags(header->dwFlags() + binpac::DDS::DDSD_LINEARSIZE);
            //The block-size is 8 bytes for DXT1, BC1, and BC4 formats, and 16 bytes for other block-compressed formats.
            block_size = 16;
            pitch = std::max( 1, ((int)(header->dwWidth()) + 3)/4 ) * block_size;
            header->set_dwPitchOrLinearSize( pitch );
            conv.from = binpac::Tex::TEX_FORMAT_DXT5;
            conv.to = binpac::DDS::DDS_FORMAT_DXT5;
            break;
        /*case binpac::Tex::TEX_FORMAT_PVRTC_RGB2:
            header->ddspf()->set_dwFlags(binpac::DDS::DDPF_ALPHAPIXELS + binpac::DDS::DDPF_RGB);
            header->ddspf()->set_dwRGBBitCount(0x10);
            header->ddspf()->set_dwRBitMask(0xF00);
            header->ddspf()->set_dwGBitMask(0xF0);
            header->ddspf()->set_dwBBitMask(0xF);
            header->ddspf()->set_dwABitMask(0xF000);
        */    
        default:
            DEBUG_MESSAGE("Unknown texture type");
            DEBUG_VALUE_OF(format);
            return -1;
            break;
    }
    
    std::unique_ptr<DirectMemory,decltype(&DeleteDirectMemory)> temp_pixels(new DirectMemory(), DeleteDirectMemory);
    temp_pixels->Open(size);
    unsigned int unpack_size = size + 128; 
    unsigned int src_pixel, dst_pixel;
    unsigned int pixels_count = size/4;
    for (unsigned int k = 0; k < pixels_count; k++)
    {
        src_mmf->ReadDWord(&src_pixel, src_offset + k*4 );
        dst_pixel = conv.doit(src_pixel);
        temp_pixels->WriteDWord(dst_pixel, k*4 );
    }
    
    binpac::const_bytestring dds_pixels(temp_pixels->Begin(), size);
    dds_pixels.set_mem_strg(temp_pixels.get());
    
    dds->set_bdata(dds_pixels);
    dds->set_nbytes(size);
    dds->set_nbytes2(0);
    dds->set_header(header);
    
    dds->set_header10_or_not_case_index(0);
    // INIT CODE FOR STORING
    MemoryMappedFile::SetSelf(dst_mmf.get());
    binpac::DDS::ADD_STORE_PIECE_FPTR = &(MemoryMappedFile::WrapperAddStorePiece); 
    binpac::IMemoryStorageSingleton::instance()->set_mem_strg(dst_mmf.get()); 
    
    size_t store_size = dds_connection->store(true, dst_mmf->Begin(), dst_mmf->Begin() + unpack_size, dds);
    
    dst_mmf->SetSize(store_size);
    return 0;
 }
 
 
int DDSUtil::dds_to_tex(std::shared_ptr<MemoryMappedFile> dst_mmf, std::shared_ptr<MemoryMappedFile> src_mmf, std::unique_ptr<binpac::Tex::Connection>& tex_connection)
{
        DEBUG_METHOD();
        // care: dst_mmf must have real data for copy!
        //binpac::IMemoryStorageSingleton::instance()->set_mem_strg(dst_mmf.get()); 
        //auto tex_connection = make_unique<binpac::Tex::Connection>();
        binpac::Tex::Pac* tex = tex_connection->upflow()->GetDataUnit();
        
        auto dds_connection_ = make_unique<binpac::DDS::Connection>();
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(src_mmf.get()); // IMORTANT: Before using binpac we need set IMemoryStorage for offset_pr
        binpac::DDS::Pac* dds = dds_connection_->parse(true, src_mmf->Begin(), src_mmf->End());
        
        
        
        // Copy data
        //binpac::Tex::Pac* tex = new binpac::Tex::Pac(); 
        //binpac::Tex::GlobalHeader* global_header = new binpac::Tex::GlobalHeader();
        //tex->set_global_header(global_header);
        tex->global_header()->set_width(dds->header()->dwWidth());
        tex->global_header()->set_height(dds->header()->dwHeight());
        binpac::uint32 image_size = dds->nbytes();
        tex->global_header()->set_image_size(image_size); // IMPORTANT: if has mipmap then this code invalid
        //tex->global_header()->set_prefetch(1); // dont touch tex // unity > 3.5 colorspace// if < 2 then store colorspace
        tex->global_header()->set_mipmap(0); // ADD SUPPORT
        //tex->global_header()->set_readable(0);
        //tex->global_header()->set_read_allow(1);
        //tex->global_header()->set_dummy(0);
        tex->global_header()->set_image_count(1);
        tex->global_header()->set_texture_dimension(2); // ???
        tex->global_header()->set_filter_mode(0);
        tex->global_header()->set_anisotropy(0);
        tex->global_header()->set_mip_bias(0);
        //tex->global_header()->set_wrap_mode(1);  // dont touch
        tex->global_header()->set_lightmap_format(0);
        //tex->global_header()->set_colorspace(0);
        tex->global_header()->set_bytes(image_size);
        //binpac::Tex::ImagePixels* image_pixels = new binpac::Tex::ImagePixels(image_size);
        //tex->set_image_pixels(image_pixels);
        
        DDSToTexConvertor conv;
    unsigned int format = dds->format();
    switch (format)
    {
        case binpac::DDS::DDS_FORMAT_A8:
            if (dds->header()->ddspf()->dwABitMask() != 0xFF)
            {
                DEBUG_MESSAGE("INVALID BITMASK");
                return -1;
            }
            tex->global_header()->set_compression_type(binpac::Tex::TEX_FORMAT_A8);
            conv.from = binpac::DDS::DDS_FORMAT_A8;
            conv.to = binpac::Tex::TEX_FORMAT_A8 ;
            break;
        case binpac::DDS::DDS_FORMAT_A4R4G4B4:
        
            if (dds->header()->ddspf()->dwABitMask() != 0xF000)
            {
                DEBUG_MESSAGE("INVALID BITMASK");
                return -1;
            }
            tex->global_header()->set_compression_type(binpac::Tex::TEX_FORMAT_ABGR4444);
            conv.from = binpac::DDS::DDS_FORMAT_A4R4G4B4;
            conv.to = binpac::Tex::TEX_FORMAT_ABGR4444;
            break;
        case binpac::DDS::DDS_FORMAT_A8R8G8B8:
            //if (dds->header()->ddspf()->dwABitMask() != 0xFF)
            if (dds->header()->ddspf()->dwABitMask() != 0xFF000000)
            {
                DEBUG_MESSAGE("INVALID BITMASK");
                return -1;
            }
            tex->global_header()->set_compression_type(binpac::Tex::TEX_FORMAT_BGRA32);
            conv.from = binpac::DDS::DDS_FORMAT_A8R8G8B8;
            conv.to = binpac::Tex::TEX_FORMAT_BGRA32 ;
            break;
        case binpac::DDS::DDS_FORMAT_DXT1:
            tex->global_header()->set_compression_type(binpac::Tex::TEX_FORMAT_DXT1);
            conv.from = binpac::DDS::DDS_FORMAT_DXT1;
            conv.to = binpac::Tex::TEX_FORMAT_DXT1;
            break;
        case binpac::DDS::DDS_FORMAT_DXT5:
            tex->global_header()->set_compression_type(binpac::Tex::TEX_FORMAT_DXT5);
            conv.from = binpac::DDS::DDS_FORMAT_DXT5;
            conv.to = binpac::Tex::TEX_FORMAT_DXT5;
            break;
        default:
            DEBUG_MESSAGE("Unknown texture type");
            DEBUG_VALUE_OF(format);
            return -1;
            break;
    }
        
        
    std::unique_ptr<DirectMemory,decltype(&DeleteDirectMemory)> temp_pixels(new DirectMemory(), DeleteDirectMemory);
    temp_pixels->Open(image_size);
    
    size_t src_offset = dds->bdata().begin() - src_mmf->Begin();
    unsigned int unpack_size = image_size + 38; 
    unsigned int src_pixel, dst_pixel;
    unsigned int pixels_count = image_size/4;
    for (unsigned int k = 0; k < pixels_count; k++)
    {
        src_mmf->ReadDWord(&src_pixel, src_offset + k*4 );
        dst_pixel = conv.doit(src_pixel);
        temp_pixels->WriteDWord(dst_pixel, k*4 );
    }
    
        binpac::const_bytestring tex_pixels(temp_pixels->Begin(), temp_pixels->GetSize());
        tex_pixels.set_mem_strg(temp_pixels.get());
        //delete tex->image_pixels()->pixels(); // evade memory leak
        tex->image_pixels()->set_pixels(tex_pixels);
        // check. probably forgot in 0.7.3
        tex->image_pixels()->set_bytes(temp_pixels->GetSize());
        // INIT CODE FOR STORING
        MemoryMappedFile::SetSelf(dst_mmf.get());
        binpac::Tex::ADD_STORE_PIECE_FPTR = &(MemoryMappedFile::WrapperAddStorePiece); 
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(dst_mmf.get()); 
        //auto tex_connection = make_unique<binpac::Tex::Connection>();
        
        size_t store_size = tex_connection->store(true, dst_mmf->Begin(), dst_mmf->Begin() + src_mmf->GetSize()+100, tex);
        //????? size_t store_size = tex->Store( dst_mmf->Begin(), dst_mmf->Begin() + state->mmf->GetSize()+100);
        //delete tex_connection;
        dst_mmf->SetSize(store_size);
}
  
 
