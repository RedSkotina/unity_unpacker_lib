#include "plugin.hpp"
PLUMA_PROVIDER_SOURCE(Storage, 1, 1);

std::string get_path(std::string fullpatch)
{
	return fullpatch.substr(0,fullpatch.find_last_of('\\') );
}	
std::string get_current_dir()
{
	char path[MAX_PATH];
	HMODULE hm = NULL;
	static int anchor = 0;

	if (!GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | 
        GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        (LPCSTR) &anchor, 
        &hm))
	{
		int ret = GetLastError();
		OutputDebugString("unity.get_current_dir(): GetModuleHandle returned error\n");
	}
	GetModuleFileNameA(hm, path, sizeof(path));
	std::string cur_path = get_path(std::string(path));
	OutputDebugString(cur_path.c_str());
	return cur_path;
}

