#include "plugin_assets.hpp"
#include <algorithm>

#include <list>

//DEBUG_TO_FILE("c:\\totalcmd\\Plugins\\wcx\\Unity\\plugin-assets-log.txt");
std::string get_current_dir();
DEBUG_TO_FILE((get_current_dir()+std::string("\\plugin-assets-log.txt")).c_str());

typedef binpac::Assets::DirectoryEntry DirectoryEntry;
//Convert an array of four bytes into a 32-bit integer.
DWORD BytesToDword(BYTE *b, bool LittleEndian = 1 )
{
    if (LittleEndian)
        return (b[0]) | (b[1] << 8) | (b[2] << 16) | (b[3] << 24);
    else
        return (b[0]<<24) | (b[1] << 16) | (b[2] << 8) | (b[3] );
}
void DwordToBytes(DWORD a, BYTE *b, bool LittleEndian = 1 )
{
    if (LittleEndian)
    {
        b[3] = (a >> 24) & 0xff;
        b[2] = (a >> 16) & 0xff;
        b[1] = (a >> 8) & 0xff;
        b[0] = a & 0xff ;
    }
    else
    {
        b[0] = (a >> 24) & 0xff;
        b[1] = (a >> 16) & 0xff;
        b[2] = (a >> 8) & 0xff;
        b[3] = a & 0xff ;
    }
}

std::string vector_to_string(std::vector<binpac::uint32>* v)
{
    std::string s = "";
    BYTE temp[4];
    for (std::vector<binpac::uint32>::iterator it = v->begin(); it != v->end(); it++)
    {
        DwordToBytes(*it, temp, 0);
        s+= temp[0];
        s+= temp[1];
        s+= temp[2];
        s+= temp[3];
    }
    return s;
}
void WriteDWORD(MemoryMappedFile* mmf, binpac::const_byteptr& offset, DWORD value, bool endianess = 1)
{
    unsigned int pageid = offset.page_; // TODO: mb remove ? setpage yet in get_ptr()
    mmf->SetPage(pageid); // ignore possible error . look TODO
    BYTE bvalue[4];
    DwordToBytes((DWORD)value, bvalue, endianess);
    memcpy((unsigned char*)(offset).get_ptr(),bvalue,sizeof(bvalue));
}
void* TryPtrOrDie(void* ptr)  // TEMPLATE !!!!!
{
    assert(ptr!=nullptr);
    return ptr;
}
void* TryHandleOrDie(void* ptr)
{
    if (ptr == INVALID_HANDLE_VALUE)
        throw std::bad_exception();
    return ptr;
}
void TryReadResultOrDie(int must_read_bytes,int readed_bytes)
{
    if(must_read_bytes!=readed_bytes) throw std::system_error(std::error_code());
    return;
}

int FixDirectoryOffsets (MemoryMappedFile* mmf, unsigned char* dst_map, std::vector<DirectoryEntry*>::iterator& it,
                         const std::vector<DirectoryEntry*>::iterator& end, long rel_offset, bool endianess, long dir_offset = 0  )
{
    DEBUG_METHOD();
    // set new size of changed recource
    binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf);
    WriteDWORD( mmf,
                mmf->Begin() + (*it)->field_ptr() + 2 * sizeof(DWORD),
                (*it)->r_size() + rel_offset,
                endianess );
    // set new offset of next recources
    it++;
    while ( it!=end )
    {
        WriteDWORD( mmf,
                    mmf->Begin() + (*it)->field_ptr() + sizeof(DWORD),
                    (*it)->r_offset() + rel_offset,
                    endianess);

        it++;
    }
}
int FixFileSize(MemoryMappedFile* dst_mmf,DWORD FileSize)
{
    DEBUG_METHOD();
    /*
    dst_mmf->SetPage(0); // TODO: [3] invalid page, if page_size < 8 byte.
    BYTE bfile_size[4];
    DwordToBytes((DWORD)FileSize, bfile_size,  0); // Global header have byte order Big Endian
    memcpy((unsigned char*)dst_mmf->Begin() + sizeof(DWORD),bfile_size,sizeof(bfile_size)); // TODO: [3] potential error, look at  when new_size intersect page
    */
    binpac::IMemoryStorageSingleton::instance()->set_mem_strg(dst_mmf);
    WriteDWORD( dst_mmf,
                (binpac::const_byteptr)dst_mmf->Begin() + sizeof(DWORD),
                (DWORD)FileSize,
                0);

    return 0;

}

long InsertToPac(MemoryMappedFile* dst_mmf, MemoryMappedFile* src_mmf, unsigned char* src, size_t src_size,unsigned char* dst_begin_map,size_t nFileSize, size_t dst_region_offset, size_t dst_region_size)
{
    DEBUG_METHOD();

    long rel_size = src_size - dst_region_size;
    if (rel_size >= 0)   // OR set size before extend file
        dst_mmf->SetSize(nFileSize + rel_size);
    MemMove(
        dst_mmf,
        (unsigned char*)dst_begin_map + dst_region_offset + src_size, // new end of dst file
        (unsigned char*)dst_begin_map + dst_region_offset + dst_region_size,  // end of dst file
        nFileSize - (dst_region_offset+dst_region_size)  // size of moved bytes

    );
    if (rel_size < 0)  // OR set size after truncate file
        dst_mmf->SetSize(nFileSize + rel_size);
    MemMove( dst_mmf, src_mmf, (unsigned char*)dst_begin_map + dst_region_offset, src, src_size); // copy src to dst file
    return rel_size;

}

bool PluginAssets::mem_can_handle(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    BOOL can_handle = true;

    AssetsStorageState* plugin_state = nullptr;
    try
    {
        plugin_state = (AssetsStorageState*)mem_open(mem);
    }
    catch (std::system_error& exc)
    {
        DEBUG_MESSAGE("Unhandled Exception");
        can_handle = false;
        goto clean;

    }
    if (plugin_state != NULL)
    {
        vector<binpac::uint8>* version = plugin_state->connection->upflow()->GetDataUnit()->global_header()->secondary()->version();
        if (version->size() < 3 || (version->size() > 2 && (*version)[1] != 0x2E)) 
        {
            vector<binpac::uint8> tversion = *version;
            DEBUG_VALUE_OF_COLLECTION_HEX(tversion);
            can_handle = false;
        }


    }
    else
    {
        DEBUG_MESSAGE("plugin_state == NULL");

        can_handle = false;
        return  can_handle;
    }

clean:
    mem_close(plugin_state);



    return can_handle;
}

std::unique_ptr<Memory> PluginAssets::get_mem(StorageState* plugin_state)
{
    DEBUG_METHOD();
    AssetsStorageState* state = (AssetsStorageState*)plugin_state;
    binpac::Assets::Pac* pac = state->connection->upflow()->GetDataUnit();
    DWORD i = plugin_state->counter - 1; //because next_item increase that
    
    if (i >= pac->directory_length())
    {
        return nullptr;

    }
    auto mem = make_unique<Memory>();
    unsigned int c_offset = pac->directory()->entries()->at(i)->resource()->c_offset();
    unsigned int c_size = pac->directory()->entries()->at(i)->resource()->c_size();
    std::string c_fullname = pac->directory()->entries()->at(i)->resource()->fullname();

    mem->begin = binpac::const_byteptr(state->mmf->Begin()+c_offset);
    mem->end = binpac::const_byteptr(state->mmf->Begin()+c_offset+c_size);
    mem->mmf = state->mmf;
    mem->name = c_fullname;

    return mem;
}

std::unique_ptr<Memory> PluginAssets::get_mem_by_name(StorageState* plugin_state, std::string name)
{
    DEBUG_METHOD();
    AssetsStorageState* state = (AssetsStorageState*)plugin_state;
    binpac::Assets::Pac* pac = state->connection->upflow()->GetDataUnit();

    auto mem = make_unique<Memory>();

    std::vector<DirectoryEntry*>* entries = pac->directory()->entries();
    std::vector<DirectoryEntry*>::iterator it;

    it = std::find_if(entries->begin(), entries->end(),
                      [name](const DirectoryEntry*  e) -> bool { return e->resource()->fullname() == std::string(name); });
    if (it == entries->end() )
    {
        return nullptr;
    }

    unsigned int c_offset = (*it)->resource()->c_offset();
    unsigned int c_size = (*it)->resource()->c_size();
    std::string c_fullname = (*it)->resource()->fullname();

    mem->begin = binpac::const_byteptr(state->mmf->Begin()+c_offset);
    mem->end = binpac::const_byteptr(state->mmf->Begin()+c_offset+c_size);
    mem->mmf = state->mmf;
    mem->name = c_fullname;

    return mem;
}

std::unique_ptr<Memory> PluginAssets::mem_pre_open(std::string& abs_filename)
{
    DEBUG_METHOD();
    MemoryMappedFile* mmf;
    try
    {
        std::shared_ptr<MemoryMappedFile> mmf(new MemoryMappedFile(64*16) , DeleteMemoryMappedFile ); // 64 Mb
        mmf->Open(abs_filename);
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf.get());
        auto mem = make_unique<Memory>();
        mem->begin = binpac::const_byteptr(mmf->Begin());
        mem->end = binpac::const_byteptr(mmf->Begin()+mmf->GetSize());
        mem->mmf = mmf;
        mem->name = extract_filename_from_absolute_filename(abs_filename);
        return mem;
    }
    catch (...)
    {
        DEBUG_MESSAGE("Error: unhandled exception");
        return nullptr;
    }
}
StorageState* PluginAssets::mem_open(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    try
    {
        std::shared_ptr<MemoryMappedFile> mmf = mem->mmf;

        binpac::Assets::Ext::assets_filename = extract_basename_from_filename(mem->name);
        binpac::Assets::Ext::filename_map_ptr->clear();

        DEBUG_MESSAGE("Processing message");

        auto connection = make_unique<binpac::Assets::Connection>();
        binpac::IMemoryStorageSingleton::instance()->set_mem_strg(mmf.get()); // IMORTANT: Before using binpac we need set IMemoryStorage for offset_pr

        connection->parse(true, mem->begin, mem->end);

        AssetsStorageState* plugin_state = new AssetsStorageState();
        plugin_state->filename = mem->name;
        plugin_state->mmf = mmf;
        plugin_state->connection = std::move(connection);
        return (StorageState*)plugin_state;

    }
    catch (binpac::Exception& exc)
    {
        DEBUG_MESSAGE("Error: binpac error");
        DEBUG_MESSAGE(exc.c_msg());
        return NULL;
    }
    catch (std::system_error& exc)
    {
        DEBUG_MESSAGE("Error: system error");
        DEBUG_MESSAGE(exc.what());
        return NULL;
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE("Error: runtime error");
        DEBUG_MESSAGE(exc.what());
        return NULL;
    }
    catch (...)
    {
        DEBUG_MESSAGE("Error: unhandled exception");
        return NULL;
    }
}

pHeaderData* PluginAssets::next_item(StorageState* plugin_state)
{
    DEBUG_METHOD();
    AssetsStorageState* state = (AssetsStorageState*)plugin_state;
    binpac::Assets::Pac* pac = state->connection->upflow()->GetDataUnit();
    if (state->counter >= pac->directory_length())
    {
        return nullptr;
        //plugin_state->counter = 0;
    }
    DWORD i = state->counter;
    pHeaderData* header = new pHeaderData();
    header->arc_name  = state->filename;
    header->file_name = pac->directory()->entries()->at(i)->resource()->fullname();
    header->file_time = pac->directory()->entries()->at(i)->r_fid()*30; // minutes
    header->pack_size = pac->directory()->entries()->at(i)->resource()->c_size();
    header->unp_size  = pac->directory()->entries()->at(i)->resource()->c_size();

    state->counter++;

    return header;//ok
};

bool PluginAssets::unpack(StorageState* plugin_state, string dest_filename)
{
    DEBUG_METHOD();
    AssetsStorageState* state = (AssetsStorageState*)plugin_state;
    binpac::Assets::Pac* pac = state->connection->upflow()->GetDataUnit();
    int i = state->counter - 1;
    DEBUG_MESSAGE(dest_filename.c_str());
    DEBUG_MESSAGE(pac->directory()->entries()->at(i)->resource()->fullname().c_str() );
    std::shared_ptr<MemoryMappedFile> dst_mmf(new MemoryMappedFile(8*16) , DeleteMemoryMappedFile ); // 8 Mb
    try
    {
        dst_mmf->Open(dest_filename);
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE(exc.what());
        return false;
    }

    dst_mmf->SetSize(pac->directory()->entries()->at(i)->resource()->c_size());


    MemMove(dst_mmf.get(),
            state->mmf.get(),
            dst_mmf->Begin(),
            //state->mmf->Begin() + pac->directory()->entries()->at(i)->resource()->c_offset(), // memmove use fictive pointer
            (state->begin + pac->directory()->entries()->at(i)->resource()->c_offset()).get_fictive_ptr(), // memmove use fictive pointer
            pac->directory()->entries()->at(i)->resource()->c_size());

    //DEBUG_MESSAGE("TEST");

    //write info to ini
    //minIni ini(dest_filename + ".ini");
    //ini.put("", "unity_version", vector_to_string(pac->global_header()->secondary()->version()) );


    return true;//ok
};

int PluginAssets::mem_close(StorageState* plugin_state)
{
    DEBUG_METHOD();
    AssetsStorageState* state = (AssetsStorageState*) plugin_state;
    if (!state)
    {
        return 0;
    }
    if (state->connection)
    {
        state->connection.reset();
    }
    if (state)
    {
        delete state;
        state = nullptr;
    }

    return 0;// ok
};
int PluginAssets::mem_post_close(std::unique_ptr<Memory>& mem)
{
    DEBUG_METHOD();
    mem->mmf.reset();
    return 0;// ok
};


std::shared_ptr<MemoryMappedFile> PluginAssets::t_pack(std::unique_ptr<Memory>& arc_mem, std::string& src_path, std::list<std::string> pack_list)
{
    DEBUG_METHOD();
    AssetsStorageState *state;
    std::shared_ptr<MemoryMappedFile> dst_mmf(nullptr);
    bool direct_using_arc = false;
    if (! arc_mem->mmf)
    {
        direct_using_arc = true;
    }

    for(std::list<std::string>::iterator pack_list_it = pack_list.begin(); pack_list_it != pack_list.end(); pack_list_it++)
    {
        std::string pack_file = *pack_list_it;
        if ( direct_using_arc)
        {
            // important: arc_mem->name must be absolute filename  
            auto mem = mem_pre_open(arc_mem->name); // invalid
            if (!mem)
                return false;
            arc_mem = std::move(mem);
        }
        state = (AssetsStorageState*)mem_open(arc_mem);

        auto pac = state->connection->upflow()->GetDataUnit();
        std::vector<DirectoryEntry*>* entries = pac->directory()->entries();
        std::vector<DirectoryEntry*>::iterator it;

        it = std::find_if(entries->begin(), entries->end(),
                          [pack_file](const DirectoryEntry*  e) -> bool { return e->resource()->fullname() == pack_file; });
        if (it == entries->end() )
        {
            mem_close(state);
            if (direct_using_arc)
                mem_post_close(arc_mem);
            DEBUG_MESSAGE("Not found file with name " + pack_file);
            return nullptr;
        }
        DWORD FileOffset = (*it)->resource()->c_offset();
        DWORD FileSize = (*it)->resource()->c_size();

        DEBUG_MESSAGE("assets_Pack: Create Dst File");

        std::string dst_abs_filename;
        if (direct_using_arc)
        {
            dst_mmf = state->mmf;
            dst_abs_filename = arc_mem->name; // invalid
        }
        else
        {
            // take temp path
            DWORD buffer_len = 255;
            char buffer[buffer_len];
            GetTempPath(buffer_len, buffer);
            std::string tmp_path = buffer;
            // open temp file
            std::shared_ptr<MemoryMappedFile> tmp_mmf(new MemoryMappedFile(8*16), DeleteMemoryMappedFile);
            dst_mmf = tmp_mmf;
            dst_abs_filename = tmp_path + arc_mem->name;
            dst_mmf->Open(dst_abs_filename, false);
        }


        std::shared_ptr<MemoryMappedFile> src_mmf(new MemoryMappedFile(8*16),DeleteMemoryMappedFile); // 8 Mb
        std::string src_abs_filename = src_path + pack_file;
        src_mmf->Open(src_abs_filename, true);

        if (!direct_using_arc)
        {
            //copy arc mem to dst file
            CopyFile(dst_mmf.get(),arc_mem.get());
        }
        // modify dst file
        long rel_offset = 0;
        // TODO: better use resource()->c_offset() from dst_mmf
        rel_offset = InsertToPac(dst_mmf.get(), src_mmf.get(),
                                 src_mmf->RangeBegin(),
                                 src_mmf->GetSize(),
                                 dst_mmf->RangeBegin(),
                                 dst_mmf->GetSize(),
                                 (*it)->resource()->c_offset(),
                                 (*it)->resource()->c_size() );
        if (pac->global_header()->main()->bias() == 0) // unity 3 ?
        {
            FixFileSize(dst_mmf.get(), dst_mmf->GetSize());
            FixDirectoryOffsets(dst_mmf.get(),dst_mmf->RangeBegin(), it, entries->end(), rel_offset, pac->global_header()->byte_order(), rel_offset); // directory offset changed
        }
        else // unity4
        {
            FixFileSize(dst_mmf.get(), dst_mmf->GetSize());
            FixDirectoryOffsets(dst_mmf.get(), dst_mmf->RangeBegin(), it, entries->end(), rel_offset, pac->global_header()->byte_order()); // directory offset constant
        }
        //src_mmf->Close();
        //src_mmf->FinalClose();
        //delete src_mmf;
        //dst_mmf->Close();
        //dst_mmf->FinalClose();
        mem_close(state);
        if (direct_using_arc)
            mem_post_close(arc_mem);
    }

    return dst_mmf; //ok
}
