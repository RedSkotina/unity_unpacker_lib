
analyzer Tex withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac
extern type ext;
extern type stdstring;
%header{
	#include <string>

	//#include <custom/custom.h>
    typedef std::string stdstring; 
%}
    
# image
type Pac = record {
  global_header: GlobalHeader;
  image_pixels: ImagePixels(global_header.bytes);
  } &let
{
	extension: stdstring = ext.getExtension();
    fullname: stdstring =  ext.fullname( ext.tex_filename, extension);
} &byteorder=littleendian  ;

# header of main storage
type GlobalHeader = record {
  width: uint32;
  height: uint32;
  image_size: uint32;
  compression_type: uint32;
  mipmap: uint8;
  readable: uint8;
  read_allow: uint8;
  dummy: uint8;
  image_count: uint32;
  texture_dimension: uint32;
  filter_mode: uint32; #0:point 1:bilinear 2:trilinear
  anisotropy: uint32; # 0..9
  mip_bias:uint32;
  wrap_mode:uint32; #0:repeat 1:clamp
  lightmap_format:uint32; # unity version > 3 , fileGen >= 8
  colorspace_or_not: case prefetch < 2 of {
    true  -> colorspace:uint32; #unity version > 3.5 #fileGen == 9
	false -> none:empty;
  } &requires(prefetch);
  bytes: uint32;
} &let {
    file_size: uint32 = bytes + 14 * 4;
    prefetch: uint32 withinput $context.flow.pacdata(sourcedata, offsetof(lightmap_format)+4, 4 );
    # change to begin_of_data + offsetof
    compressed: bool = (compression_type == TEX_FORMAT_DXT1 || compression_type == TEX_FORMAT_DXT5)? true: false;
    numComponents:uint32 = ext.calc_num_components(compression_type);
    nbytes: uint32 = (compressed == true)? ext.get_compressed_size(width, height, compression_type): ext.get_uncompressed_size(width, height, numComponents);
    nbytes2: uint32 = ext.get_mipmaps_total_size(compressed, width, height, compression_type, numComponents, mipmap);
      
} &exportsourcedata;


#array of pixels	
type ImagePixels(bytes: uint32) = record {
 pixels: bytestring &transient &length=bytes;
} 

enum TEX_FORMAT
{
    TEX_FORMAT_A8       = 1,
    TEX_FORMAT_ABGR4444 = 2,
    TEX_FORMAT_BGR24    = 3,
    TEX_FORMAT_ABGR32   = 4,
    TEX_FORMAT_BGRA32    = 5, # invalid, BGRA32
    TEX_FORMAT_BGR565   = 7,
    TEX_FORMAT_DXT1     = 10,
    TEX_FORMAT_DXT5     = 12,
    TEX_FORMAT_BGRA4bpp = 13,
    TEX_FORMAT_PVRTC_RGB2       = 30, # invalid byteorder
    TEX_FORMAT_PVRTC_RGBA2      = 31,
    TEX_FORMAT_PVRTC_RGB4       = 32,
    TEX_FORMAT_PVRTC_RGBA4      = 33,
    TEX_FORMAT_ETC_RGB4         = 34,
    TEX_FORMAT_ATC_RGB4         = 35,
    TEX_FORMAT_ATC_RGBA8        = 36
};


%header{
 
	class Ext{
	public:
		Ext(){};
	static stdstring tex_filename;
	static stdstring extension;
	
	stdstring const & getExtension() const{
		return Ext::extension;
	}
	
	static stdstring fullname(const stdstring& name, const stdstring& name_ext  )
	{
		return (name + name_ext);
	}
	
    static uint32 get_mipmaps_total_size(bool compressed, uint32 width, uint32 height, uint32 format, uint32 num_components, uint32 mipmapcount )
    {
        uint32 h = height >> 1;
        uint32 w = width >> 1;
        uint32 size = 0;
        uint32 total_size = 0;
        
        for (unsigned int i = 0; i < mipmapcount && (w || h); i++)
        {
            if (compressed)
                size = get_compressed_size(w, h, format);
            else
                size = get_uncompressed_size(w, h, num_components);
            
            total_size += size;

            h >>= 1;
            w >>= 1;
        }
        
        return total_size;
    }
    static uint32 get_compressed_size(uint32 width, uint32 height, uint32 format)
    {
        uint32 size = ((width+3)/4)*((height+3)/4) *  (format == TEX_FORMAT_DXT1? 8: 16);
        return size;
    }
    static uint32 get_uncompressed_size(uint32 width, uint32 height, uint32 num_components)
    {
        uint32 size = width * height * num_components;
        return size;
    }
	static uint32 calc_num_components(uint32 format)  
	{
        uint32 num_components;
		switch ( format )
        {
        case TEX_FORMAT_DXT5:
            num_components = 4;
            break;
        case TEX_FORMAT_DXT1:
            num_components = 3;
            break;
        default:
            num_components = 4;
        }
        return num_components;
	}
	};
	static Ext ext;
%}
%code{
	stdstring Ext::tex_filename="init";
	stdstring Ext::extension=".dds";
%}