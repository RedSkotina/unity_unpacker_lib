analyzer Tga withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

# main storage
type Pac = record {
  global_header: GlobalHeader;
  image_colormap: Image_ColorMap(global_header); 
 }  &byteorder=littleendian;


type GlobalHeader = record {
    image_ident_field_length: uint8;
    colormap_type: uint8;
    image_type: uint8;
    colormap_spec: ColorMap_Spec;
    image_spec: Image_Spec;
} 

type ColorMap_Spec = record {
    colormap_origin: uint16;
    colormap_length: uint16;
    colormap_entry_size: uint8;
}

type Image_Spec = record {
    x_origin: uint16;
    y_origin: uint16;
    width: uint16;
    height: uint16;
    image_pixel_size: uint8;
    image_descriptor: uint8;
}

type Image_ColorMap(global_header: GlobalHeader) = record {
    image_ident_field: uint8[global_header.image_ident_field_length];
    colormap_data_or_not: case global_header.colormap_type of {
        1 -> colormap_data: ColorMap_Data(global_header.colormap_spec);
        0 -> none:empty;
    };
    image_pixels_or_indicies: case global_header.colormap_type of {
        1 -> image_indicies: Image_Data_Indicies(global_header.image_spec);
        0 -> image_pixels: Image_Data_Pixels(global_header.image_spec);
    };
    
    
}
####  ColorMap
type ColorMap_Data(colormap_spec: ColorMap_Spec) = record {
    entries: ColorMap_Entry(colormap_spec.colormap_entry_size)[colormap_spec.colormap_length];
}
type ColorMap_Entry(colormap_entry_size: uint8) = record {
    colormap_entry: case colormap_entry_size of {
    32 -> colormap_entry32: ColorMap_Entry32;
	24 -> colormap_entry24: ColorMap_Entry24;
	16 -> colormap_entry16: ColorMap_Entry16;
  };
}
type ColorMap_Entry32 = record {
    b: uint8;
    g: uint8;
    r: uint8;
    a: uint8;
}
type ColorMap_Entry24 = record {
    b: uint8;
    g: uint8;
    r: uint8;
}
type ColorMap_Entry16 = record {
    lo: uint8;
    hi: uint8;
}
#### Image WITHOUT ColorMap
type Image_Data_Pixels(image_spec: Image_Spec) = record {
    pixels: Image_Pixel(image_spec.image_pixel_size)[image_spec.width * image_spec.height];
}
type Image_Pixel(image_pixel_size: uint8) = record {
    image_pixel: case image_pixel_size of {
    32 -> image_pixel32: Image_Pixel32;
	24 -> image_pixel24: Image_Pixel24;
	16 -> image_pixel16: Image_Pixel16;
  }; 
}
type Image_Pixel32 = record {
    b: uint8;
    g: uint8;
    r: uint8;
    a: uint8;
}
type Image_Pixel24 = record {
    b: uint8;
    g: uint8;
    r: uint8;
}
type Image_Pixel16 = record {
    lo: uint8;
    hi: uint8;
}

#### Image WITH ColorMap
type Image_Data_Indicies(image_spec: Image_Spec) = record {
    indicies: Image_Index(image_spec.image_pixel_size)[image_spec.width * image_spec.height];
}
type Image_Index(image_pixel_size: uint8) = record {
    image_index: case image_pixel_size of {
    16 -> image_index16: Image_Index16;
    8 -> image_index8: Image_Index8;
  }; 
}
type Image_Index16 = record {
    lo: uint8;
    hi: uint8;
}
type Image_Index8 = record {
    lo: uint8;
}
