
analyzer Tex withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

extern type ext;
extern type stdstring;
%header{
	#include <string>

	//#include <custom/custom.h>
    typedef std::string stdstring; 
	 
	class Ext{
	public:
		Ext(){};
	static stdstring tex_filename;
	static stdstring extension;
	
	stdstring const & getExtension() const{
		return Ext::extension;
	}
	
	static stdstring fullname(const stdstring& name, const stdstring& name_ext  )
	{
		return (name + name_ext);
	}
	
	};
	static Ext ext;
%}
%code{
	stdstring Ext::tex_filename="init";
	stdstring Ext::extension=".tex";
%}
# image
type Pac = record {
  global_header: GlobalHeader;
 } &let
{
	extension: stdstring = ext.getExtension();
    fullname: stdstring =  ext.fullname( ext.tex_filename, extension);
	pixels: Pixels(this) withinput $context.flow.pacdata(sourcedata, global_header.pixels_offset, global_header.bytes );
    
} &byteorder=littleendian &exportsourcedata;

# header of main storage
type GlobalHeader = record {
  width: uint32;
  height: uint32;
  image_size: uint32;
  compression_type: uint32;
  reserved2: uint32;
  reserved3: uint32;
  
  reserved4: uint32;
  reserved5: uint32;
  reserved6: uint32;
  reserved7: uint32;
  reserved8: uint32;
  reserved9: uint32;
  reserved10: uint32;
  
  bytes: uint32;
} &let
{
	pixels_offset: uint32 = 0x38;
}


#array of pixels	
type Pixels(pac: Pac) = record {
 
 pixel: uint8[pac.global_header.bytes];
 #pixel: Pixel(pac)[pac.global_header.image_size] ;

} 

# element-pixel	
type Pixel(pac: Pac) = record {
  alpha: uint32;
  r: uint32;
  g: uint32;
  b: uint32;
} 


