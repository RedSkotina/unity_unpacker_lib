analyzer Assets withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

# main storage
type Pac = record {
  global_header: GlobalHeader(this);
  #body: bytestring &chunked &restofdata;
 } &let
{
    directory_length: uint32 withinput $context.flow.pacdata(sourcedata, global_header.directory_offset, 4);
    directory: Directory(this) withinput $context.flow.pacdata(sourcedata, global_header.directory_offset + 4, 4 * 5 * directory_length);
    
} &byteorder=global_header.byte_order &exportsourcedata;

type GlobalHeaderMain = record {
  info_size: uint32;
  info: uint32;
  reserved1: uint32;
  bias: uint32;
  reserved2: uint32;
} &byteorder=bigendian;
type GlobalHeaderSecondary(pac: Pac) = record {
  #version: uint32[2];
  version: uint8[] &until($element == 0);
  endianess: uint32;
  reserved3: uint32[2];
} &byteorder=bigendian;

# header of main storage
type GlobalHeader(pac: Pac) = record {
  main: GlobalHeaderMain;
}  &let{
	secondary_offset:uint32 = (main.bias != 0) ? 0x14 : main.info - main.info_size + 0x1 ;
	secondary: GlobalHeaderSecondary(pac) withinput $context.flow.pacdata(pac.sourcedata, secondary_offset, 30);
	#secondary_version:uint8[] = secondary.version;
    directory_offset:uint32 = secondary_offset +  sizeof(secondary); 
	byte_order = (secondary.endianess == 0xb)||(secondary.endianess == 0xa) ? bigendian : littleendian;
	
	
}&byteorder=bigendian;

#array of directory entries	
type Directory(pac: Pac) = record {
 
 entries: DirectoryEntry(pac)[pac.directory_length] ;

} 

# element of directory array	
type DirectoryEntry(pac: Pac) = record {
  r_fid: uint32;
  r_offset: uint32;
  r_size: uint32;
  r_type: uint32;
  dummy: uint32;
  #field_ptr:Ptr(offsetof(field_ptr));  #the magic  
	
} 
&let {
    field_ptr: uint32 = goffsetof(r_fid);
	e_offset: uint32 = offsetof(r_fid);
	resource: Resource(r_type, r_size, r_fid, pac.global_header.main.bias + r_offset) withinput $context.flow.pacdata(pac.sourcedata, pac.global_header.main.bias + r_offset, r_size); 
	
}
extern type ext;
extern type stdstring;
%header{
	#include <string>

	#include <custom/custom.h>
    typedef std::string stdstring; 
	 
	class Ext{
	public:
		Ext(){};
	static stdstring assets_filename;
	static stdstring extension;
	
	static bytestring charToByte(const char* data){
		size_t len = strlen(data);
		return bytestring((uint8*)data, len);
	}
	static stdstring fullname(const bytestring& name, const stdstring& extension  )
	{
		return (std_str(name) + extension);
	}
	static stdstring generate_name(const stdstring& fid, const stdstring& ftype  )
	{
		return (assets_filename +stdstring("_")+ fid +stdstring(".") + ftype);
	}
	
	static stdstring fid(const uint32& fid )
	{
		
		return (to_string(fid));
	}
	static stdstring ftype(const uint32& ftype )
	{
		
		return (to_string(ftype));
	}
	
	static bool hasName(unsigned int r_type)  
	{
		
		switch (r_type)
		{
		//case 21,43,48,49,74,89,115,128,152,159: return true;
		case 21: Ext::extension = binpac_fmt(".mat"); return true;
		case 28: Ext::extension = binpac_fmt(".tex"); return true;
		case 43: Ext::extension = binpac_fmt(""); return true;
		case 48: Ext::extension = binpac_fmt(".shader"); return true;
		case 49: Ext::extension = binpac_fmt(".mov.xml.bcg"); return true;
		case 74: Ext::extension = binpac_fmt(".ami"); return true;
		case 83: Ext::extension = binpac_fmt(".mp3"); return true;
		case 89: Ext::extension = binpac_fmt(""); return true;
		case 115: Ext::extension = binpac_fmt(""); return true;
		case 128: Ext::extension = binpac_fmt(".ttf"); return true;
		case 152: Ext::extension = binpac_fmt(".ogm"); return true;
		case 159: Ext::extension = binpac_fmt(""); return true;
		
		default: return false;
		}
	}
	stdstring const & getExtension() const{
		return Ext::extension;
	}
	stdstring to_stdstring(const bytestring& bstr)
	{
		return std_str(bstr);
	}
    typedef std::vector<std::string> TFilenameVector;
	typedef std::map<std::string, TFilenameVector > TFilenameMap;
	//typedef TFilenameMap::iterator TFM_IT; 
	static auto_ptr<TFilenameMap> filename_map_ptr;
	stdstring get_unique_name(const bytestring& bname,const stdstring& extension)
	{
		stdstring name = std_str(bname);
		
		// check for unique filename
		
		std::pair<TFilenameMap::iterator, bool> mit;
		do
		{
			mit = filename_map_ptr->insert(std::make_pair(name + extension, TFilenameVector(1, name)));  
			if(!mit.second)
			{
				int dupe_count = mit.first->second.size();
				name += "." + to_string( dupe_count);
				(mit.first->second).push_back(name); 
			}
		} while(!mit.second);
		return name + extension;
	}
	};
	static Ext ext;
	
%}
%code{
	stdstring Ext::extension="init";
	stdstring Ext::assets_filename="assets";
	typedef std::map<std::string, std::vector<std::string> > TFilenameMap; 
	auto_ptr<TFilenameMap>  Ext::filename_map_ptr = auto_ptr<TFilenameMap>(new TFilenameMap());
	
%}

# header of resource
type LocalHeader(r_type:uint32) = record {
  name_length: uint32;
  name: bytestring &length = name_length;
  pad:  padding align 4;  
	
} &let {
    extension: stdstring = ext.getExtension();
	real_fullname: stdstring = ext.fullname(name, extension);
	pad_size: uint32 = (name_length%4 == 0)?0:(4 - name_length%4);
}



#local resource
type Resource(r_type:uint32, r_size: uint32, r_fid: uint32, r_offset: uint32) = record {
  local_header_or_not: case ext.hasName(r_type) of {
    true -> local_header: LocalHeader(r_type);
	false -> none:empty;
  };
  content: bytestring &chunked, &length = c_size;  
} &let {
  has_name:bool = ext.hasName(r_type);
  c_size:uint32 =       (has_name == true) ? r_size - local_header.name_length - 4 - local_header.pad_size: r_size ;
  fullname:stdstring =  (has_name == true) ? ext.get_unique_name(local_header.name,local_header.extension) :  ext.generate_name( ext.fid(r_fid), ext.ftype(r_type));
  c_offset:uint32 =     (has_name == true) ? r_offset + local_header.name_length + 4 +   local_header.pad_size : r_offset ;
}


