analyzer DDS withcontext {
connection: Connection;
flow: Flow;
};

%include common.pac

# main storage
type Pac = record {
  magic_number: uint32;
  header: DDS_HEADER;
  header10_or_not: case ext.hasHeader10(header) of {
    true -> header10: DDS_HEADER_DXT10;
	false -> none:empty;
  };
  bdata:  bytestring &transient &length=nbytes;
  bdata2: bytestring &transient &length=nbytes2;
 } &let {
     compressed: bool = ((header.ddspf.dwFlags & DDPF_FOURCC) != 0)? true: false;
     numBlocks:uint32 = ((header.dwWidth + 3)/4) * ((header.dwHeight + 3)/4);
     format:uint32 = ext.detect_format(compressed, header);
     numComponents:uint32 = ext.calc_num_components(format);
     nbytes: uint32 = (compressed == true)? ext.get_compressed_size(header.dwWidth, header.dwHeight, format): ext.get_uncompressed_size(header.dwWidth, header.dwHeight, numComponents);
     nbytes2: uint32 = ext.get_mipmaps_total_size(compressed, header.dwWidth, header.dwHeight, format, numComponents, header);
     
 }  &byteorder=littleendian;

type DDS_HEADER = record {
  dwSize: uint32;
  dwFlags: uint32;
  dwHeight: uint32;
  dwWidth: uint32;
  dwPitchOrLinearSize: uint32;
  dwDepth: uint32;
  dwMipMapCount: uint32;
  dwReserved1: uint32[11];
  ddspf: DDS_PIXELFORMAT;
  dwCaps: uint32;
  dwCaps2: uint32;
  dwCaps3: uint32;
  dwCaps4: uint32;
  dwReserved2: uint32;
}
# typical dds int: argb memory: [bgra] 
type DDS_PIXELFORMAT = record {
  dwSize: uint32;
  dwFlags: uint32;
  dwFourCC: uint32;
  dwRGBBitCount: uint32;
  dwRBitMask: uint32;       # typical 0x00ff0000
  dwGBitMask: uint32;       # typical 0x0000ff00
  dwBBitMask: uint32;       # typical 0x000000ff
  dwABitMask: uint32;       # typical 0xff000000
}

type DDS_HEADER_DXT10 = record {
  dxgiFormat: uint32; #DXGI_FORMAT;
  resourceDimension: uint32; # D3D10_RESOURCE_DIMENSION;
  miscFlag: uint32;
  arraySize: uint32;
  miscFlags2: uint32;
}
enum DDS_HEADER_FLAGS {
    DDSD_CAPS	                                = 0x1,
    DDSD_HEIGHT	                                = 0x2,
    DDSD_WIDTH	                                = 0x4,
    DDSD_PITCH	                                = 0x8,
    DDSD_PIXELFORMAT                            = 0x1000,
    DDSD_MIPMAPCOUNT                            = 0x20000,
    DDSD_LINEARSIZE	                            = 0x80000,
    DDSD_DEPTH	                                = 0x800000
}
enum DDSCAPS_FLAGS {
    DDSCAPS_COMPLEX	 = 0x8,
    DDSCAPS_MIPMAP	= 0x400000,
    DDSCAPS_TEXTURE	= 0x1000
}
enum PIXELFORMAT_FLAGS {
    DDPF_ALPHAPIXELS	= 0x1,
    DDPF_ALPHA	        = 0x2,
    DDPF_FOURCC	        = 0x4,
    DDPF_RGB	        = 0x40,
    DDPF_YUV	        = 0x200,
    DDPF_LUMINANCE	    = 0x20000

}
enum  DXGI_FORMAT {
  DXGI_FORMAT_UNKNOWN                     = 0,
  DXGI_FORMAT_R32G32B32A32_TYPELESS       = 1,
  DXGI_FORMAT_R32G32B32A32_FLOAT          = 2,
  DXGI_FORMAT_R32G32B32A32_UINT           = 3,
  DXGI_FORMAT_R32G32B32A32_SINT           = 4,
  DXGI_FORMAT_R32G32B32_TYPELESS          = 5,
  DXGI_FORMAT_R32G32B32_FLOAT             = 6,
  DXGI_FORMAT_R32G32B32_UINT              = 7,
  DXGI_FORMAT_R32G32B32_SINT              = 8,
  DXGI_FORMAT_R16G16B16A16_TYPELESS       = 9,
  DXGI_FORMAT_R16G16B16A16_FLOAT          = 10,
  DXGI_FORMAT_R16G16B16A16_UNORM          = 11,
  DXGI_FORMAT_R16G16B16A16_UINT           = 12,
  DXGI_FORMAT_R16G16B16A16_SNORM          = 13,
  DXGI_FORMAT_R16G16B16A16_SINT           = 14,
  DXGI_FORMAT_R32G32_TYPELESS             = 15,
  DXGI_FORMAT_R32G32_FLOAT                = 16,
  DXGI_FORMAT_R32G32_UINT                 = 17,
  DXGI_FORMAT_R32G32_SINT                 = 18,
  DXGI_FORMAT_R32G8X24_TYPELESS           = 19,
  DXGI_FORMAT_D32_FLOAT_S8X24_UINT        = 20,
  DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS    = 21,
  DXGI_FORMAT_X32_TYPELESS_G8X24_UINT     = 22,
  DXGI_FORMAT_R10G10B10A2_TYPELESS        = 23,
  DXGI_FORMAT_R10G10B10A2_UNORM           = 24,
  DXGI_FORMAT_R10G10B10A2_UINT            = 25,
  DXGI_FORMAT_R11G11B10_FLOAT             = 26,
  DXGI_FORMAT_R8G8B8A8_TYPELESS           = 27,
  DXGI_FORMAT_R8G8B8A8_UNORM              = 28,
  DXGI_FORMAT_R8G8B8A8_UNORM_SRGB         = 29,
  DXGI_FORMAT_R8G8B8A8_UINT               = 30,
  DXGI_FORMAT_R8G8B8A8_SNORM              = 31,
  DXGI_FORMAT_R8G8B8A8_SINT               = 32,
  DXGI_FORMAT_R16G16_TYPELESS             = 33,
  DXGI_FORMAT_R16G16_FLOAT                = 34,
  DXGI_FORMAT_R16G16_UNORM                = 35,
  DXGI_FORMAT_R16G16_UINT                 = 36,
  DXGI_FORMAT_R16G16_SNORM                = 37,
  DXGI_FORMAT_R16G16_SINT                 = 38,
  DXGI_FORMAT_R32_TYPELESS                = 39,
  DXGI_FORMAT_D32_FLOAT                   = 40,
  DXGI_FORMAT_R32_FLOAT                   = 41,
  DXGI_FORMAT_R32_UINT                    = 42,
  DXGI_FORMAT_R32_SINT                    = 43,
  DXGI_FORMAT_R24G8_TYPELESS              = 44,
  DXGI_FORMAT_D24_UNORM_S8_UINT           = 45,
  DXGI_FORMAT_R24_UNORM_X8_TYPELESS       = 46,
  DXGI_FORMAT_X24_TYPELESS_G8_UINT        = 47,
  DXGI_FORMAT_R8G8_TYPELESS               = 48,
  DXGI_FORMAT_R8G8_UNORM                  = 49,
  DXGI_FORMAT_R8G8_UINT                   = 50,
  DXGI_FORMAT_R8G8_SNORM                  = 51,
  DXGI_FORMAT_R8G8_SINT                   = 52,
  DXGI_FORMAT_R16_TYPELESS                = 53,
  DXGI_FORMAT_R16_FLOAT                   = 54,
  DXGI_FORMAT_D16_UNORM                   = 55,
  DXGI_FORMAT_R16_UNORM                   = 56,
  DXGI_FORMAT_R16_UINT                    = 57,
  DXGI_FORMAT_R16_SNORM                   = 58,
  DXGI_FORMAT_R16_SINT                    = 59,
  DXGI_FORMAT_R8_TYPELESS                 = 60,
  DXGI_FORMAT_R8_UNORM                    = 61,
  DXGI_FORMAT_R8_UINT                     = 62,
  DXGI_FORMAT_R8_SNORM                    = 63,
  DXGI_FORMAT_R8_SINT                     = 64,
  DXGI_FORMAT_A8_UNORM                    = 65,
  DXGI_FORMAT_R1_UNORM                    = 66,
  DXGI_FORMAT_R9G9B9E5_SHAREDEXP          = 67,
  DXGI_FORMAT_R8G8_B8G8_UNORM             = 68,
  DXGI_FORMAT_G8R8_G8B8_UNORM             = 69,
  DXGI_FORMAT_BC1_TYPELESS                = 70,
  DXGI_FORMAT_BC1_UNORM                   = 71,
  DXGI_FORMAT_BC1_UNORM_SRGB              = 72,
  DXGI_FORMAT_BC2_TYPELESS                = 73,
  DXGI_FORMAT_BC2_UNORM                   = 74,
  DXGI_FORMAT_BC2_UNORM_SRGB              = 75,
  DXGI_FORMAT_BC3_TYPELESS                = 76,
  DXGI_FORMAT_BC3_UNORM                   = 77,
  DXGI_FORMAT_BC3_UNORM_SRGB              = 78,
  DXGI_FORMAT_BC4_TYPELESS                = 79,
  DXGI_FORMAT_BC4_UNORM                   = 80,
  DXGI_FORMAT_BC4_SNORM                   = 81,
  DXGI_FORMAT_BC5_TYPELESS                = 82,
  DXGI_FORMAT_BC5_UNORM                   = 83,
  DXGI_FORMAT_BC5_SNORM                   = 84,
  DXGI_FORMAT_B5G6R5_UNORM                = 85,
  DXGI_FORMAT_B5G5R5A1_UNORM              = 86,
  DXGI_FORMAT_B8G8R8A8_UNORM              = 87,
  DXGI_FORMAT_B8G8R8X8_UNORM              = 88,
  DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM  = 89,
  DXGI_FORMAT_B8G8R8A8_TYPELESS           = 90,
  DXGI_FORMAT_B8G8R8A8_UNORM_SRGB         = 91,
  DXGI_FORMAT_B8G8R8X8_TYPELESS           = 92,
  DXGI_FORMAT_B8G8R8X8_UNORM_SRGB         = 93,
  DXGI_FORMAT_BC6H_TYPELESS               = 94,
  DXGI_FORMAT_BC6H_UF16                   = 95,
  DXGI_FORMAT_BC6H_SF16                   = 96,
  DXGI_FORMAT_BC7_TYPELESS                = 97,
  DXGI_FORMAT_BC7_UNORM                   = 98,
  DXGI_FORMAT_BC7_UNORM_SRGB              = 99,
  DXGI_FORMAT_AYUV                        = 100,
  DXGI_FORMAT_Y410                        = 101,
  DXGI_FORMAT_Y416                        = 102,
  DXGI_FORMAT_NV12                        = 103,
  DXGI_FORMAT_P010                        = 104,
  DXGI_FORMAT_P016                        = 105,
  DXGI_FORMAT_420_OPAQUE                  = 106,
  DXGI_FORMAT_YUY2                        = 107,
  DXGI_FORMAT_Y210                        = 108,
  DXGI_FORMAT_Y216                        = 109,
  DXGI_FORMAT_NV11                        = 110,
  DXGI_FORMAT_AI44                        = 111,
  DXGI_FORMAT_IA44                        = 112,
  DXGI_FORMAT_P8                          = 113,
  DXGI_FORMAT_A8P8                        = 114,
  DXGI_FORMAT_B4G4R4A4_UNORM              = 115,
  DXGI_FORMAT_FORCE_UINT                  = 0xffffffff
} 
enum D3D10_RESOURCE_DIMENSION {
  D3D10_RESOURCE_DIMENSION_UNKNOWN    = 0,
  D3D10_RESOURCE_DIMENSION_BUFFER     = 1,
  D3D10_RESOURCE_DIMENSION_TEXTURE1D  = 2,
  D3D10_RESOURCE_DIMENSION_TEXTURE2D  = 3,
  D3D10_RESOURCE_DIMENSION_TEXTURE3D  = 4
}

# check sources of information
# GL_BGRA_EXT int: argb memory:[bgra]
enum DDS_FORMAT
{
    DDS_FORMAT_UNKNOWN              = 0x0,
    DDS_FORMAT_R8G8B8               = 0x1,
    DDS_FORMAT_A8R8G8B8             = 0x2,
    DDS_FORMAT_A4R4G4B4             = 0x7,
    DDS_FORMAT_A8                   = 0x8,
    DDS_FORMAT_DXT1                 = 0x3E,
    DDS_FORMAT_DXT3                 = 0x3F,
    DDS_FORMAT_DXT5                 = 0x40
}
enum GL_TEXTURE_INTERNAL_FORMAT
{
    GL_UNKNOWN                        = 0x0,
    GL_BGR_EXT                        = 0x80E0,
    GL_BGRA_EXT                       = 0x80E1,
    GL_BGRA4444_EXT                   = 0x80E2,
    GL_COMPRESSED_RGB_S3TC_DXT1_EXT   = 0x83F0,
	GL_COMPRESSED_RGBA_S3TC_DXT1_EXT  = 0x83F1,
 	GL_COMPRESSED_RGBA_S3TC_DXT3_EXT  = 0x83F2,
 	GL_COMPRESSED_RGBA_S3TC_DXT5_EXT  = 0x83F3,
}

extern type ext;
extern type stdstring;
%header{
	#include <string>
	#include <custom/custom.h>
    typedef std::string stdstring; 
	
    const unsigned long FOURCC_DXT1 = 0x31545844; //(MAKEFOURCC('D','X','T','1'))
    const unsigned long FOURCC_DXT3 = 0x33545844; //(MAKEFOURCC('D','X','T','3'))
    const unsigned long FOURCC_DXT5 = 0x35545844; //(MAKEFOURCC('D','X','T','5'))
    const unsigned long FOURCC_DX10 = 0x30315844; //(MAKEFOURCC('D','X','1','0'))
    const unsigned long DDS_MAGIC_NUMBER = 0x20534444; // (MAKEFOURCC('D','D','S',' '))
	class Ext{
	public:
		Ext(){};
    static bool hasHeader10(DDS_HEADER* header)
    {
        if ((header->ddspf()->dwFlags() & DDPF_FOURCC) && (header->ddspf()->dwFourCC() == FOURCC_DX10))
            return true;
        return false;
    }
	static uint32 detect_format(bool compressed, DDS_HEADER* header)
    {
        uint32 format;
        const uint32 DDS_RGBA =  DDPF_RGB | DDPF_ALPHAPIXELS;
        if (compressed)
        switch ( header->ddspf()->dwFourCC() )
        {
        case FOURCC_DXT1:
            if ( header->ddspf()->dwFlags() & DDPF_ALPHAPIXELS ) 
                format = DDS_FORMAT_DXT1; //GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
            else
                format = DDS_FORMAT_DXT1 ; //GL_COMPRESSED_RGB_S3TC_DXT1_EXT
            break;

        case FOURCC_DXT3:
            format     = DDS_FORMAT_DXT3;
            break;

        case FOURCC_DXT5:
            format    = DDS_FORMAT_DXT5;
            break;

        default:
            format = DDS_FORMAT_UNKNOWN;
        }
        else //uncompressed
        switch ( header->ddspf()->dwFlags() )
        {
        case DDPF_ALPHA:
            if (header->ddspf()->dwRGBBitCount() == 8)
                format = DDS_FORMAT_A8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;
        case DDS_RGBA:
            if (header->ddspf()->dwRGBBitCount() == 32)
                format = DDS_FORMAT_A8R8G8B8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;

        case DDPF_RGB:
            if ( header->ddspf()->dwRGBBitCount() == 32 ) 
                format = DDS_FORMAT_A8R8G8B8;
            else if ( header->ddspf()->dwRGBBitCount() == 24 )
                format = DDS_FORMAT_R8G8B8;
            else
                format = DDS_FORMAT_UNKNOWN;
            break;

        default:
            format = DDS_FORMAT_UNKNOWN;
        }    
        
        return format;
    }
    static uint32 get_mipmaps_total_size(bool compressed, uint32 width, uint32 height, uint32 format, uint32 num_components, DDS_HEADER* header )
    {
        uint32 h = height >> 1;
        uint32 w = width >> 1;
        uint32 size = 0;
        uint32 total_size = 0;
        
        for (unsigned int i = 0; i < header->dwMipMapCount() && (w || h); i++)
        {
            if (compressed)
                size = get_compressed_size(w, h, format);
            else
                size = get_uncompressed_size(w, h, num_components);
            
            total_size += size;

            h >>= 1;
            w >>= 1;
        }
        
        return total_size;
    }
    static uint32 get_compressed_size(uint32 width, uint32 height, uint32 format)
    {
        uint32 size = ((width+3)/4)*((height+3)/4) *  (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT? 8: 16);
        return size;
    }
    static uint32 get_uncompressed_size(uint32 width, uint32 height, uint32 num_components)
    {
        uint32 size = width * height * num_components;
        return size;
    }
	static uint32 calc_num_components(uint32 format)  
	{
        uint32 num_components;
		switch ( format )
        {
        case GL_BGRA_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
            num_components = 4;
            break;
        case GL_BGR_EXT:
        case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
            num_components = 3;
            break;
        default:
            num_components = 4;
        }
        return num_components;
	}
    };

	static Ext ext;
	
%}
%code{
//	stdstring Ext::extension="init";
//	stdstring Ext::assets_filename="assets";
%}
