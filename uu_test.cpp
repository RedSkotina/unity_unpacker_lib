#include <UnitTest++.h>
#include <TestReporterStdout.h>
#include "unity.h"
#include "iostream"
#include <fstream>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// To add a test, simply put the following code in the a .cpp file of your choice:
//
// =================================
// Simple Test
// =================================
//
//  TEST(YourTestName)
//  {
//  }
//
// The TEST macro contains enough machinery to turn this slightly odd-looking syntax into legal C++, and automatically register the test in a global list.
// This test list forms the basis of what is executed by RunAllTests().
//
// If you want to re-use a set of test data for more than one test, or provide setup/teardown for tests,
// you can use the TEST_FIXTURE macro instead. The macro requires that you pass it a class name that it will instantiate, so any setup and teardown code should be in its constructor and destructor.
//
//  struct SomeFixture
//  {
//    SomeFixture() { /* some setup */ }
//    ~SomeFixture() { /* some teardown */ }
//
//    int testData;
//  };
//
//  TEST_FIXTURE(SomeFixture, YourTestName)
//  {
//    int temp = testData;
//  }
//
// =================================
// Test Suites
// =================================
//
// Tests can be grouped into suites, using the SUITE macro. A suite serves as a namespace for test names, so that the same test name can be used in two difference contexts.
//
//  SUITE(YourSuiteName)
//  {
//    TEST(YourTestName)
//    {
//    }
//
//    TEST(YourOtherTestName)
//    {
//    }
//  }
//
// This will place the tests into a C++ namespace called YourSuiteName, and make the suite name available to UnitTest++.
// RunAllTests() can be called for a specific suite name, so you can use this to build named groups of tests to be run together.
// Note how members of the fixture are used as if they are a part of the test, since the macro-generated test class derives from the provided fixture class.
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct MemCpyFixture
{
    MemCpyFixture()
    {
        CopyFile("../../test/original.file", "../../test/test1.file", false);
        CopyFile("../../test/original.file", "../../test/test2.file", false);
        string _src_name = "../../test/test1.file";
        _src_mmf = new MemoryMappedFile(64*16);
        string _dst_name = "../../test/test2.file";
        _dst_mmf = new MemoryMappedFile(64*16);

        _dst_mmf->Open(_dst_name);
        _src_mmf->Open(_src_name);

        src_buffer = new unsigned char[256];
        dst_buffer = new unsigned char[256];

    };

    ~MemCpyFixture()
    {
        _dst_mmf->Close();
        _src_mmf->Close();
        delete _dst_mmf;
        delete _src_mmf;
        delete src_buffer;
        delete dst_buffer;

    }
    const std::string _dst_name;
    const std::string _src_name;
    MemoryMappedFile* _dst_mmf;
    MemoryMappedFile* _src_mmf;
    unsigned char* src;
    unsigned char* dst;
    unsigned char* src_buffer;
    unsigned char* dst_buffer;
    size_t size;
};

SUITE(MemoryMappedFileTest)
{
    TEST_FIXTURE(MemCpyFixture, MemCpy_NonOverlap_OnePage_OneFile)
    {
        //non overlapped copy , one page, one file
        src = _src_mmf->Begin();
        dst = _src_mmf->Begin()+128;
        size = 64;
        MemCpy( _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _src_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);
    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_NonOverlap_OnePage_TwoFile)
    {
        //non overlapped copy , one page, two file
        src = _src_mmf->Begin();
        dst = _dst_mmf->Begin()+128;
        size = 64;
        MemCpy( _dst_mmf, _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _dst_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);

    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_NonOverlap_TwoPage_OneFile)
    {
        //non overlapped copy , two page, one file
        src = _src_mmf->Begin();
        dst = _src_mmf->Begin()+ _src_mmf->GetPageSize() + 128;
        size = 64;
        MemCpy( _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _src_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);

    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_NonOverlap_TwoPage_TwoFile)
    {
        //non overlapped copy , two page, two file
        src = _src_mmf->Begin();
        dst = _dst_mmf->Begin()+ _dst_mmf->GetPageSize() + 128;
        size = 64;
        MemCpy( _dst_mmf, _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _dst_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);

    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_Overlap_OnePage_OneFile)
    {
        //overlapped copy , one page, one file
        src = _src_mmf->Begin();
        dst = _src_mmf->Begin()+32;
        size = 64;
        MemCpy( _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _src_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);

    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_Overlap_OnePage_TwoFile)
    {
        //overlapped copy , one page, two file
        src = _src_mmf->Begin();
        dst = _dst_mmf->Begin()+32;
        size = 64;
        MemCpy( _dst_mmf, _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _dst_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);

    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_Overlap_TwoPage_OneFile)
    {
        //overlapped copy , two page, one file
        src = _src_mmf->Begin()+ _src_mmf->GetPageSize() - 16;
        dst = _src_mmf->Begin()+ _src_mmf->GetPageSize() + 32;
        size = 64;
        MemCpy( _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _src_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 16); // only check overlaped header

    }
    TEST_FIXTURE(MemCpyFixture, MemCpy_Overlap_TwoPage_TwoFile)
    {
        //overlapped copy , two page, two file
        src = _src_mmf->Begin()+ _src_mmf->GetPageSize() - 16;
        dst = _dst_mmf->Begin()+ _dst_mmf->GetPageSize() + 32;
        size = 64;
        MemCpy( _dst_mmf, _src_mmf, dst, src, size, true);

        _src_mmf->Read(src_buffer, src, 64);
        _dst_mmf->Read(dst_buffer, dst, 64);
        CHECK_ARRAY_EQUAL(src_buffer, dst_buffer, 64);

    }

    TEST(MemMove)
    {
    }


}



SUITE(UnityV4)
{
    TEST(ListV4_6)
    {
        tState* state = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/uogm/sharedassets0.assets";
        data->ArcName = name;

        state = OpenArchive(data);
        CHECK(state);
        tHeaderData* header = new tHeaderData();
        int res = 0;
        int i = 1;
        while(res != E_END_ARCHIVE)
        {
            res = ReadHeader(state, header);
            if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
            i++;
        }

        CloseArchive(state);
    }
    
    TEST(UnpackV4_6)
    {
        tState* state = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.6.0b11/assets/sharedassets17.assets";
        data->ArcName = name;

        state = OpenArchive(data);
        CHECK(state);
        
        tHeaderData* header = new tHeaderData();
        int res = 0;
        int i = 1;
        while(res != E_END_ARCHIVE)
        {
            res = ReadHeader(state, header);
            if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
            i++;
        }
        
        CloseArchive(state);
    }
        TEST(RecursiveUnpackV4_6)
    {
        tState* state = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/uogm/sharedassets0.assets";
        data->ArcName = name;

        state = OpenArchive(data);

        tHeaderData* header = new tHeaderData();
        int res = 0;
        int i = 1;
        while(i != 1856)
        {
            res = ReadHeader(state, header);
            if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
            i++;
        }
        
        
        char dest_path[] = "d:/code/unity/test/uogm/";
        char dest_name[] = "DFC_opening_titles.uogm.ogm\0";


        ProcessFile(state, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(state);
    }

    TEST(Pack_v4)
    {
        tState* assets = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/sharedassets88.assets";
        data->ArcName = name;

        assets = OpenArchive(data);

        tHeaderData* header = new tHeaderData();
        ReadHeader(assets, header);
        ReadHeader(assets, header);

        printf ("%s\n", header->FileName);

        char dest_path[] = "D:/code/unity/test/";
        char dest_name[] = "Actions2x.mat\0";


        ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(assets);


        ofstream testfile;
        testfile.open ("D:/code/unity/test/Actions2x.mat", ios::out | ios::app | ios::binary);
        testfile << (unsigned int)(1001);
        testfile.close();

        PackFiles(name, NULL, dest_path, dest_name, 0);

    }
    
    TEST(MultiPack_V4)
    {
        char name[] = "d:\\code\\unity\\test\\unity4\\assets\\sharedassets1.assets";
        char* sub_path = NULL;
        char dest_path[] = "d:\\code\\unity\\test\\unity4\\assets\\";
        char dest_name[] = "ABPath\0ActivateTrigger\0";


        PackFiles(name, sub_path, dest_path, dest_name, 0);
        
    }

}

SUITE(TexV4)
{
    TEST(Unpack_1)
    {
        tState* assets = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.5.5p2/tex/1/E_GoboCity_01.tex";
        data->ArcName = name;

        assets = OpenArchive(data);

        tHeaderData* header = new tHeaderData();
        ReadHeader(assets, header);
        printf ("%s\n", header->FileName);

        char dest_path[] = "d:/code/unity/test/unity4.5.5p2/tex/1/";
        char dest_name[] = "E_GoboCity_01.tex.dds\0";


        ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(assets);
    }
    TEST(Unpack_2)
    {
        tState* assets = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.2.2f1/tex/loginBackgroundIPadPortrait.tex";
        data->ArcName = name;

        assets = OpenArchive(data);
        CHECK(assets);
        if (!assets)
            return;
        tHeaderData* header = new tHeaderData();
        ReadHeader(assets, header);
        printf ("%s\n", header->FileName);

        char dest_path[] = "d:/code/unity/test/unity4.2.2f1/";
        char dest_name[] = "loginBackgroundIPadPortrait.dds";


        ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(assets);
    }
    TEST(Pack_5)
    {
        char name[] = "d:\\code\\unity\\test\\5pack\\ledBlue.tex";
        char* sub_path = NULL;
        char dest_path[] = "d:\\code\\unity\\test\\5pack\\";
        char dest_name[] = "ledBlue.tex.dds\0";

        PackFiles(name, sub_path, dest_path, dest_name, 0);
        
    }
    TEST(Unpack_10)
    {
        tState* assets = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.5.5p2/tex/10/cf001_head_color.tex";
        data->ArcName = name;

        assets = OpenArchive(data);
        
        tHeaderData* header = new tHeaderData();
        ReadHeader(assets, header);
        printf ("%s\n", header->FileName);

        char dest_path[] = "d:/code/unity/test/unity4.5.5p2/tex/10/";
        char dest_name[] = "cf001_head_color.tex.dds\0";


        ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(assets);
    }
    TEST(Unpack_12)
    {
        tState* assets = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.5.5p2/tex/12/BL_AlgaeDots.tex";
        data->ArcName = name;

        assets = OpenArchive(data);

        tHeaderData* header = new tHeaderData();
        ReadHeader(assets, header);
        printf ("%s\n", header->FileName);

        char dest_path[] = "d:/code/unity/test/unity4.5.5p2/tex/10/";
        char dest_name[] = "BL_AlgaeDots.tex.dds\0";


        ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(assets);
    }
}
SUITE(UOgm)
{
    /*
    TEST(Unpack_V4)
    {
        tState* assets = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.2.2f1/tex/loginBackgroundIPadPortrait.tex";
        data->ArcName = name;

        assets = OpenArchive(data);

        tHeaderData* header = new tHeaderData();
        ReadHeader(assets, header);
        printf ("%s\n", header->FileName);

        char dest_path[] = "d:/code/unity/test/unity4.2.2f1/";
        char dest_name[] = "loginBackgroundIPadPortrait.dds";


        ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

        CloseArchive(assets);
    }
    */
    TEST(Pack_V4)
    {
        char name[] = "d:\\code\\unity\\test\\uogm\\DFC_title_sequence.uogm";
        char* sub_path = NULL;
        char dest_path[] = "d:\\code\\unity\\test\\uogm\\";
        char dest_name[] = "DFC_title_sequence.uogm.ogm\0";

        PackFiles(name, sub_path, dest_path, dest_name, 0);
        
    }
}


SUITE(UnityV5)
{
    TEST(ListV50)
    {
        tState* state = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        //char name[] = "d:/code/unity/test/unity5/assets/sharedassets1.assets";
        char name[] = "d:/code/shadowrun-translate/branches/public/data/input/ios/2.0.9/resources.assets";
        
        data->ArcName = name;

        state = OpenArchive(data);
        CHECK(state);
        tHeaderData* header = new tHeaderData();
        int res = 0;
        int i = 1;
        while(res != E_END_ARCHIVE)
        {
            res = ReadHeader(state, header);
            if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
            i++;
        }

        CloseArchive(state);
    }
    /*
    TEST(UnpackV4_6)
    {
        tState* state = new tState();
        tOpenArchiveData* data = new tOpenArchiveData();

        char name[] = "d:/code/unity/test/unity4.6.0b11/assets/sharedassets17.assets";
        data->ArcName = name;

        state = OpenArchive(data);
        CHECK(state);
        
        tHeaderData* header = new tHeaderData();
        int res = 0;
        int i = 1;
        while(res != E_END_ARCHIVE)
        {
            res = ReadHeader(state, header);
            if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
            i++;
        }
        
        CloseArchive(state);
    }*/
}

void generate_test_files()
{
#ifdef der
    ofstream original_file;
    original_file.open ("D:/code/unity/test/original.file", ios::out  | ios::binary);
    unsigned int original_size = 64 * 16 * (64 * 1024) + 10 * 16 * (64 * 1024);
    for (unsigned int i = 0; i <= original_size; i++)
        original_file << (unsigned char)(i%256);
    original_file.close();
#endif
    FILE* pFile;
    pFile = fopen("D:/code/unity/test/original.file", "wb");
    unsigned int original_size = 64 * 16 * (64 * 1024) + 10 * 16 * (64 * 1024);
    for (unsigned long long j = 0; j < original_size; ++j)
    {
        //Some calculations to fill a[]
        fwrite(&j, 1, 1, pFile);
    }
    fclose(pFile);
}
int test_unpack_tex();
int test_unpack_tex3();
int test_unpack_tex4();
int test_list_unity10();
int test_pack_tex();
int test_plugin_pack_tex();
int test_plugin_unpack_tex_mipmap();
#include "graph.hpp"
int test_graph1()
{
    // Create a graph given in the above diagram
    CGraph::Graph g(4);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 3);
    g.addEdge(3, 3);

    cout << "Following is Breadth First Traversal (starting from vertex 2) \n";
    g.BFS(2);

    return 0;
}
#include <cstdio>
  
class MyTrue;
// run all tests
int main(int argc, char **argv)
{
#ifdef GEN_TEST_FILE
    generate_test_files();
#endif
     bool isSuite = true;
    std::string suiteName = "UnityV5";
    std::string testName  = "";

    UnitTest::TestReporterStdout reporter;
    /*UnitTest::TestRunner runner(reporter);
    
    if (isSuite) {
        runner.RunTestsIf(UnitTest::Test::GetTestList(),
            NULL, MyTrue(suiteName, testName), 0);
    } else {
        runner.RunTestsIf(UnitTest::Test::GetTestList(),
            NULL, UnitTest::True(), 0);
    }*/
    int maxTestTimeInMs = 20000;
    RunAllTests(reporter, UnitTest::Test::GetTestList(), suiteName.c_str(), maxTestTimeInMs);
    //return UnitTest::RunAllTests();
}

















class MyTrue
{
    public:
        MyTrue(const std::string & suiteName, const std::string & testName)
                : suite(suiteName), test(testName) {}

        bool operator()(const UnitTest::Test* const testCase) const
        {
            return suite.compare(testCase->m_details.suiteName) == 0 && 
                         (test.compare(testCase->m_details.testName) == 0 || 
                          test.compare("") == 0);
        }
    private:
        std::string suite;
        std::string test;
};
int test_memmove(bool generate_file)
{
    // prepare test files
    if (generate_file)
    {
        ofstream original_file;
        original_file.open ("D:/code/unity/test/original.file", ios::out  | ios::binary);
        unsigned int original_size = 64 * 16 * (64 * 1024) + 10 * 16 * (64 * 1024);
        for (unsigned int i = 0; i <= original_size; i++)
            original_file << (unsigned char)(i%256);
        original_file.close();
    }
    CopyFile("D:/code/unity/test/original.file", "D:/code/unity/test/test3.file", false);
    CopyFile("D:/code/unity/test/original.file", "D:/code/unity/test/test4.file", false);

    // open test files
    string name1 = "d:/code/unity/test/test3.file";
    MemoryMappedFile mmf1(64*16);
    string name2 = "d:/code/unity/test/test4.file";
    MemoryMappedFile mmf2(64*16);
    mmf1.Open(name1);
    mmf2.Open(name2);

    // execute tests

    unsigned char* src;
    unsigned char* dst;
    size_t size;

    //MemMove

    //non overlapped copy , one page, one file
    src = mmf1.Begin();
    dst = mmf1.Begin()+128;
    size = 64;
    MemMove( &mmf1, &mmf1, dst, src, size);
    //non overlapped copy , one page, two file
    src = mmf1.Begin();
    dst = mmf2.Begin()+128;
    size = 64;
    MemMove( &mmf2, &mmf1, dst, src, size);
    //non overlapped copy , two page, one file
    src = mmf1.Begin();
    dst = mmf1.Begin()+ mmf1.GetPageSize() + 128;
    size = 64;
    MemMove( &mmf1, &mmf1, dst, src, size);
    //non overlapped copy , two page, two file
    src = mmf1.Begin();
    dst = mmf2.Begin()+ mmf2.GetPageSize() + 128;
    size = 64;
    MemMove( &mmf2, &mmf1, dst, src, size);

    //overlapped copy , one page, one file
    src = mmf1.Begin();
    dst = mmf1.Begin()+32;
    size = 64;
    MemMove( &mmf1, &mmf1, dst, src, size);
    //overlapped copy , one page, two file
    src = mmf1.Begin();
    dst = mmf2.Begin()+32;
    size = 64;
    MemMove( &mmf2, &mmf1, dst, src, size);
    //overlapped copy , two page, one file
    src = mmf1.Begin()+ mmf1.GetPageSize() - 16;
    dst = mmf1.Begin()+ mmf1.GetPageSize() + 32;
    size = 64;
    MemMove( &mmf1, &mmf1, dst, src, size);
    //overlapped copy , two page, two file
    src = mmf1.Begin()+ mmf1.GetPageSize() - 16;
    dst = mmf2.Begin()+ mmf2.GetPageSize() + 32;
    size = 64;
    MemMove( &mmf2, &mmf1, dst, src, size);


    mmf1.Close();
    mmf2.Close();




}


int test_unity_unpack()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/sharedassets0.assets";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    ReadHeader(assets, header);

    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "Conversation2x.mat";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    /*
    ofstream testfile;
    testfile.open ("D:/code/unity/test/background.mat", ios::out | ios::app | ios::binary);
    testfile << (unsigned int)(1001);
    testfile.close();
    */
    //PackFiles(name, NULL, dest_path, dest_name, 0);


}
int test_unity3()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/unity32.assets";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    ReadHeader(assets, header);

    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "background.mat";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    /*
    ofstream testfile;
    testfile.open ("D:/code/unity/test/background.mat", ios::out | ios::app | ios::binary);
    testfile << (unsigned int)(1001);
    testfile.close();
    */
    //PackFiles(name, NULL, dest_path, dest_name, 0);


}

int test_unity5()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/sharedassets0.assets";
    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "sharedassets0_1875.-9";


    PackFiles(name, NULL, dest_path, dest_name, 0);


}

int test_multiply_unpack()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/sharedassets88.assets";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "background.mat";

    for (int i = 1; i <= 60; i++)
    {
        ReadHeader(assets, header);
        printf ("%i: %s\n", i, header->FileName);
        ProcessFile(assets, PK_EXTRACT, dest_path, header->FileName );
    }

    CloseArchive(assets);

}

int test_multiply_unpack2()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/cypher4.assets";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();

    char dest_path[] = "D:/code/unity/test/t/";

    for (int i = 1; i <= 350; i++)
    {
        ReadHeader(assets, header);
        printf ("%i: %s\n", i, header->FileName);
        if (header->FileAttr == AT_DIR)
        {
            printf("DIR\n");
            CreateDirectory((std::string(dest_path)+ std::string(header->FileName)).c_str(),NULL);
        }
        else
            ProcessFile(assets, PK_EXTRACT, dest_path, header->FileName );

    }

    CloseArchive(assets);

}
int test_multiply_unpack3()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/cypher4.assets";
    data->ArcName = name;

    char dest_path[] = "D:/code/unity/test/t/";

    tHeaderData* header = new tHeaderData();

    // read main list
    assets = OpenArchive(data);
    SetProcessDataProc(assets,nullptr);
    SetChangeVolProc(assets,nullptr);
    int i = -1;
    int error_code = 0;
    while (1)
    {
        i++;
        error_code = ReadHeader(assets, header);
        std::cout<<i<<" "<<header->FileName<<std::endl;
        if (error_code == E_END_ARCHIVE)
            break;
        ProcessFile(assets, PK_SKIP, dest_path, header->FileName );
    }
    CloseArchive(assets);
    //process 20 files
    int counter;
    int ids[] = {79,64,50,71,62,66,96,92,70,81,77,54,48,67,65,85,57,52,63,50};
    for (int loop = 0; loop < 20; loop++)
    {

        assets = OpenArchive(data);
        SetProcessDataProc(assets,nullptr);
        SetChangeVolProc(assets,nullptr);
        counter = 0;
        int i = -1;
        while(1)
        {
            i++;
            ReadHeader(assets, header);
            counter = assets->plugins_chain->front().second->counter;
            if (counter == ids[loop])
            {
                if (header->FileAttr == AT_DIR)
                {
                    CreateDirectory((std::string(dest_path)+ std::string(header->FileName)).c_str(),NULL);
                    ProcessFile(assets, PK_SKIP, dest_path, header->FileName );
                }
                ReadHeader(assets, header);
                ProcessFile(assets, PK_EXTRACT, dest_path, header->FileName );
                break;
            }
            else
                ProcessFile(assets, PK_SKIP, dest_path, header->FileName );
        }
        CloseArchive(assets);
    }
    delete header;
    delete assets;

}
int test_list_unity7()
{
    tState* state = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/sharedassets0.assets";
    data->ArcName = name;

    state = OpenArchive(data);
    if (!state)
        return -1;
    tHeaderData* header = new tHeaderData();
    int res = 0;
    int i = 1;
    while(res != E_END_ARCHIVE)
    {
        res = ReadHeader(state, header);
        if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
        i++;
    }


    CloseArchive(state);

}
int test_list_unity8()
{
    tState* state = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/MediumNormal2x_0.tex";
    data->ArcName = name;

    state = OpenArchive(data);
    if (!state)
        return -1;
    tHeaderData* header = new tHeaderData();
    int res = 0;
    int i = 1;
    while(res != E_END_ARCHIVE)
    {
        res = ReadHeader(state, header);
        if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
        i++;
    }


    CloseArchive(state);

}

int test_can_handle_unity9()
{

    char name[] = "d:/code/unity/test/MediumNormal2x_0.tex";
    bool res = CanYouHandleThisFile(name);
    printf("CanYouHandleThisFile = %d", res);

}

int test_unpack_tex()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/MediumNormal2x_0.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "MediumNormal2x_0.dds";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    //PackFiles(name, NULL, dest_path, dest_name, 0);


}

int test_pack_tex()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:\\code\\unity\\test\\MediumNormal2x_0.tex";
    char dest_path[] = "D:\\code\\unity\\test\\";
    char dest_name[] = "MediumNormal2x_0.tga\0";


    PackFiles(name, NULL, dest_path, dest_name, 0);

}

int test_unpack_tex2()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/mainMenuBGRaw.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "mainMenuBGRaw.tga";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    //PackFiles(name, NULL, dest_path, dest_name, 0);


}

int test_unpack_tex3()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/toggle.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "toggle.tga";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    //PackFiles(name, NULL, dest_path, dest_name, 0);


}
int test_unpack_tex4()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/MediumNormal1x_0.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "MediumNormal1x_0.dds";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    //PackFiles(name, NULL, dest_path, dest_name, 0);


}
int test_unpack_tex5()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/toggle.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "D:/code/unity/test/";
    char dest_name[] = "toggle.dds";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    //PackFiles(name, NULL, dest_path, dest_name, 0);


}

int test_unpack_tex6()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/3.4.2/5/24floor.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "d:/code/unity/test/3.4.2/5/";
    char dest_name[] = "24floor.dds";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);

    //PackFiles(name, NULL, dest_path, dest_name, 0);


}
int test_list_unity10()
{
    tState* state = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/sharedassets2.assets";
    data->ArcName = name;

    state = OpenArchive(data);
    if (!state)
        return -1;
    tHeaderData* header = new tHeaderData();
    int res = 0;
    int i = 1;
    while(res != E_END_ARCHIVE)
    {
        res = ReadHeader(state, header);
        if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
        i++;
    }


    CloseArchive(state);

}

int test_plugin_pack_assets()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:\\code\\unity\\test\\cypher4.assets";
    char sub_path[] = "cgun.tex";
    char dest_path[] = "D:\\code\\unity\\test\\";
    char dest_name[] = "cgun.tex.dds\0";


    PackFiles(name, sub_path, dest_path, dest_name, 0);

}

int test_plugin_pack_tex()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:\\code\\unity\\test\\cgun.tex";
    char* sub_path = nullptr;
    char dest_path[] = "D:\\code\\unity\\test\\";
    char dest_name[] = "cgun.tex.dds\0";


    PackFiles(name, sub_path, dest_path, dest_name, 0);

}
int test_plugin_pack_assets_tex()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:\\code\\unity\\test\\cypher4.assets";
    char sub_path[] = "cgun.tex";
    char dest_path[] = "D:\\code\\unity\\test\\";
    char dest_name[] = "cgun.tex.dds\0";


    PackFiles(name, sub_path, dest_path, dest_name, 0);

}
int test_plugin_unpack_tex_mipmap()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/unity4/tex_mipmap/4/HelpBG2x.tex";
    data->ArcName = name;

    assets = OpenArchive(data);

    tHeaderData* header = new tHeaderData();
    ReadHeader(assets, header);
    printf ("%s\n", header->FileName);

    char dest_path[] = "d:/code/unity/test/unity4/tex_mipmap/4/";
    char dest_name[] = "HelpBG2x.dds";


    ProcessFile(assets, PK_EXTRACT, dest_path, dest_name );

    CloseArchive(assets);
}

int test_list_unity9()
{
    tState* state = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/unity4/assets/sharedassets0.assets";
    data->ArcName = name;

    state = OpenArchive(data);
    if (!state)
        return -1;
    tHeaderData* header = new tHeaderData();
    int res = 0;
    int i = 1;
    while(res != E_END_ARCHIVE)
    {
        res = ReadHeader(state, header);
        if (res != E_END_ARCHIVE) printf ("%i: %s\n", i, header->FileName);
        i++;
    }


    CloseArchive(state);

}

int test_pack_assets()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:\\code\\unity\\test\\resources.assets";
    char* sub_path = NULL;
    char dest_path[] = "D:\\code\\unity\\test\\";
    char dest_name[] = "code.mov.xml.bcg\0";


    PackFiles(name, sub_path, dest_path, dest_name, 0);

}

int test_pack_assets2()
{
    tState* assets = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    char name[] = "d:/code/unity/test/resources.assets";
    data->ArcName = name;

    char dest_path[] = "D:/code/unity/test/";

    tHeaderData* header = new tHeaderData();

    // read main list
    assets = OpenArchive(data);
    SetProcessDataProc(assets,nullptr);
    SetChangeVolProc(assets,nullptr);
    int i = -1;
    int error_code = 0;
    while (1)
    {
        i++;
        error_code = ReadHeader(assets, header);
        std::cout<<i<<" "<<header->FileName<<std::endl;
        if (error_code == E_END_ARCHIVE)
            break;
        ProcessFile(assets, PK_SKIP, dest_path, header->FileName );
    }
    CloseArchive(assets);
    /*//process 20 files
    int counter;
    int ids[] = {79,64,50,71,62,66,96,92,70,81,77,54,48,67,65,85,57,52,63,50};
    for (int loop = 0; loop < 20; loop++)
    {

        assets = OpenArchive(data);
        SetProcessDataProc(assets,nullptr);
        SetChangeVolProc(assets,nullptr);
        counter = 0;
        int i = -1;
        while(1)
        {
            i++;
            ReadHeader(assets, header);
            counter = assets->plugins_chain->front().second->counter;
            if (counter == ids[loop])
            {
                if (header->FileAttr == AT_DIR)
                {
                    CreateDirectory((std::string(dest_path)+ std::string(header->FileName)).c_str(),NULL);
                    ProcessFile(assets, PK_SKIP, dest_path, header->FileName );
                }
                ReadHeader(assets, header);
                ProcessFile(assets, PK_EXTRACT, dest_path, header->FileName );
                break;
            }
            else
                ProcessFile(assets, PK_SKIP, dest_path, header->FileName );
        }
        CloseArchive(assets);
    }
    */
    delete header;
    delete assets;

}
