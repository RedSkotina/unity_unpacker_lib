#ifndef COMMON_H
#define COMMON_H
#include <string>
std::string extract_filename_from_absolute_filename(std::string& abs_filename);
std::string extract_basename_from_filename(std::string& filename);
        
#endif // COMMON_H