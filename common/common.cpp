#include "common.h"
std::string extract_filename_from_absolute_filename(std::string& abs_filename)
{
    const size_t last_slash_idx = abs_filename.find_last_of("\\/");
    std::string filename = abs_filename.substr(last_slash_idx+1);
    return filename;
}
std::string extract_basename_from_filename(std::string& filename)
{
    size_t basename_len = filename.find_last_of('.');
	std::string basename = filename.substr(0, basename_len);
    return basename;
}	