#include "unity.h"
#include "iostream"
#include <fstream>
#include "tclap/CmdLine.h"


int extract_content(char* assets_filename, std::string& dst_extract_path, std::string& extract_filename )
{

    //std::string filename = dst_extract_filename.substr(dst_extract_filename.find_last_of('\\') + 1);


    tState* state = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    data->ArcName = assets_filename;

    state = OpenArchive(data);
    if (!state)
        return -1;

    tHeaderData* header = new tHeaderData();
    int res = 0;
    int res2 = -1;
    std::string filename;
    while(res != E_END_ARCHIVE && res2 != 0)
    {
        res = ReadHeader(state, header);
        filename = header->FileName;
        if (extract_filename == filename)
            res2 = ProcessFile(state, PK_EXTRACT, const_cast<char*>(dst_extract_path.c_str()), header->FileName );
    }

    CloseArchive(state);
    return res;

}
int list_content(char* assets_filename)
{
    tState* state = new tState();
    tOpenArchiveData* data = new tOpenArchiveData();

    data->ArcName = assets_filename;

    state = OpenArchive(data);
    if (!state)
        return -1;
    tHeaderData* header = new tHeaderData();
    int res = 0;
    int i = 1;
    while(res != E_END_ARCHIVE)
    {
        res = ReadHeader(state, header);
        if (res != E_END_ARCHIVE)
            if (header->FileAttr == AT_DIR)
            {
                printf ("DIR: %s\n", header->FileName);
                continue;
            }
            else
                printf ("%i: %s\n", i, header->FileName);

        i++;
    }

    CloseArchive(state);
    return 0;
}

std::string& replace_slash(std::string& s)
{
    std::replace( s.begin(), s.end(), '/', '\\');
    return s;
}
int main(int argc, char *argv[])
{

    try
    {

        // Define the command line object.
        TCLAP::CmdLine cmd("Unity Unpacker", ' ', "0.2");

        // Define a value argument and add it to the command line.
        TCLAP::ValueArg<string> assets_filename_arg("f","packed_file","path to packed file",true,"","string");
        cmd.add( assets_filename_arg );

        // Define a switch and add it to the command line.
        TCLAP::SwitchArg list_switch("l","list","list content assets file", false);
        TCLAP::SwitchArg extract_switch("e","extract","extract file from assets file", false);

        // Define a value argument and add it to the command line.
        TCLAP::ValueArg<string> src_path_arg("s","src_path","source path",false,"","string");
        cmd.add( src_path_arg );
        // Define a value argument and add it to the command line.
        TCLAP::ValueArg<string> int_path_arg("i","int_path","internal path",false,"","string");
        cmd.add( int_path_arg );
        TCLAP::UnlabeledValueArg<string>  filename_arg( "filename", "extract or pack filename", false, "./", "string"  );
        cmd.add(filename_arg);

        TCLAP::SwitchArg pack_switch("p","pack","pack file to assets file", false);

        vector<TCLAP::Arg*>  xorlist;
        xorlist.push_back(&list_switch);
        xorlist.push_back(&extract_switch);
        xorlist.push_back(&pack_switch);
        cmd.xorAdd( xorlist );


        // Parse the args.
        cmd.parse( argc, argv );

        // Get the value parsed by each arg.
        std::string assets_filename = assets_filename_arg.getValue();
        std::string src_path = src_path_arg.getValue();
        std::string int_path = int_path_arg.getValue();
        std::string filename = filename_arg.getValue();


        replace_slash(assets_filename);
        replace_slash(src_path);
        replace_slash(int_path);
        replace_slash(filename);
        // Do what you intend too...
        if ( list_switch.isSet() )
        {
            list_content(const_cast<char*>(assets_filename.c_str()));
        }
        else if ( extract_switch.isSet() )
        {

            int res = extract_content(const_cast<char*>(assets_filename.c_str()), src_path, filename  );
            if (res == E_END_ARCHIVE)
            {
                cout<<"ERROR: "<<filename<<" not found in "<<assets_filename<<endl;
                return EXIT_FAILURE;

            }
        }
        else if ( pack_switch.isSet() )
        {
            filename += '\0' ;
            int res = PackFiles(const_cast<char*>(assets_filename.c_str()), const_cast<char*>(int_path.c_str()), const_cast<char*>(src_path.c_str()), const_cast<char*>(filename.c_str()), 0);
            if (res == E_NO_FILES)
            {
                cout<<"ERROR: file "<<filename<<" not found in packed file "<<assets_filename<<endl;
                return EXIT_FAILURE;
            }
            if (res == E_UNKNOWN_FORMAT)
            {
                cout<<"ERROR: packed file "<<assets_filename<< " not found or unknown format  "<<endl;
                return EXIT_FAILURE;
            }

            return res;
        }
        else
        {
            return EXIT_FAILURE;
        }

    }
    catch (TCLAP::ArgException &e)  // catch any exceptions
    {
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;




}
