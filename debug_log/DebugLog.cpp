#include "DebugLog.hpp"

#if DEBUG_LOG_ENABLE

#include <exception>

namespace bornander
{
	namespace debug
	{
		int log::indentation = 0;
		std::ostream* log::stream = &std::cout;
		bool log::logfile_first_use = true;
	
		log::log(const std::string& ctx, bool silence)
			: context(ctx), _silence(silence)
#ifdef DEBUG_LOG_ENABLE_TIMING
			, start_time(clock())
#endif
		{
			if (logfile_using)
			{
				if (logfile_first_use)
				{
					logfile.open(log_filename.c_str());
					logfile_first_use = false;
				}
				else
					logfile.open(log_filename.c_str(),  std::fstream::out | std::fstream::app);
				bornander::debug::log::set_stream(logfile);
			}
			if (!_silence)
			{
			write_indentation();
			*stream << "--> " << context << std::endl;
			++indentation;
			stream->flush();
			
			}
		}

		log::~log()
		{
			if (!_silence)
			{
			--indentation;
			write_indentation(std::uncaught_exception() ? '*' : ' ');
			*stream << "<-- " << context;
#ifdef DEBUG_LOG_ENABLE_TIMING
			*stream << " in " << ((double)(clock() - start_time) / CLOCKS_PER_SEC) << "s";
#endif
			*stream << std::endl;
			stream->flush();
			//extern bool logfile_using;
			if (logfile_using)
			{
				//extern std::ofstream logfile;
				logfile.close();
				log::stream = &std::cout;
			}
			}
		}

		void log::set_stream(std::ostream& stream)
		{
			log::stream = &stream;
		}


		void log::write_indentation()
		{
			write_indentation(' ');
		}

		void log::write_indentation(const char prefix)
		{
            if (logfile_using) // reenable logfile after exit from sub function
            {
                bornander::debug::log::set_stream(logfile);
			}
            *stream << prefix;
			for(int i = 0; i < indentation * 2; ++i)
			{
				*stream << " ";
			}
		}

		void log::message(const std::string& message)
		{
			write_indentation();
			*stream << message << std::endl;
			stream->flush();
		}

	}
}
#endif