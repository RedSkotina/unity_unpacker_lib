#pragma warning(push)
#pragma warning(disable:4200)
#include "unity.h"
#pragma warning(pop)
#include "stdafx.h"
#include "stdio.h"
#include <stdlib.h>
#include <ctype.h>
#include <algorithm>

#include <assert.h>
#include <exception>
#include <stdexcept>
#include <system_error>
#include <memory>


std::string get_current_dir();
DEBUG_TO_FILE((get_current_dir()+std::string("\\unity-log.txt")).c_str());

HMODULE GetModuleHandleFromAddress( PVOID pAddress ) throw()
{
    MEMORY_BASIC_INFORMATION MemInfo;
    VirtualQuery(pAddress, &MemInfo, sizeof(MemInfo));
    return ( static_cast<HMODULE>(MemInfo.AllocationBase) );
}

//------------------=[ Global Varailables ]=-------------

tChangeVolProc pGlobChangeVol;
tProcessDataProc pGlobProcessData;

BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    DEBUG_METHOD();
    if( !hModule )
        return FALSE;

    return TRUE;
}


STATE_HANDLE unity_open(tOpenArchiveData *ArchiveData)
{
    DEBUG_METHOD();
    int result;
    tState* state = new tState() ;
    std::string arc_abs_filename(ArchiveData->ArcName);
    std::replace( arc_abs_filename.begin(), arc_abs_filename.end(), '/', '\\');
    DEBUG_VALUE_OF(arc_abs_filename.c_str());
    //---- select plugin -----
    Storage* current_plugin = nullptr;

    UPlugins* plugins = new UPlugins();
    state->plugins = plugins;

    auto all_plugins = state->plugins->get_all();
    for (auto it = all_plugins->begin() ; it != all_plugins->end() ; ++it)
    {
        std::unique_ptr<Memory> mem = (*it)->mem_pre_open(arc_abs_filename);
        if (!mem)
        {
            DEBUG_MESSAGE("Error: mem == NULL");
            delete state->plugins;
            delete state;
            ArchiveData->OpenResult=E_EOPEN;
            return NULL;
        }
        if ((*it)->mem_can_handle(mem))
        {
            current_plugin = *(it);
            DEBUG_MESSAGE("CAN_HANDLE: " + (*it)->description());
        }
        (*it)->mem_post_close(mem);
    }
    delete all_plugins;

    if (! current_plugin)
    {
        DEBUG_MESSAGE("Error: all plugins refused process this file");
        delete state->plugins;
        delete state;
        ArchiveData->OpenResult=E_EREAD;
        return NULL;
    }

    try
    {

        ArchiveData->CmtBuf     = 0;
        ArchiveData->CmtBufSize = 0;
        ArchiveData->CmtSize    = 0;
        ArchiveData->CmtState   = 0;
        ArchiveData->OpenResult = E_NO_MEMORY;// default error type

        std::unique_ptr<Memory> mem = current_plugin->mem_pre_open(arc_abs_filename);
        StorageState* current_plugin_state = current_plugin->mem_open(mem);

        state->plugins_chain = new std::list<std::pair<Storage*,StorageState*>>();
        state->plugins_chain->push_back(std::make_pair(current_plugin,current_plugin_state));
        ArchiveData->OpenResult = 0;// ok

        return state;


    }
    catch (std::system_error& exc)
    {
        DEBUG_MESSAGE("Error: E_EREAD");
        if (state->hArchFile!=NULL) CloseHandle(state->hArchFile);
        delete state->plugins;
        delete state;
        ArchiveData->OpenResult=E_EREAD;
        //logfile.close();
        return NULL;
    }
    catch (std::runtime_error& exc)
    {
        DEBUG_MESSAGE("Error: runtime error");
        DEBUG_MESSAGE(exc.what());
        if (state->hArchFile!=NULL) CloseHandle(state->hArchFile);
        delete state->plugins;
        delete state;
        ArchiveData->OpenResult=E_EREAD;
        //logfile.close();
        return NULL;
    }

};

/* tc listdir
 * tc -> open
 * tc -> nextitem
 * tc -> nextitem
 * tc-> close
 */

int unity_next_item(STATE_HANDLE hArcData, tHeaderData *HeaderData)
{
    DEBUG_METHOD();

    minIni ini(get_current_dir() + std::string("\\unity.ini"));
    bool recursion = ini.getbool( "unity", "recursion" , false );
    DEBUG_VALUE_OF(recursion);

    tState* state=(tState*)(hArcData);

    if (state->plugins_chain->empty())
    {
        DEBUG_MESSAGE("Error: Plugins chain is empty. Is archive not open ?");
        return E_EOPEN;
    }

main:
    Storage* current_plugin = state->plugins_chain->back().first;
    StorageState* current_plugin_state = state->plugins_chain->back().second;

    // ------------------ Get current file
    pHeaderData* header = current_plugin->next_item(current_plugin_state);

    // ------------------ Check for end current folder
    if (header == nullptr)
    {
        DEBUG_MESSAGE("Warning: End of folder");
        if (state->plugins_chain->size() > 1)
        {
            current_plugin->mem_close(current_plugin_state);
            state->plugins_chain->pop_back();
            goto main; // continue with next file.
        }
        DEBUG_MESSAGE("Warning: End of archive");
        return E_END_ARCHIVE;
    }
    // ----------------   prepare file or folder for return
    std::string path = "";
    auto start_it = state->plugins_chain->begin();
    if (state->plugins_chain->size() > 1)
    {

        start_it++;
        for (auto it = start_it; it != state->plugins_chain->end(); it++)
            path += (*it).second->filename + "\\";
    }
    //for (const auto& i : *(state->plugins_chain))
    //    path += i.second->filename + "\\";

    std::string arc_abs_filename(current_plugin_state->mmf->GetFullPath());
    strcpy(HeaderData->ArcName, arc_abs_filename.c_str());
    strcpy(HeaderData->FileName, (path + header->file_name).c_str());
    HeaderData->FileAttr = AT_OK;
    HeaderData->FileTime = header->file_time;
    HeaderData->PackSize = header->pack_size;
    HeaderData->UnpSize  = header->unp_size;
    HeaderData->CmtBuf=0;
    HeaderData->CmtBufSize=0;
    HeaderData->CmtSize=0;
    HeaderData->CmtState=0;
    HeaderData->UnpVer=0;
    HeaderData->Method=0;
    HeaderData->FileCRC=0;
    delete header;
    DEBUG_VALUE_OF(current_plugin_state->counter);
    DEBUG_VALUE_OF(HeaderData->FileName);
    
    // simple view ?
    if (!recursion)
        return 0;
    // ------------   Check current file is subfolder
    std::unique_ptr<Memory> mem = current_plugin->get_mem(current_plugin_state);
    if (!mem)
    {
        DEBUG_MESSAGE("cant get mem. it is normal");
        return 0;
    }
    auto childs_plugins =  state->plugins->get_childs(current_plugin->get_id().id);
    for (auto it = childs_plugins->begin() ; it != childs_plugins->end() ; ++it)
    {
        if ((*it)->mem_can_handle(mem))
        {
            DEBUG_MESSAGE("CHILD CAN_HANDLE: " + (*it)->description());
            auto sub_plugin = (*it);
            auto sub_plugin_state = sub_plugin->mem_open(mem);
            state->plugins_chain->push_back(std::make_pair(sub_plugin, sub_plugin_state));
            HeaderData->FileAttr = AT_DIR;
        }
    }
    
    delete childs_plugins;
    
    return 0;//ok
};

int unity_process(STATE_HANDLE hArcData, int Operation, char *DestPath, char *DestName)
{
    DEBUG_METHOD();
    DEBUG_VALUE_OF(Operation);
    //DEBUG_MESSAGE(DestPath);
    //DEBUG_MESSAGE(DestName);
    tState *state=hArcData;
    bool result;

    if( Operation == PK_SKIP || Operation == PK_TEST ) return 0;


    char Dest[260]="";
    if (DestPath) strcpy(Dest,DestPath);
    if (DestName) strcat(Dest,DestName);
    std::string dst_abs_name = Dest;
    std::replace( dst_abs_name.begin(), dst_abs_name.end(), '/', '\\');

    if (state->plugins_chain->empty())
    {
        DEBUG_MESSAGE("Error: Plugins chain is empty. Is archive not open ?");
        return E_EOPEN;
    }

    Storage* current_plugin = state->plugins_chain->back().first;
    StorageState* current_plugin_state = state->plugins_chain->back().second;

    result = current_plugin->unpack(current_plugin_state, dst_abs_name.c_str());

    if (!result)
    {
        DEBUG_MESSAGE("Error: Wrong unpack");
        return E_UNKNOWN_FORMAT;
    }

    return 0;//ok
};

int unity_close(STATE_HANDLE hArcData)
{
    DEBUG_METHOD();

    tState *state=hArcData;

    if (state->plugins_chain->empty())
    {
        DEBUG_MESSAGE("Error: Plugins chain is empty. Is archive not open ?");
        return E_EOPEN;
    }

    auto mem = make_unique<Memory>();
    auto front = state->plugins_chain->front();
    mem->mmf = front.second->mmf;

    for (const auto& i : reverse(*(state->plugins_chain)))
        i.first->mem_close(i.second);

    state->plugins_chain->front().first->mem_post_close(mem);

    state->plugins_chain->clear();
    delete state->plugins_chain;
    state->plugins_chain = nullptr;
    delete state->plugins;
    state->plugins = nullptr;

    return 0;// ok
};

int SplitPackList(char *AddList, std::list<char*> &pack_list)
{

    char* n = AddList;
    while( *n != 0 )
    {
        pack_list.push_back(n);
        //OutputDebugString(n);
        while( *n != 0)
        {
            n++;
        }
        n++;
    }
}
char *my_strdup(const char *str)
{
    size_t len = strlen(str);
    char *x = (char *)malloc(len+1); /* 1 for the null terminator */
    if(!x) return NULL; /* malloc could not allocate memory */
    memcpy(x,str,len+1); /* copy the string into the new buffer */
    return x;
}
int SplitSubList(char* SubPath,std::list<std::string> &sub_list)
{
    string sub_path;
    if (SubPath)
        sub_path = SubPath;
    else
        sub_path = "";

    char* pTempStr = my_strdup( sub_path.c_str() );
    char* delim = "\\";
    char* pWord = strtok(pTempStr, delim);
    while(pWord != NULL)
    {
        sub_list.push_back(std::string(pWord));
        pWord = strtok(NULL, delim);
    }

    free(pTempStr);

};
int unity_pack (char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags)
{
    DEBUG_METHOD();
    DEBUG_VALUE_OF(PackedFile);
    if (SubPath != NULL) DEBUG_VALUE_OF(SubPath);
    DEBUG_VALUE_OF(SrcPath);
    DEBUG_VALUE_OF(AddList);
    DEBUG_VALUE_OF(Flags);

    int pack_result = 0;
    std::list<char*> pack_list;
    SplitPackList(AddList, pack_list);
    // list<char*> => list<string>
    std::list<std::string> pack_list_str;
    std::list<char*>::iterator lit;
    for (lit = pack_list.begin() ; lit != pack_list.end() ; ++lit)
        pack_list_str.push_back(std::string(*lit));
    
    // multifile
    std::list<string>::iterator slit;
    for (slit = pack_list_str.begin() ; slit != pack_list_str.end() ; ++slit)
    {
        // current file
        std::list<std::string> cur_pack_list;
        cur_pack_list.push_back(*slit);


    string arc_abs_filename = PackedFile;
    std::replace( arc_abs_filename.begin(), arc_abs_filename.end(), '/', '\\');
    string src_path = SrcPath;

    std::list<std::string> sub_list;
    SplitSubList(SubPath, sub_list);


    tState *state = new tState();
    UPlugins* plugins = new UPlugins();
    state->plugins = plugins;
    Storage* current_plugin = nullptr;

    auto all_plugins = state->plugins->get_all();
    for (auto it = all_plugins->begin() ; it != all_plugins->end() ; ++it)
    {
        std::unique_ptr<Memory> mem = (*it)->mem_pre_open(arc_abs_filename);
        if (!mem)
        {
            DEBUG_MESSAGE("Error: mem == NULL");
            return E_EREAD;
        }
        if ((*it)->mem_can_handle(mem))
        {
            current_plugin = *(it);
            DEBUG_MESSAGE("CAN_HANDLE: " + (*it)->description());
        }
        (*it)->mem_post_close(mem);

    }
    delete all_plugins;

    if (! current_plugin)
    {
        DEBUG_MESSAGE("Error: current_plugin not found or all plugins refused process this file");
        delete state->plugins;
        delete state;
        return E_UNKNOWN_FORMAT;
    }

    minIni ini(get_current_dir() + std::string("\\unity.ini"));
    bool recursion = ini.getbool( "unity", "recursion" , false );
    DEBUG_VALUE_OF(recursion);
    // find sub plugins
    std::list<std::pair<std::unique_ptr<Memory>,Storage*>> plugins_chain;

    //Build plugins chain
    //Storage* current_plugin = state->current_plugin;
    //TODO: use pair m_open, mem_open
    std::unique_ptr<Memory>  first_mem = current_plugin->mem_pre_open(arc_abs_filename);
    //---
    plugins_chain.push_back(std::make_pair(std::move(first_mem),current_plugin));
    if (recursion)
        for (auto sub_list_it = sub_list.begin() ; sub_list_it != sub_list.end() ; ++sub_list_it)
        {
            std::string sub_file = (*sub_list_it);

            StorageState* plugin_state = current_plugin->mem_open(plugins_chain.back().first);
            std::unique_ptr<Memory> sub_mem = current_plugin->get_mem_by_name(plugin_state, sub_file);
            current_plugin->mem_close(plugin_state);
            if (!sub_mem)
            {
                DEBUG_MESSAGE(sub_file);
                DEBUG_MESSAGE("cannot get sub_mem . check sub_path.");
                current_plugin->mem_post_close(plugins_chain.front().first);
                ////std::for_each(plugins_chain.begin(), plugins_chain.end(), [](std::pair<unique_ptr<Memory>,Storage*> &v){ delete v.first; });
                plugins_chain.clear();
                return E_END_ARCHIVE;
                break;
            }


            auto childs_plugins =  state->plugins->get_childs(current_plugin->get_id().id);
            //std::vector<Storage*>::iterator it;
            bool bFound = false;
            for (auto it = childs_plugins->begin() ; it != childs_plugins->end() ; ++it)
            {
                if ((*it)->mem_can_handle(sub_mem))
                {
                    current_plugin = *it;
                    //cur_mem = sub_mem;
                    bFound = true;
                    break;
                }
            }
            delete childs_plugins;

            if (!bFound)
            {
                DEBUG_MESSAGE(sub_mem->name);
                DEBUG_MESSAGE("cant build plugins chain. subplugin cant handle.");
                //std::for_each(plugins_chain.begin(), plugins_chain.end(), [](std::pair<std::unique_ptr<Memory>,Storage*> &v){ delete v.first; });
                plugins_chain.clear();
                return E_END_ARCHIVE;
            }
            plugins_chain.push_back(std::make_pair(std::move(sub_mem),current_plugin));


        }
    // Reverse plugins chain
    plugins_chain.reverse();

    //Pack through plugins chain
    std::string cur_src_path = src_path;
    //convert char* list to string list

    auto null_mem = make_unique<Memory>();
    null_mem->begin = 0;
    null_mem->begin = 0;
    null_mem->mmf = nullptr;
    null_mem->name = arc_abs_filename;
    //std::list<std::pair<std::unique_ptr<Memory>,Storage*>>::iterator pc_it;
    for (auto pc_it = plugins_chain.begin() ; pc_it != plugins_chain.end() ; ++pc_it)
    {
        std::unique_ptr<Memory>& cur_mem = (*pc_it).first;
        bool is_main_plugin = false;
        if (std::distance(pc_it, plugins_chain.end()) == 1) // it is main plugin
        {
            is_main_plugin = true;
            (*pc_it).second->mem_post_close(cur_mem); // need close, because t_pack reopen him
        }
        if (is_main_plugin)
            cur_mem.swap(null_mem);
        std::shared_ptr<MemoryMappedFile> temp_mmf = (*pc_it).second->t_pack(cur_mem, cur_src_path, cur_pack_list);
        cur_mem.reset();
        if (is_main_plugin)
            cur_mem.swap(null_mem);

        if (!temp_mmf)
        {
            pack_result = E_NO_FILES;
            break;
        }
        std::string cur_full_path = temp_mmf->GetFullPath();
        const size_t last_slash_idx = cur_full_path.find_last_of("\\/");
        std::string cur_src_name = cur_full_path.substr(last_slash_idx+1);
        cur_pack_list.clear();
        cur_pack_list.push_back(cur_src_name);
        cur_src_path = cur_full_path.substr(0,last_slash_idx+1);
        //delete temp_mmf;
    }

clean:

    //current_plugin->mem_post_close(plugins_chain.front().first); // t_pack yet close file
    plugins_chain.clear();

    delete state->plugins;
    delete state;
    
    } // end of multifile
    return pack_result;

}

BOOL  unity_can_handle(char *FileName)
{

    DEBUG_METHOD();
    //OutputDebugString("ucanhandle");
    tState *state = new tState();
    string arc_abs_filename = FileName;
    std::replace( arc_abs_filename.begin(), arc_abs_filename.end(), '/', '\\');
    bool can_handle = false;

    UPlugins* plugins = new UPlugins();
    state->plugins = plugins;
    Storage* current_plugin = nullptr;
    std::vector<Storage*>* all_plugins = state->plugins->get_all();
    for (auto it = all_plugins->begin() ; it != all_plugins->end() ; ++it)
    {
        std::unique_ptr<Memory> mem = (*it)->mem_pre_open(arc_abs_filename);
        if ((*it)->mem_can_handle(mem))
        {
            current_plugin = *(it);
            DEBUG_MESSAGE("CAN_HANDLE: " + (*it)->description());
            can_handle = true;
        }
        (*it)->mem_post_close(mem);
    }
    delete all_plugins;

    if (! current_plugin)
    {
        DEBUG_MESSAGE("Error: current_plugin not found or all plugins refused process this file");
        can_handle = false;
    }

    delete state->plugins;
    delete state;

    return can_handle;
}

void unity_set_calback_vol(STATE_HANDLE hArcData, tChangeVolProc pChangeVolProc)
{
    DEBUG_METHOD();
    DEBUG_VALUE_OF(hArcData);
    tState *assets=(tState *)(hArcData);
    if (assets == INVALID_HANDLE_VALUE)
        DEBUG_MESSAGE("assets_SetCallBackVol: invalid handle")
    else
        assets->pLocChangeVol=pChangeVolProc;
};

void unity_set_callback_proc(STATE_HANDLE hArcData, tProcessDataProc pProcessDataProc)
{
    DEBUG_METHOD();
    DEBUG_VALUE_OF(hArcData);
    tState *assets=(tState *)(hArcData);
    if (assets == INVALID_HANDLE_VALUE)
        DEBUG_MESSAGE("assets_SetCallBackProc: invalid handle")
    else
        assets->pLocProcessData=pProcessDataProc;

};


//-----------------------=[ DLL exports ]=--------------------

// OpenArchive should perform all necessary operations when an archive is to be opened
STATE_HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData)
{
    return unity_open(ArchiveData);
}

// WinCmd calls ReadHeader to find out what files are in the archive
int __stdcall ReadHeader(STATE_HANDLE hArcData, tHeaderData *HeaderData)
{
    return unity_next_item(hArcData, HeaderData);
}

// ProcessFile should unpack the specified file or test the integrity of the archive
int __stdcall ProcessFile(STATE_HANDLE hArcData, int Operation, char *DestPath, char *DestName)
{
    return unity_process(hArcData, Operation, DestPath, DestName);
}

// CloseArchive should perform all necessary operations when an archive is about to be closed
int __stdcall CloseArchive(STATE_HANDLE hArcData)
{
    return unity_close(hArcData);
}

// This function allows you to notify user about changing a volume when packing files
void __stdcall SetChangeVolProc(STATE_HANDLE hArcData, tChangeVolProc pChangeVolProc)
{
    unity_set_calback_vol(hArcData, pChangeVolProc);
}

// This function allows you to notify user about the progress when you un/pack files
void __stdcall SetProcessDataProc(STATE_HANDLE hArcData, tProcessDataProc pProcessDataProc)
{
    unity_set_callback_proc(hArcData, pProcessDataProc);
}


int __stdcall PackFiles (char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags)
{
    return unity_pack(PackedFile, SubPath, SrcPath, AddList, Flags);
}

int __stdcall GetPackerCaps()
{
    return PK_CAPS_NEW|PK_CAPS_MODIFY|PK_CAPS_MULTIPLE|PK_CAPS_BY_CONTENT ;
};

int __stdcall DeleteFiles (char *PackedFile, char *DeleteList)
{
    return 0;
};

BOOL __stdcall CanYouHandleThisFile (char *FileName)
{
    return unity_can_handle(FileName);
}

int __stdcall SetKeyRegisterCallback(tKeyRegisterProc KeyRegisterProc)
{
    DEBUG_METHOD();
    unsigned int a = KeyRegisterProc(12);
    DEBUG_VALUE_OF(a);
}
int __stdcall KeyHandle (unsigned int ShortCut)
{
    DEBUG_METHOD();
    DEBUG_VALUE_OF(ShortCut);
    if (ShortCut & SC_ALT )
    {
        unsigned int Key = ShortCut - SC_ALT;
        DEBUG_VALUE_OF(Key);
        if (Key == 75)
            return PK_KEY_RELOAD;
        if (Key == 74)
        {    
            int msgboxID = MessageBoxW(
                NULL,
                (LPCWSTR)L"Resource not available\nDo you want to try again?",
                (LPCWSTR)L"Account Details",
                MB_OKCANCEL 
                );
        }
    }
    return PK_KEY_SUCCESS;
    
}

UPlugins::UPlugins()
{
    DEBUG_METHOD();
    DEBUG_VALUE_OF(get_current_dir());
    plugin_manager_ = new pluma::Pluma();
    plugin_manager_->acceptProviderType<StorageProvider>();
    // Load libraries
    plugin_manager_->loadFromFolder((get_current_dir() + std::string("\\plugins")).c_str());
    // Get providers into a vector
    std::vector<StorageProvider*> providers;
    plugin_manager_->getProviders(providers);
    // Create a Storage from each provider
    std::vector<StorageProvider*>::iterator it;
    heirarchy_ =  new CGraph::Graph(providers.size());
    DEBUG_MESSAGE("Load plugins:");
    for (it = providers.begin() ; it != providers.end() ; ++it)
    {
        // Create a Storage
        Storage* storage = (*it)->create();
        StorageID id = storage->get_id();
        if (id.id != 0)
            heirarchy_->addEdge(id.parent_id, id.id);
        plugins_map_.insert(std::make_pair(id.id, storage));
        DEBUG_VALUE_OF(id.id);
        DEBUG_VALUE_OF(storage->description());
    }

}

UPlugins::~UPlugins()
{
    delete plugin_manager_;
    delete heirarchy_;
}

std::vector<Storage*>* UPlugins::get_all()
{
    vector<Storage*>* all = new std::vector<Storage*>();
    for(std::map<int,Storage*>::iterator it = plugins_map_.begin(); it != plugins_map_.end(); ++it)
        all->push_back(it->second);
    return all;
}

std::vector<Storage*>* UPlugins::get_childs(int parent_id)
{
    std::vector<Storage*>* plugin_childs = new std::vector<Storage*>;
    std::vector<int> plugin_childs_ids = heirarchy_->get_childs(parent_id);
    std::vector<int>::iterator it;
    for (it = plugin_childs_ids.begin() ; it != plugin_childs_ids.end() ; ++it)
    {
        plugin_childs->push_back(plugins_map_[*(it)] );
    }
    return  plugin_childs;
}
/*
std::vector<Storage*>* UPlugins::get_from_list(std::list<char*> sub_list)
{
    std::vector<Storage*>* plugins = new std::vector<Storage*>;
    std::vector<int> plugins_ids = heirarchy_->get_childs(parent_id);
    std::vector<int>::iterator it;
    for (it = plugin_childs_ids.begin() ; it != plugin_childs_ids.end() ; ++it)
    {
       plugin_childs->push_back(plugins_map_[*(it)] );
    }
    return  plugin_childs;
}*/
