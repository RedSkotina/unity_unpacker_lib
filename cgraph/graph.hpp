#ifndef GRAPH_HPP
#define GRAPH_HPP
#include <list>
#include <vector>
namespace CGraph
{

class Graph
{
public:
    Graph(int V);
    ~Graph();
public:
    void addEdge(int v, int w); // function to add an edge to graph
    void BFS(int s);  // prints BFS traversal from a given source s
    std::vector<int> get_childs(int k);
private:
    int V;    // No. of vertices
    std::list<int> *adj;    // Pointer to an array containing adjacency lists
};




}

#endif // GRAPH_HPP
