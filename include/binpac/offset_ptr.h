#ifndef offset_ptr_h
#define offset_ptr_h
#include "binpac.h"
#include <exception>

class offset_ptr
{
public:
	offset_ptr() { page_ = 0; offset_ = 0;};
	
	offset_ptr(const offset_ptr& ptr)
	{
		page_ =  ptr.page_;
		offset_ = ptr.offset_;
	};
	offset_ptr(const unsigned char*   ptr) 
	{
        if (! binpac::IMemoryStorageSingleton::instance()->mem_strg())
        {
            page_ = 0;
			offset_ = 0;
            return;
        }    
		if (ptr < binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin())
			throw std::bad_exception();
		ptrdiff_t diff = ptr - binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin(); // TODO: check for outbound
		if (diff != 0 )
		{
            page_ =  diff / binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
            offset_ = diff % binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		}
		else
		{
			page_ = 0;
			offset_ = 0;
		}
	};

	
	~offset_ptr() {};
	
	unsigned char* get_fictive_ptr() const
	{
		return (binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() + page_*binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() + offset_);
	}

	unsigned char* get_ptr() const
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		return ( binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() + offset_);
	}

	
	unsigned char* operator->()
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		
		return ( binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() + offset_);
	};
	/*
	unsigned char operator*()
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		
		return (unsigned char) *( binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() + offset_);
	};*/
	unsigned char& operator*()
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		
		return (unsigned char&) *( binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() + offset_);
	};
	  
	operator short unsigned int*() const
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		
		return (short unsigned int*)( binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() +  offset_);
	}
	
    operator unsigned int*() const
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		
		return (unsigned int*)( binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() +  offset_);
	}
	
	operator unsigned char * () const
	{
		binpac::IMemoryStorageSingleton::instance()->mem_strg()->SetPage(page_);
		
		return binpac::IMemoryStorageSingleton::instance()->mem_strg()->Begin() +  offset_;
	};
	
	bool operator <=(const offset_ptr& c2) const
	{
        // ptrdiff_t (signed)=> size_t(unsigned)
		size_t  orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		size_t  second_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * c2.page_ + c2.offset_;
		
		return orig_diff <= second_diff;
	};
	bool operator >=(const offset_ptr& c2) const
	{
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		size_t second_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * c2.page_ + c2.offset_;
		
		return orig_diff >= second_diff;
	};
	
	bool operator >(const offset_ptr& c2) const
	{
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		size_t second_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * c2.page_ + c2.offset_;
		
		return orig_diff > second_diff;
	};
	
	int operator+(const offset_ptr & c2) const
	{
		offset_ptr temp;
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		size_t second_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * c2.page_ + c2.offset_;
		ptrdiff_t diff = orig_diff + second_diff;
		
		
		
		return diff;
	};
	int operator-(const offset_ptr & c2) const
	{
		offset_ptr temp;
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		size_t second_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * c2.page_ + c2.offset_;
		ptrdiff_t diff = orig_diff - second_diff;
		
		return diff;
	};
	offset_ptr operator+(const int & c2) const
	{
		offset_ptr temp;
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		orig_diff = orig_diff + c2;
		temp.page_ =  orig_diff / binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		temp.offset_ = orig_diff % binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		
		return temp;
	};
	offset_ptr operator-(const int & c2) const
	{
		offset_ptr temp;
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		orig_diff = orig_diff - c2;
		temp.page_ =  orig_diff / binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		temp.offset_ = orig_diff % binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		
		return temp;
	};
	
	const offset_ptr& operator+=(const int & c2) const
	{
		size_t orig_diff = binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize() * page_ + offset_;
		orig_diff = orig_diff + c2;
		page_ =  orig_diff / binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		offset_ = orig_diff % binpac::IMemoryStorageSingleton::instance()->mem_strg()->GetPageSize();
		
		return *this;
	};
	

	const offset_ptr& operator=(const offset_ptr  & c2) const
	{
		if(this != &c2)
		{
			page_ = c2.page_;
			offset_ = c2.offset_;
		}
		return *this;
		
	};
	
public:
	mutable uint32_t page_;
	mutable uint32_t offset_;
	//static binpac::IMemoryStorageSingleton* mem_strg_singleton;
};
//binpac::IMemoryStorage* binpac::offset_ptr::mem_strg_ = nullptr;
#endif 