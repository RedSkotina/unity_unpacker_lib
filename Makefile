.PHONY: clean All

All:
	@echo "----------Building project:[ debug_log - Debug ]----------"
	@$(MAKE) -f  "debug_log.mk"
	@echo "----------Building project:[ memory_mapped_file - Debug ]----------"
	@$(MAKE) -f  "memory_mapped_file.mk"
	@echo "----------Building project:[ plugin_assets - Debug ]----------"
	@cd "plugins" && $(MAKE) -f  "plugin_assets.mk" && $(MAKE) -f  "plugin_assets.mk" PostBuild
	@echo "----------Building project:[ plugin_tex - Debug ]----------"
	@cd "plugins" && $(MAKE) -f  "plugin_tex.mk" && $(MAKE) -f  "plugin_tex.mk" PostBuild
	@echo "----------Building project:[ plugin_uogm - Debug ]----------"
	@cd "plugins" && $(MAKE) -f  "plugin_uogm.mk" && $(MAKE) -f  "plugin_uogm.mk" PostBuild
	@echo "----------Building project:[ unity - Debug ]----------"
	@$(MAKE) -f  "unity.mk" && $(MAKE) -f  "unity.mk" PostBuild
clean:
	@echo "----------Cleaning project:[ debug_log - Debug ]----------"
	@$(MAKE) -f  "debug_log.mk"  clean
	@echo "----------Cleaning project:[ memory_mapped_file - Debug ]----------"
	@$(MAKE) -f  "memory_mapped_file.mk"  clean
	@echo "----------Cleaning project:[ plugin_assets - Debug ]----------"
	@cd "plugins" && $(MAKE) -f  "plugin_assets.mk"  clean
	@echo "----------Cleaning project:[ plugin_tex - Debug ]----------"
	@cd "plugins" && $(MAKE) -f  "plugin_tex.mk"  clean
	@echo "----------Cleaning project:[ plugin_uogm - Debug ]----------"
	@cd "plugins" && $(MAKE) -f  "plugin_uogm.mk"  clean
	@echo "----------Cleaning project:[ unity - Debug ]----------"
	@$(MAKE) -f  "unity.mk" clean
